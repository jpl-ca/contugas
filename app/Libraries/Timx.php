<?php

namespace Contugas\Libraries;

use \DateTime;

class Timx {

	public static function spanishDate($date)
    {
    	setlocale(LC_ALL,"es_ES.UTF-8");
		$date = DateTime::createFromFormat("Y-m-d h:i:s", $date);
		return ucfirst(strftime("%B",$date->getTimestamp()));
    }

}