<?php

namespace Contugas\Libraries;

class Pusher
{
	protected $registration_ids = array();
	protected $data_to_send = array();
	private $api_access_key;

	public function __construct() {
		$this->api_access_key = config('app.gcm_app_key', '');
	}

	public function addRecipent($registration_id) {
		$this->registration_ids = $registration_id;
	}

	public function addData( $data )
	{
		$this->data_to_send = $data;
	}

	public function send() {
		$deb = array();
		
		$fields = array(
			'to' => $this->registration_ids,
			'data'	=> $this->data_to_send
		);

		$headers = array(
			'Authorization: key=' . $this->api_access_key,
			'Content-Type: application/json'
		);
		$curl = curl_init();
		curl_setopt( $curl,CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send' );
		curl_setopt( $curl,CURLOPT_POST, true );
		curl_setopt( $curl,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $curl,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $curl,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($curl );

		curl_close( $curl );
		return $deb;
	}
}