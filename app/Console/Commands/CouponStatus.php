<?php

namespace Contugas\Console\Commands;

use Illuminate\Console\Command;

use Log;

use Carbon;

use Contugas\Models\Coupon;

class CouponStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coupons:change-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cambia los estados de los cupones';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Coupon::where('status', 'WAITING')
            ->where('start_date', '<', Carbon\Carbon::now())
            ->where('end_date', '>', Carbon\Carbon::now())
            ->update(['status' => 'RUNNING']);

        Coupon::where('status', 'RUNNING')
            ->where('end_date', '<', Carbon\Carbon::now())
            ->update(['status' => 'FINISHED']);

        Log::info("Changing status of coupons");
    }
}
