<?php

namespace Contugas\Providers;

use \Form;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Form::component('fhOpen', 'components.form-horizontal.open', ['attributes' => []]);
        Form::component('fhClose', 'components.form-horizontal.close', []);
        Form::component('fhText', 'components.form-horizontal.text', ['name', 'label', 'value' => null, 'attributes' => []]);
        Form::component('fhSelect', 'components.form-horizontal.select', ['name', 'label', 'value' => [], 'selected' => null, 'attributes' => []]);
        Form::component('fhSubmit', 'components.form-horizontal.submit', ['text' => 'Submit', 'class' => 'default', 'icon' => 'check']);
        Form::component('fhDateTime', 'components.form-horizontal.datetime', ['name', 'label', 'id' => null, 'value' => null, 'attributes' => []]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
            $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
        }
    }
}
