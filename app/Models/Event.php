<?php

namespace Contugas\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

use Carbon\Carbon;

class Event extends Model
{
    const FILES_PATH = 'models/events/';
    const FILES_URL = 'app/models/events/';
    const EVENT_TYPE = 'event';
    const RAFFLE_TYPE = 'raffle';
    protected $appends = ['image_url'];

    public static function files_path()
    {
    	return storage_path('app').'/'.self::FILES_PATH;
    }

    public static function types()
    {
    	return [SELF::EVENT_TYPE, SELF::RAFFLE_TYPE];
    }

    public function getImageUrlAttribute()
    {
    	if(isset($this->image_path) && !empty($this->image_path)){
            return url('/').'/'.SELF::FILES_URL.$this->id . '?rnd=' . uniqid('', true);
        }
        return null;
    }

    public static function prepareImageUrl($id)
    {
    	return SELF::FILES_PATH.$id;
    }

    public function participations()
    {
        return $this->hasMany(UserEventParticipation::class);
    }

    public function removeImageFileFromDisk()
    {
        if(!empty($this->image_path) && Storage::exists($this->image_path)){
            Storage::delete($this->image_path);
        }
    }
}
