<?php

namespace Contugas\Models;
use Storage;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class AppComponent extends Model
{
    const FILES_PATH = 'models/app-components/';
    const FILES_URL = 'app/models/app-components/';

    protected $appends = ['image_url', 'label'];

    public static function boot()
    {
        parent::boot();
        self::observe(new AppComponentObserver);
    }

    public static function files_path()
    {
    	return storage_path('app').'/'.self::FILES_PATH;
    }

    public function getImageUrlAttribute()
    {
        if(isset($this->image_path) && !empty($this->image_path)){
            return url('/').'/'.SELF::FILES_URL.$this->id . '?rnd=' . uniqid('', true);
        }
        return null;
    }

    public static function prepareImageUrl($id)
    {
        return SELF::FILES_PATH.$id;
    }

    public function removeImageFileFromDisk()
    {
        if(!empty($this->image_path) && Storage::exists($this->image_path)){
            Storage::delete($this->image_path);
        }
    }

    public function ubigeo()
    {
        return $this->belongsTo(Ubigeo::class, 'ubigeo_code');
    }

    public static function sections()
    {
        $list = [
            [
                "machine_name" => "MAIN",
                "label" => "Pantalla principal"
            ],
            [
                "machine_name" => "INVOICE",
                "label" => "Consumo"
            ],
            [
                "machine_name" => "HISTORY",
                "label" => "Historial"
            ],
            [
                "machine_name" => "PAYMENT",
                "label" => "Lista de pagos"
            ],
            [
                "machine_name" => "FINANCING",
                "label" => "Financiamiento"
            ]
        ];

        return $list;
    }

    public function getLabelAttribute() {
        $label = "";

        foreach (self::sections() as $section) {
            if ($section['machine_name'] == $this->section) {
                $label = $section['label'];
            }
        }

        return $label;
    }
}
