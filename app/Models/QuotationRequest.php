<?php

namespace Contugas\Models;

use Illuminate\Database\Eloquent\Model;
use Contugas\Models\Ubigeo;

class QuotationRequest extends Model
{
    protected $fillable = ['name','last_name','address','province_id','phone','email','comment'];

	public function province()
    {
    	return $this->belongsTo(Ubigeo::class, 'province_id', 'code');
    }

    public function district()
    {
    	return $this->belongsTo(Ubigeo::class, 'district_id', 'code');
    }
}
