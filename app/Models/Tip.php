<?php

namespace Contugas\Models;

use Illuminate\Database\Eloquent\Model;

use Storage;

use Carbon\Carbon;

class Tip extends Model
{
    const FILES_PATH = 'models/tips/';
    const FILES_URL = 'app/models/tips/';
    protected $appends = ['image_url'];

    public static function files_path()
    {
    	return storage_path('app').'/'.self::FILES_PATH;
    }

    public function getImageUrlAttribute()
    {
        if(isset($this->image_path) && !empty($this->image_path)){
            return url('/').'/'.SELF::FILES_URL.$this->id . '?rnd=' . uniqid('', true);  
        }
        return null;
    }

    public static function prepareImageUrl($id)
    {
        return SELF::FILES_PATH.$id;
    }

    public function removeImageFileFromDisk()
    {
        if(!empty($this->image_path) && Storage::exists($this->image_path)){
            Storage::delete($this->image_path);
        }
    }
}
