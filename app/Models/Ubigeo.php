<?php

namespace Contugas\Models;

use Illuminate\Database\Eloquent\Model;

class Ubigeo extends Model
{

    protected $table = 'ubigeo';

    protected $primaryKey = 'code';

    public $timestamps = false;

    public function provinces()
    {
    	return $this->hasMany(Ubigeo::class, 'parent', 'code');
    }

    public function districts()
    {
    	return $this->hasMany(Ubigeo::class, 'parent', 'code');
    }

    public function coupons()
    {
        return $this->belongsToMany(Coupon::class, 'ubigeo_coupon_constraints', 'ubigeo_code', 'coupon_id');
    }

    public static function departaments()
    {
        return SELF::where('parent', '=', null);
    }

    public static function findProvincesFromText( $texto )
    {
        return Ubigeo::whereNotNull('parent')->where('name', strtoupper($texto));
    }
}
