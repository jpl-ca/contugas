<?php

namespace Contugas\Models;

use Illuminate\Database\Eloquent\Model;

class CouponObserver
{
    public function saving(Coupon $coupon)
    {
    	if($coupon->status == CouponStatus::PENDING || !isset($coupon->status) ){

    		if( isset($coupon->coupon_category_id) &&
    			isset($coupon->company_id) && 
    			isset($coupon->title) &&
    			isset($coupon->user_swaps) &&
    			isset($coupon->user_swaps) &&
    			isset($coupon->start_date) &&
    			isset($coupon->end_date)
    			)
	    	{
	    		$coupon->status = CouponStatus::WAITING;	
	    	}
    	}
    }

    public function saved(Coupon $coupon)
    {

    }

}
