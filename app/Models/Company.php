<?php

namespace Contugas\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'ruc',
    ];

    public function merchants()
    {
        return $this->hasMany(Merchant::class, 'company_id', 'id');
    }

    public function company_places()
    {
        return $this->hasMany(CompanyPlace::class, 'company_id', 'id');
    }

    public function swaps()
    {
        return $this->hasManyThrough(Swap::class, Coupon::class, 'company_id', 'coupon_id');
    }
}
