<?php

namespace Contugas\Models\OAuth;

use Illuminate\Database\Eloquent\Model;
use Contugas\Models\OAuth\OauthSession;

class OauthAccessToken extends Model
{
    public static function removeExpiredOnes()
    {
    	$accessTokens = self::where('expire_time', '<', time())->get();

    	foreach ($accessTokens as $accessToken) {
    		$accessToken->session->delete();
    	}
    }

    public function session()
    {
    	return $this->belongsTo(OauthSession::class, 'session_id', 'id');
    }
}
