<?php

namespace Contugas\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyPlace extends Model
{
	public function company()
	{
		return $this->belongsTo(Company::class, 'company_id', 'id');
	}
}
