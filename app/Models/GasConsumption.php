<?php

namespace Contugas\Models;

use Illuminate\Database\Eloquent\Model;
use Contugas\Libraries\Timx;
use Carbon\Carbon;

class GasConsumption extends Model
{
    public function toArray($custom = false)
    {
        $array = parent::toArray();
    	if($custom)
    	{
	        $array['month'] = Carbon::createFromFormat('Y-m-d h:i:s', $this->date)->format('m');
	        $array['month_name'] = Timx::spanishDate($this->date);
	        $array['year'] = Carbon::createFromFormat('Y-m-d h:i:s', $this->date)->format('Y');
	        unset($array['date']);
	        unset($array['created_at']);
	        unset($array['updated_at']);    		
    	}
        return $array;
    }

}
