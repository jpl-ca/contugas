<?php

namespace Contugas\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Storage;

class Coupon extends Model
{
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title', 'description', 'img_url', 'max_swaps', 'user_swaps', 'start_date', 'end_date', 'active', 'merchants'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
        'merchants' => 'array'
    ];

    protected $appends = ['img_url', 'status_label'];

    const FILES_PATH = 'models/coupons/';
    const FILES_URL = 'app/models/coupons/';

    public static function boot()
    {
        parent::boot();
        self::observe(new CouponObserver);
    }
    public function setStatusAttribute($value)
    {
        if(in_array($value, CouponStatus::all())){
            $this->attributes['status'] = $value;
        }else{
            $this->attributes['status'] = CouponStatus::PENDING;
        }
    }

    public function getImgUrlAttribute()
    {
        if(isset($this->image_path) && !empty($this->image_path)){
            return url('/').'/'.SELF::FILES_URL.$this->id . '?rnd=' . uniqid('', true);
        }
        return null;
    }

    public static function files_path()
    {
        return storage_path('app').'/'.self::FILES_PATH;
    }

    public static function prepareImageUrl($id)
    {
        return SELF::FILES_PATH.$id;
    }

    /**
     * Scope a query to only include active coupons.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    /**
     * Scope a query to only include inactive coupons.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeInactive($query)
    {
        return $query->where('active', false);
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'merchant_id', 'id');
    }

    public function swaps()
    {
        return $this->hasMany(Swap::class, 'coupon_id', 'id');
    }

    public function ubigeoCouponConstraints()
    {
        return $this->hasMany(UbigeoCouponConstraint::class, 'coupon_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public static function generateCode()
    {
        return strtoupper(str_random(7));
    }

    public function removeImageFileFromDisk()
    {
        if(!empty($this->image_path) && Storage::exists($this->image_path)){
            Storage::delete($this->image_path);
        }
    }

    public function getStatusLabelAttribute()
    {
        $labels[CouponStatus::PENDING] = 'PENDIENTE';
        $labels[CouponStatus::WAITING] = 'EN ESPERA';
        $labels[CouponStatus::STOPPED] = 'PARADO';
        $labels[CouponStatus::RUNNING] = 'EN EJECUCIÓN';
        $labels[CouponStatus::FINISHED] = 'FINALIZADO';
        return  (isset($labels[$this->status]) ? $labels[$this->status] : 'DESCONOCIDO');
    }
}
