<?php

namespace Contugas\Models;

use Illuminate\Database\Eloquent\Model;
use Contugas\Models\User;

class Swap extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'user_id', 'coupon_id',
    ];

    /**
     * Scope a query to only include swaps by user_id.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByUser($query, $user_id)
    {
        return $query->where('user_id', $user_id);
    }

    /**
     * Scope a query to only include swaps by coupon_id.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByCoupon($query, $coupon_id)
    {
        return $query->where('coupon_id', $coupon_id);
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class, 'coupon_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
