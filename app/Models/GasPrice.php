<?php

namespace Contugas\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

use Carbon\Carbon;

use Log;

class GasPrice extends Model
{
    static public function lastPrices()
    {
    	$numOfMonths = 6;
    	$startDatetime = Carbon::now()->addMonth(-$numOfMonths-1);
    	$endDatetime = Carbon::now()->addMonth(-1);
    	$rawSql = "
			select
				gp.year,
				gp.month,
				(
					select
						gas_prices.value
					from
						gas_prices
					where
						gas_prices.year = gp.year AND
						gas_prices.month = gp.month
					order by
						gas_prices.created_at desc
					limit 1
				) last_value
			from
				gas_prices gp
			where
				str_to_date( concat(gp.month, '/1/' ,gp.year), '%m/%d/%Y') >= '{$startDatetime}' AND
				str_to_date( concat(gp.month, '/1/' ,gp.year), '%m/%d/%Y') <= '{$endDatetime}'
			group by
				gp.year,
				gp.month
			order by
				CAST(gp.year AS UNSIGNED) desc,
			    CAST(gp.month AS UNSIGNED) desc
    	";

        Log::info($rawSql);

    	$rows = DB::select($rawSql);

    	$months = [];

    	if (count($rows) > 0) {
    		$lastPrice = $rows[0];
    	}

    	for ($i = 0; $i < $numOfMonths; $i++) {
    		$date = Carbon::now()->addMonth( - $i -1);
    		$months[$i]['year'] = $date->year;
    		$months[$i]['month'] = $date->month;
    		$months[$i]['value'] = 0;
    	}

    	for ($i = 0; $i < count($months); $i++) {

    		foreach($rows as $key => $value) {

    			if ((int)$months[$i]['year'] == (int)$value->year && (int)$months[$i]['month'] == (int)$value->month) {
    				$months[$i]['value'] = (float)$value->last_value;
                    break;
    			}else{
    				$months[$i]['value'] = (float)$lastPrice->last_value;
    			}
    		}
    	}

    	return $months;
    }
}
