<?php

namespace Contugas\Models;

use Illuminate\Database\Eloquent\Model;

class AdminDashboard extends Model
{
    protected $fillable = ['admin_id', 'new_quotation_requests', 'new_suggestions'];
}
