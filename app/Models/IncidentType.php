<?php

namespace Contugas\Models;

use Illuminate\Database\Eloquent\Model;
use Contugas\Models\Incident;

class IncidentType extends Model
{
    public function incidents()
    {
    	return $this->hasMany(Incident::class, 'incident_type_id', 'id');
    }
}
