<?php

namespace Contugas\Models;

use Illuminate\Database\Eloquent\Model;
use Contugas\Models\IncidentType;
use Contugas\Models\User;

class Incident extends Model
{
    protected $fillable = ['user_id', 'incident_type_id', 'comment', 'lat', 'lng', 'address'];
    
    public function type()
    {
    	return $this->belongsTo(IncidentType::class, 'incident_type_id', 'id');
    }

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function toArray()
    {
        $array = parent::toArray();
	    $array['user_name'] = is_null($this->user) ? 'Usuario no registrado' : $this->user->name . ' ' . $this->last_name;
	    $array['type_name'] =$this->type->name;
        return $array;
    }
}
