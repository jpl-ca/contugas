<?php

namespace Contugas\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function dashboard()
    {
        return $this->hasOne( AdminDashboard::class);
    }

    public function isRoot(){
        return ( (string)$this->id == '1' );
    }
}
