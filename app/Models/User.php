<?php

namespace Contugas\Models;

use Carbon\Carbon;
use Contugas\Libraries\Timx;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Contugas\Models\Ubigeo;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'client_number', 'id_number', 'email', 'password', 'gcm_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function swaps()
    {
        return $this->hasMany(Swap::class, 'user_id', 'id');
    }

    public function swappedCoupons()
    {
        return $this->belongsToMany(Coupon::class, 'swaps', 'user_id', 'coupon_id');
    }

    public function gasConsumptions()
    {
        return $this->hasMany(GasConsumption::class, 'user_id', 'id');
    }

    public function gasConsumptionLastMonths($months = 6)
    {
        $gasConsumptionsCollection = $this->gasConsumptions()->orderBy('date', 'desc')->take($months)->get();
        $gasConsumptions = array();
        foreach ($gasConsumptionsCollection as $gasConsumption) {
            array_push( $gasConsumptions, $gasConsumption->toArray(true) );
        }
        $currentDate = Carbon::now()->format('Y-m-d h:i:s');
        $currentGasConsumption = [
            'user_id' => $this->id,
            'consumption' => 0,
            'amount_paid' => 0,
            'amount_saved' => 0,
            'month' => Carbon::now()->format('m'),
            'month_name' => Timx::spanishDate($currentDate),
            'year' => Carbon::now()->format('Y')
        ];
        $gasConsumptions = array_reverse($gasConsumptions);
        array_push($gasConsumptions, $currentGasConsumption);
        $gasConsumptions = array_reverse($gasConsumptions);
        return $gasConsumptions;// array_push($gasConsumptions, $currentGasConsumption);
    }

    public function province()
    {
        return $this->belongsTo(Ubigeo::class, 'province_id');
    }

    public function district()
    {
        return $this->belongsTo(Ubigeo::class, 'district_id');
    }
}
