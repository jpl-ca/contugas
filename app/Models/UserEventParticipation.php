<?php

namespace Contugas\Models;

use Illuminate\Database\Eloquent\Model;

class UserEventParticipation extends Model
{
	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
