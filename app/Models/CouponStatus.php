<?php 

namespace Contugas\Models;

class CouponStatus
{
	const RUNNING = 'RUNNING';
	const WAITING = 'WAITING';
	const STOPPED = 'STOPPED';
	const PENDING = 'PENDING';
	const FINISHED = 'FINISHED';

	public static function all()
	{
		return [self::PENDING, self::WAITING, self::RUNNING, self::STOPPED, self::FINISHED];
	}
}