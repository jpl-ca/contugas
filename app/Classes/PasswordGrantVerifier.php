<?php

namespace Contugas\Classes;

use Illuminate\Support\Facades\Auth;

use Contugas\Models\User;

class PasswordGrantVerifier
{
  public function verify($username, $password)
  {
      $credentials = [
        'email'    => $username,
        'password' => $password
      ];

      if (Auth::once($credentials)) {
          return Auth::user()->id;
      }

      $user = User::where('client_number', $username)->where('id_number', $password)->first();
      
      if (!is_null($user)) {
          return $user->id;
      }

      return false;
  }
}
