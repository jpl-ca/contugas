<?php

namespace Contugas\Classes;
//use \ArrayObject;
use Contugas\Classes\ContugasAccount;

class ContugasAccountCollection //extends ArrayObject
{
	protected $_collection;

	public function __construct()
	{
		$this->_collection = collect([
			852963 => [
				'name' => 'Luis Sanchez',
				'client_number' => 852963,
				'id_number' => 44556677
			],
			784412 => [
				'name' => 'Juan Ramirez',
				'client_number' => 784412,
				'id_number' => 33221144
			],
			221001 => [
				'name' => 'Carmen Ordoñez',
				'client_number' => 221001,
				'id_number' => 44772200
			]
		]);
	}

	public function find($clientNumber)
	{
		return $this->_collection->get($clientNumber);
	}

	public function exists($clientNumber, $idNumber)
	{

		$filtered = $this->_collection->where('client_number', $clientNumber)->where('id_number', $idNumber);
		return $filtered->count() > 0;
	}

}