<?php

namespace Contugas\Classes;
//use \ArrayObject;

class ContugasAccount //extends ArrayObject
{
	public $name = array();
	public $client_number = array();
	public $id_number = array();

	public function __construct(array $data)
	{
		foreach ($data as $key => $value) {
			$this->$key = $value;
		}
	}

	public function toArray()
	{
		return [			
			'name' => $this->name,
			'client_number' => $this->client_number,
			'id_number' => $this->id_number,
		];
	}

}