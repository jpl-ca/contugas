<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('admin/');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

/*
Route::group(['middleware' => ['web']], function () {
    //
});
*/

Route::group(['middleware' => 'web'], function () {
    Route::group(['prefix' => 'temp'], function(){
        Route::get('/invoices/{filename}', 'TempController@invoice');
    });

    Route::group(['prefix' => 'alianzas'], function () {
        Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function() {
            multiAuthRoutes('MerchantAuthController', 'PasswordController');
        });
        Route::group(['middleware' => ['auth:merchant']], function() {
            Route::get('/', 'MerchantHomeController@index');
            Route::get('/coupons/swap.html', 'MerchantHomeController@swap');
            Route::get('/coupons/history.{format}', 'MerchantHomeController@history');
        });
    });

    Route::group(['prefix' => 'merchant'], function () {
        Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function() {
            multiAuthRoutes('MerchantAuthController', 'PasswordController');
        });

        Route::group(['middleware' => ['auth:merchant']], function() {
            Route::get('/', 'MerchantHomeController@index');
            Route::get('/coupons/swap.html', 'MerchantHomeController@swap');
            Route::get('/coupons/history.{format}', 'MerchantHomeController@history');
        });
    });

    Route::group(['prefix' => 'admin'], function () {

        Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function() {

            multiAuthRoutes('AdminAuthController', 'PasswordController');

        });

        Route::group( ['middleware' => ['auth:admin'] ], function(){

            Route::group(['prefix' => 'coupons'], function() {
                Route::get('/index.{format}', 'CouponController@index');
                Route::get('/create.{format}', 'CouponController@create');
                Route::get('/{id}.{format}', 'CouponController@show');
                Route::get('/{id}/edit.{format}', 'CouponController@edit');
                Route::get('/{id}/show.{format}', 'CouponController@show');
            });

            Route::group(['prefix' => 'tips'], function(){
                Route::get('/index.{format}', 'TipController@index');
                Route::get('/create.{format}', 'TipController@create');
                Route::get('/{id}.{format}', 'TipController@show');
                Route::get('/{id}/edit.{format}', 'TipController@edit');
            });

            Route::group(['prefix' => 'events'], function(){
                Route::get('/index.{format}', 'EventController@index');
                Route::get('/create.{format}', 'EventController@create');
                Route::get('/{id}.{format}', 'EventController@show');
                Route::get('/{id}/edit.{format}', 'EventController@edit');
                Route::get('/{id}/participations.{format}', 'EventController@participations');
            });

            Route::group(['prefix' => 'companies'], function(){
                Route::get('/index.{format}', 'CompanyController@index');
                Route::get('/create.{format}', 'CompanyController@create');
                Route::get('/{id}.{format}', 'CompanyController@show');
                Route::get('/{id}/edit.{format}', 'CompanyController@edit');
            });

            Route::group(['prefix' => 'suggestions'], function(){
                Route::get('/index.{format}', 'SuggestionController@index');
                Route::get('/{id}.{format}', 'SuggestionController@show');
            });

            Route::group(['prefix' => 'app-components'], function(){
                Route::get('/index.{format}', 'AppComponentController@index');
                Route::get('/create.{format}', 'AppComponentController@create');
                Route::get('/{id}.{format}', 'AppComponentController@show');
                Route::get('/{id}/edit.{format}', 'AppComponentController@edit');
            });

            Route::group(['prefix' => 'quotation-requests'], function(){
                Route::get('/index.{format}', 'QuotationRequestController@index');
                Route::get('/create.{format}', 'QuotationRequestController@create');
                Route::get('/{id}.{format}', 'QuotationRequestController@show');
                Route::get('/{id}/edit.{format}', 'QuotationRequestController@edit');
            });

            Route::group(['prefix' => 'admin-users'], function(){
                Route::get('/index.{format}', 'AdminUserController@index');
                Route::get('/create.{format}', 'AdminUserController@create');
                Route::get('/{id}/edit.{format}', 'AdminUserController@edit');
                Route::get('/{id}.{format}', 'AdminUserController@show');
                Route::post('/', 'AdminUserController@store');
                Route::put('/{id}', 'AdminUserController@update');
                Route::delete('/{id}', 'AdminUserController@destroy');
            });

            Route::group(['prefix' => 'reports'], function() {
                Route::get('/index.{format}', 'ReportController@index');
                Route::get('/quotation-request/month-and-district.{format}', 'ReportController@quotationRequestGroupByMonthAndDistrict');
                Route::get('/quotation-request/district.{format}', 'ReportController@quotationRequestGroupByDistrict');
                Route::get('/tips/month.{format}', 'ReportController@tipsGroupByMonth');

                Route::get('/coupons/swaps/month-category.{format}', 'ReportController@couponsSwapsByMonth');
                Route::get('/coupons/swaps/year-category.{format}', 'ReportController@couponsSwapsByYearAndCategory');
                Route::get('/coupons/month-category.{format}', 'ReportController@couponsByMonthAndCategory');
                Route::get('/coupons/index.html', 'CouponReportController@index');
                Route::get('/coupons/items.json', 'CouponReportController@items');
                Route::get('/coupons/{coupon_id}/swaps/everyday.json', 'CouponReportController@swapsOfEveryday');

                Route::get('/events/month.{format}', 'ReportController@eventsByMonth');
                Route::get('/events/attendees/month.{format}', 'ReportController@eventAttendeesByMonth');
                Route::get('/raffles/month.{format}', 'ReportController@rafflesByMonth');
                Route::get('/suggestions/month.{format}', 'ReportController@suggestionsByMonth');
                Route::get('/users/month.{format}', 'ReportController@usersByMonth');
            });
            
            Route::group(['prefix' => 'customers'], function() {
                Route::get('/index.{format}', 'CustomerController@index');
                Route::delete('/{id}', 'CustomerController@destroy');
            });

            Route::group(['prefix' => 'gas-prices'], function() {
                Route::get('/index.{format}', 'GasPriceController@index');
                Route::post('/', 'GasPriceController@store');
                Route::delete('/{id}', 'GasPriceController@destroy');
            });

            Route::group(['prefix' => 'help'], function() {
                Route::get('/index.{format}', 'HelpController@index');
            });

            Route::get('/', 'HomeController@index');

            Route::group([ 'prefix' => 'system-configuration' ], function(){
                Route::get('/index.html', 'SystemConfigurationController@index');
                Route::put('/index.html', 'SystemConfigurationController@update');
            });
        });

    });
});

Route::group(['prefix' => 'api', 'middleware' => 'api'], function() {
    Route::group(['namespace' => 'Api'], function() {
            Route::group(['prefix' => 'user'], function(){
            Route::post('/create', 'UserController@postCreate');
        });

        Route::group(['prefix' => 'oauth'], function(){

        });

        Route::group(['prefix' => 'admin'], function() {

            Route::group(['prefix' => 'auth'], function() {
                Route::post('/login', 'AdminAuthController@postLogin');
                Route::get('/logout', 'AdminAuthController@getLogout');
            });

            Route::group( ['prefix' => 'dashboard'], function() {
                Route::get('/index.{format}', 'AdminDashboardController@index');
                Route::put('/', 'AdminDashboardController@update');
            } );

        });

        Route::group(['prefix' => 'user'], function() {

            Route::group(['prefix' => 'auth'], function() {
                Route::post('/login', 'UserAuthController@postLogin');
                Route::get('/logout', 'UserAuthController@getLogout');
                Route::get('/check', 'UserAuthController@getCheck');

                Route::group(['prefix' => 'me', 'middleware' => 'auth.api:user'], function(){

                    Route::put('/', 'UserController@putUpdate');
                    Route::get('/gas-consumption', 'AuthenticatedUserController@getGasConsumption');
                    Route::get('/payment-history', 'AuthenticatedUserController@getPaymentHistory');
                    Route::get('/financing', 'AuthenticatedUserController@getFinancing');
                    Route::get('/last-billing', 'AuthenticatedUserController@getLastBilling');
                    Route::get('/app-components', 'AuthenticatedUserController@getAppComponents');

                    Route::get('/', 'UserAuthController@getMe');

                    Route::group(['prefix' => 'coupons'], function() {
                        Route::get('/', 'UserCouponController@index');
                    });

                    Route::group(['prefix' => 'events'], function() {
                        Route::get('/', 'UserEventController@index');
                        Route::post('/{event_id}/participations', 'UserEventParticipationController@store');
                        Route::delete('/{event_id}/participations', 'UserEventParticipationController@destroy');
                    });

                });
            });

        });

        Route::group(['prefix' => 'merchant'], function() {

            Route::group(['prefix' => 'auth'], function() {
                Route::post('/login', 'MerchantAuthController@postLogin');
                Route::get('/logout', 'MerchantAuthController@getLogout');
                Route::get('/check', 'MerchantAuthController@getCheck');
                Route::get('/me', 'MerchantAuthController@getMe');
            });

            Route::group(['prefix' => 'coupons'], function() {
                Route::get('/{id}', 'CouponController@getCoupon');
                Route::post('/swap', 'CouponController@postSwap');
            });
        });
    });

    Route::get('/contact-information', 'ContactInformationController@index');

    Route::group(['prefix' => 'tips'], function(){
            Route::get('', 'TipController@index');
            Route::post('', 'TipController@store');
            Route::get('/{id}', 'TipController@show');
            Route::put('/{id}', 'TipController@update');
            Route::delete('/{id}', 'TipController@destroy');
    });

    Route::group(['prefix' => 'events'], function(){
            Route::get('', 'EventController@index');
            Route::post('', 'EventController@store');
            Route::get('/{id}', 'EventController@show');
            Route::put('/{id}', 'EventController@update');
            Route::delete('/{id}', 'EventController@destroy');
    });

    Route::group(['prefix' => 'companies'], function(){
            Route::get('', 'CompanyController@index');
            Route::post('', 'CompanyController@store');
            Route::get('/{id}', 'CompanyController@show');
            Route::put('/{id}', 'CompanyController@update');
            Route::delete('/{id}', 'CompanyController@destroy');
    });

    Route::group(['prefix' => 'suggestions'], function(){
            Route::get('', 'SuggestionController@index');
            Route::post('', 'SuggestionController@store');
            Route::get('/{id}', 'SuggestionController@show');
            Route::put('/{id}', 'SuggestionController@update');
            Route::delete('/{id}', 'SuggestionController@destroy');
    });

    Route::group(['prefix' => 'ubigeo'], function(){
        Route::get('/departaments', 'UbigeoController@departaments');
        Route::get('/departaments/{id}', 'UbigeoController@showDepartament');
    });

    Route::group(['prefix' => 'app-components'], function(){
            Route::get('/sections', 'AppComponentController@sections');
            Route::get('', 'AppComponentController@index');
            Route::post('', 'AppComponentController@store');
            Route::get('/{id}', 'AppComponentController@show');
            Route::put('/{id}', 'AppComponentController@update');
            Route::delete('/{id}', 'AppComponentController@destroy');
    });

    Route::group(['prefix' => 'coupons'], function(){
            Route::get('/categories', 'CouponCategoryController@index');
            Route::get('', 'CouponController@index');
            Route::post('', 'CouponController@store');
            Route::get('/{id}', 'CouponController@show');
            Route::put('/{id}', 'CouponController@update');
            Route::delete('/{id}', 'CouponController@destroy');
    });

    Route::group(['prefix' => 'quotation-requests'], function(){
            Route::get('', 'QuotationRequestController@index');
            Route::post('', 'QuotationRequestController@store');
            Route::get('/{id}', 'QuotationRequestController@show');
            Route::put('/{id}', 'QuotationRequestController@update');
            Route::delete('/{id}', 'QuotationRequestController@destroy');
    });

    /*
    Contugas's gateway
    */
    Route::post('customer/validate', 'ContugasGatewayController@postCustomerValidate');
});
