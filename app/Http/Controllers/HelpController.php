<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;

class HelpController extends Controller
{
    public function index() {
    	return response()->view('help.index');
    }
}
