<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;
use Contugas\Models\AdminDashboard;

class AdminDashboardController extends Controller
{
    public function index(Request $request, $format = 'json')
    {
        $user = multiAuth('admin')->user();

    	$dashboard = $user->dashboard;

    	if( !isset($dashboard) ){
    		$dashboard = AdminDashboard::create([
                                                'admin_id' => $user->id, 
                                                'new_quotation_requests' => 0,
                                                'new_suggestions' => 0
                                                ]);
    	}

    	$data = $dashboard;

    	if($format == 'json'){
    		return response()->json($data);
    	}
    }

    public function update(Request $request, $format = 'json')
    {
    	$dashboard = multiAuth('admin')->user()->dashboard;

        if( isset($dashboard) ) {

            if( $request->has('amount_of_decrease_of_quotation_requests') ){
                $dashboard->new_quotation_requests = $dashboard->new_quotation_requests - ( int ) $request->amount_of_decrease_of_quotation_requests;
            }

            if( $request->has('amount_of_decrease_of_suggestions') ){
                $dashboard->new_suggestions = $dashboard->new_suggestions - ( int ) $request->amount_of_decrease_of_suggestions;
            }

            $dashboard->save();
        }

    	if( $format == 'json' ){
    		return response()->json( $dashboard );
    	}
    }
}
