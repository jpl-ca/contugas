<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

use Log;

use Contugas\Models\AppComponent;
use Contugas\Libraries\Pusher;


class AppComponentController extends Controller
{
	public function index(Request $request, $format = 'json')
    {
        $items = AppComponent::orderBy('created_at', 'desc')->simplePaginate(20);
        
        if ($request->has('fields')) {
            $fields = explode(',', $request->fields);
            if(in_array('ubigeo', $fields)){
                $items->load('ubigeo');
            }
        }
        
        if($format == 'json'){
            $data = $items;
            return response()->json($data);
        }else if($format == 'html'){
            return view('app-components.index', compact('items'));
        }else if($format == 'xls'){
            Excel::create('Mensaje del App', function($excel) use ($items) {
                $excel->sheet('mensajes del app', function($sheet) use ($items) {
                    $i = 1;
                    $sheet->row($i, ['id',
                                     'Sección',
                                     'Descripción',
                                     'Código ubigeo',
                                     'URL de la Imagen',
                                     'Fecha inicio',
                                     'Fin',
                                     'Creación',
                                     ]);
                    foreach ($items as $item) {
                        $i++;
                        $sheet->row($i, [
                                            $item->id,
                                            $item->label,
                                            $item->description,
                                            ( $item->ubigeo ? $item->ubigeo->name : 'Sin ubigeo' ),
                                              $item->image_url,
                                              $item->starts,
                                              $item->ends,
                                              $item->created_at
                                        ]);
                    }
                });

            })->download();
        }
    }

    public function create(Request $request, $format = 'html')
    {
        return view('app-components.create');
    }

    public function store(Request $request, $format = 'json')
    {   
        $list_of_sections = [];
        
        foreach(AppComponent::sections() as $section){
            $list_of_sections[] =  $section['machine_name'];
        }
         $list_of_sections = join(',', $list_of_sections);

        $validator = Validator::make($request->all(), [
                'section' => 'required|in:'.$list_of_sections,
                'image_file' => 'required|mimes:jpeg,png,bmp',
                'ubigeo_code' => 'exists:ubigeo,code',
            ]);

        if($validator->fails()){
            return response()->json([
                    'message' => 'Error al crear',
                    'errors' => $validator->errors()
                ], 422);
        }

        $item = new AppComponent;
        $item->section = $request->section;
        $item->description = $request->description;
        $item->ubigeo_code = $request->ubigeo_code;
        $item->starts = $request->starts;
        $item->ends = $request->ends;
        $item->save();

        if($request->hasFile('image_file')){
            $request->file('image_file')->move(AppComponent::files_path(), $item->id);
            $item->image_path = AppComponent::prepareImageUrl($item->id);
            $item->save();
        }

        $pusher = new Pusher();
        $pusher->addRecipent('/topics/banners');
        $pusher->addData( [ 
            "message" => "Se han actualizado los banners",
            "success" => true ] );
        $pusher->send();
        Log::info("Pushing notification");

        if($format == 'json'){
            return response()->json([
                    "message" => "Item creado exitosamente",
                    "data" => [
                        "item" => $item
                    ]
                ]);
        }
    }

    public function show(Request $request, $id, $format = 'json')
    {
        $item = AppComponent::findOrFail($id);
        $item->ubigeo;
        if($format == 'json'){
            return response()->json([
                    "data" => [
                        "item" => $item->toArray()
                    ]
                ]);
        }else{
            return view('app-components.show');
        }
    }
    
    public function edit(Request $request, $id, $format = 'html')
    {
        $item = AppComponent::find($id);
        return view('app-components.edit', compact('item'));
    }

    public function update(Request $request, $id, $format = 'json')
    {
        $validator = Validator::make($request->all(), [
                'section' => 'required',
                'image_file' => 'mimes:jpeg,png,bmp',
                'ubigeo_code' => 'exists:ubigeo,code'
            ]);

        if($validator->fails()){
            return response()->json([
                    'message' => 'Error al actualizar item',
                    'errors' => $validator->errors()
                ], 422);
        }

        $item = AppComponent::findOrFail($id);
        $item->section = $request->section;
        $item->description = $request->description;
        $item->ubigeo_code = $request->ubigeo_code;
        $item->starts = $request->starts;
        $item->ends = $request->ends;

        $item->save();

        if($request->hasFile('image_file')){
            $request->file('image_file')->move(AppComponent::files_path(), $item->id);
            $item->image_path = AppComponent::prepareImageUrl($item->id);
            $item->save();
        }

        $pusher = new Pusher();
        $pusher->addRecipent('/topics/banners');
        $pusher->addData( [ 
            "message" => "Se han actualizado los banners",
            "success" => true ] );
        $pusher->send();
        Log::info("Pushing notification");

        if($format == 'json'){
            return response()->json([
                    "message" => "Item actualizado exitosamente",
                    "data" => [
                        "item" => $item
                    ]
                ]);
        }
    }

    public function destroy(Request $request, $id, $format = 'json')
    {
        $item = AppComponent::findOrFail($id);
        $item->removeImageFileFromDisk();
        $item->delete();
        if($format == 'json'){
            return response()->json([
                    'message' => 'Eliminado exitosamente',
                ]
                );
        }
    }

    public function sections(Request $request, $format = 'json')
    {
    
        $list = AppComponent::sections();

        if($format == 'json'){
            return response()->json([
                    'message' => 'Lista obtenida exitosamente',
                    'data' => $list
                ]);
        }
    }
}
