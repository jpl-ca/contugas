<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Auth;
use Contugas\Http\Requests;
use Contugas\Models\Admin;

class AdminUserController extends Controller
{
    public function index(Request $request, $format = 'json')
    {
        $items = Admin::orderBy('created_at', 'desc');
        $items = $items->simplePaginate(20);

        // Rendering
        if($format == 'json'){
            $data = $items;
            return response()->json($data);
        }else if($format == 'html'){

            return view('admin-users.index', compact('items'));
        }
    }

    public function create(Request $request, $format = 'html')
    {
        return view('admin-users.create');
    }

    public function store(Request $request, $format = 'json', $type = 'event')
    {   
        $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:admins,email',
                'password' => 'required|min:4'
            ]);

        if($validator->fails()){
            return response()->json([
                    'message' => 'Error al crear',
                    'errors' => $validator->errors()
                ], 422);
        }

        $item = new Admin;
        $item->name = $request->name;
        $item->email = $request->email;
        $item->password = \Hash::make($request->password);
        $item->save();

        if($format == 'json'){
            return response()->json([
                    "message" => "Item creado exitosamente",
                    "data" => [
                        "item" => $item
                    ]
                ]);
        }
    }

    public function show(Request $request, $id, $format = 'json')
    {
        $item = Admin::findOrFail($id);

        if($format == 'json'){
            return response()->json([
                    "data" => [
                        "item" => $item->toArray()
                    ]
                ]);
        }else{
            return view('events.show');
        }
    }
    
    public function edit(Request $request, $id, $format = 'html')
    {
        $item = Admin::find($id);
        return view('admin-users.edit', compact('item'));
    }

    public function update(Request $request, $id, $format = 'json')
    {
        $validator = Validator::make($request->all(), [
                'password' => 'min:4'
            ]);

        if($validator->fails()){
            return response()->json([
                    'message' => 'Error al actualizar item',
                    'errors' => $validator->errors()
                ], 422);
        }

        $item = Admin::findOrFail($id);

        if($request->exists('name')){
            $item->name = $request->name;    
        }
        
        if($request->exists('email')){
            $item->email = $request->email;    
        }

        if($request->exists('password')){
            $item->password = \Hash::make($request->password);
        }

        $item->save();

        if($format == 'json'){
            return response()->json([
                    "message" => "Item actualizado exitosamente",
                    "data" => [
                        "item" => $item
                    ]
                ]);
        }
    }

    public function destroy(Request $request, $id, $format = 'json')
    {
        $item = Admin::findOrFail($id);
        $item->delete();
        if($format == 'json'){
            return response()->json([
                    'message' => 'Eliminado exitosamente',
                ]
                );
        }
    }

}
