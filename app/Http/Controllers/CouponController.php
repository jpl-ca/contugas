<?php

namespace Contugas\Http\Controllers;

use Contugas\Http\Requests\StoreCouponRequest;
use Contugas\Models\Coupon;
use Contugas\Models\CouponStatus;
use Contugas\Models\UbigeoCouponConstraint;
use Contugas\Models\Merchant;
use Illuminate\Http\Request;
use Validator;
use Contugas\Http\Requests;
use Contugas\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class CouponController extends Controller
{

    public function index(Request $request, $format = 'json')
    {
        $items = Coupon::orderBy('created_at', 'desc')->simplePaginate(20);

        if ($request->has('fields')) {
            $fields = explode(',', $request->fields);

            if( in_array('company', $fields) ){
                $items->load('company');
            }
        }

        if($format == 'json'){
            $data = $items;
            return response()->json($data);
        }else if($format == 'html'){
            return view('coupons.index', compact('items'));
        }else if($format == 'xls'){
            Excel::create('Cupones', function($excel) use ($items) {
                $excel->sheet('Cupones', function($sheet) use ($items) {
                    $sheet->row(1, ['Código','', '', 'Título', 'Descripción']);
                    $sheet->fromArray($items->toArray()['data'], null, 'A2', false, false);
                });

            })->download();
        }
    }

    public function create(Request $request, $format = 'html')
    {
        return view('coupons.create');
    }

    public function store(Request $request, $format = 'json')
    {   
        $coupon_status_list = join(',', CouponStatus::all());
        $validator = Validator::make($request->all(), [
                'company_id' => 'exists:companies,id',
                'coupon_category_id' => 'exists:coupon_categories,id',
                'status' => 'in:'.$coupon_status_list
            ]);

        if($validator->fails()){
            return response()->json([
                    'message' => 'Error al crear',
                    'errors' => $validator->errors()
                ], 422);
        }

        $item = new Coupon;
        $item->id = Coupon::generateCode();
        $item->title = $request->title;
        $item->description = $request->description;
        $item->company_id = $request->company_id;
        $item->user_swaps = $request->user_swaps;
        $item->max_swaps = $request->max_swaps;
        $item->start_date = $request->start_date;
        $item->end_date = $request->end_date;
        $item->status = $request->status;
        $item->user_swaps = $request->user_swaps;
        $item->max_swaps = $request->max_swaps;
        $item->coupon_category_id = $request->coupon_category_id;
        $item->save();

        if($request->hasFile('image_file')){
            $request->file('image_file')->move(Coupon::files_path(), $item->id);
            $item->image_path = Coupon::prepareImageUrl($item->id);
            $item->save();
        }

        if($request->has('ubigeo_coupon_constraints')){
            if(is_array($request->ubigeo_coupon_constraints)){
                foreach($request->ubigeo_coupon_constraints as $ubigeo_coupon_constraint){
                    $constraint = new UbigeoCouponConstraint;
                    $constraint->coupon_id = $item->id;
                    $constraint->ubigeo_code = $ubigeo_coupon_constraint['ubigeo_code'];
                    $constraint->save();
                }
            }
        }

        if($format == 'json'){
            return response()->json([
                    "message" => "Item creado exitosamente",
                    "data" => [
                        "item" => $item
                    ]
                ]);
        }
    }

    public function show(Request $request, $id, $format = 'json')
    {
        $item = Coupon::findOrFail($id);

        if($request->has('fields')){
            $fields = explode(',', $request->fields);

            if(in_array('ubigeo_coupon_constraints', $fields)){
                $item->ubigeoCouponConstraints;
            }

            if (in_array('company', $fields)) {
                $item->company;
            }
        }

        if($format == 'json'){
            return response()->json([
                    "data" => [
                        "item" => $item->toArray()
                    ]
                ]);
        }else{
            return view('coupons.show', ['item' => $item]);
        }
    }
    
    public function edit(Request $request, $id, $format = 'html')
    {
        $item = Coupon::find($id);
        return view('coupons.edit', compact('item'));
    }

    public function update(Request $request, $id, $format = 'json')
    {
        $coupon_status_list = join(',', CouponStatus::all());
        $validator = Validator::make($request->all(), [
                'company_id' => 'exists:companies,id',
                'status' => 'in:'.$coupon_status_list
            ]);

        if($validator->fails()){
            return response()->json([
                    'message' => 'Error al crear',
                    'errors' => $validator->errors()
                ], 422);
        }

        $item = Coupon::findOrFail($id);

        if($request->exists('title')){
            $item->title = $request->title;    
        }

        if($request->exists('description')){
            $item->description = $request->description;
        }

        if($request->exists('company_id')){
            $item->company_id = $request->company_id;
        }
        
        if($request->exists('user_swaps')){
            $item->user_swaps = $request->user_swaps;
        }
        
        if($request->exists('max_swaps')){
            $item->max_swaps = $request->max_swaps;
        }

        if($request->exists('start_date')){
            $item->start_date = $request->start_date;
        }
        
        if($request->exists('end_date')){
            $item->end_date = $request->end_date;
        }

        if($request->exists('status')){
            $item->status = $request->status;
        }

        if($request->exists('coupon_category_id')){
            $item->coupon_category_id = $request->coupon_category_id;    
        }

        $item->save();

        if($request->hasFile('image_file')){
            $request->file('image_file')->move(Coupon::files_path(), $item->id);
            $item->image_path = Coupon::prepareImageUrl($item->id);
            $item->save();
        }

        if($request->has('ubigeo_coupon_constraints')){
            $item->ubigeoCouponConstraints()->delete();
            if(is_array($request->ubigeo_coupon_constraints)){
                foreach($request->ubigeo_coupon_constraints as $ubigeo_coupon_constraint){
                    $constraint = new UbigeoCouponConstraint;
                    $constraint->coupon_id = $item->id;
                    $constraint->ubigeo_code = $ubigeo_coupon_constraint['ubigeo_code'];
                    $constraint->save();
                }
            }
        }


        if($format == 'json'){
            return response()->json([
                    "message" => "Item actualizado exitosamente",
                    "data" => [
                        "item" => $item
                    ]
                ]);
        }
    }

    public function destroy(Request $request, $id, $format = 'json')
    {
        $item = Coupon::findOrFail($id);
        $item->removeImageFileFromDisk();
        $item->delete();
        if($format == 'json'){
            return response()->json([
                    'message' => 'Eliminado exitosamente',
                ]
                );
        }
    }
}
