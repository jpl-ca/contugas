<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Maatwebsite\Excel\Facades\Excel;

use Contugas\Http\Requests;

use Contugas\Models\User;

class CustomerController extends Controller
{
    public function index(Request $request, $format)
    {
    	$items = User::orderBy('created_at', 'desc');

        if ($request->has('filter_field') && $request->has('filter_value')){
            $items = $items->where($request->filter_field, 'LIKE', '%' . $request->filter_value . '%');
        }

        //return response()->json($request->all());
        $items = $items->simplePaginate(20);

    	switch ($format) {
    		case 'html':
    			return view('customers.index');
    			break;
    		case 'xls':
                Excel::create('Clientes', function($excel) use ($items) {
                    $excel->sheet('clientes', function($sheet) use ($items) {
                        $sheet->row(1, ['# Cliente', 'Nombre', 'E-mail', 'Provincia', 'District', 'Última conexión', 'Fecha de registro']);
                        $i = 2;

                        foreach ($items as $item) {
                            $province = ($item->province ? $item->province->name : null);
                            $district = ($item->district ? $item->district->name : null);
                            $sheet->row($i, [
                                    $item->client_number,
                                    $item->name,
                                    $item->email,
                                    $province,
                                    $district,
                                    $item->last_login,
                                    $item->created_at
                                ]);
                            $i++;
                        }

                    });
                })->download();
    			break;
    		case 'json':
    			return response()->json($items);
    			break;
    	}
    }

    public function destroy(Request $request, $id)
    {
        $item = User::findOrFail($id);
        $item->delete();

        $json = [
                'message' => 'Eliminado exitosamente'
            ];

        return response()->json($json);
    }
}
