<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use Symfony\Component\HttpFoundation\Response;

use Contugas\Http\Requests;
use Contugas\Models\AppComponent;
use Carbon\Carbon;
use Storage;

use Contugas\Models\GasPrice;

class AuthenticatedUserController extends Controller
{
    public function getGasConsumption(Request $request)
    {
        $customer_number = multiAuth('user')->user()->client_number;

        $http_client = new \GuzzleHttp\Client();
        $http_result = $http_client->request('POST', config('app.contugas_gateway_base_url').'/gas-consumption',[
                "headers" => [
                        'Content-Type' => 'application/json; charset=utf-8'
                    ],
                    'body' => '{"customer_number" : "' . $customer_number . '"}'
                ]
        );

        //Adding data
        $json = json_decode($http_result->getBody(), true);
        $totalSavedMoney = 0;
        if (isset($json['data']) && is_array($json['data'])) {
            $prices = GasPrice::lastPrices();
            foreach ($json['data'] as $key => $itemInvoice) {
                foreach ($prices as $itemPrice) {
                    $monthOfInvoice = (int)$itemInvoice['month'];
                    $yearOfInvoice = (int)$itemInvoice['year'];

                    $monthOfPrice = (int)$itemPrice['month'];
                    $yearOfPrice = (int)$itemPrice['year'];
                    
                    if($monthOfPrice == $monthOfInvoice && $yearOfPrice == $yearOfInvoice) {
                        $totalInvoice = (float)$itemInvoice['total_invoice'];
                        $anotherPrice = (float)$itemInvoice['volume_invoice'] * $itemPrice['value'];
                        $saved =  $anotherPrice - $totalInvoice;
                        $totalSavedMoney += $saved;
                        $json['data'][$key]['saved'] = $saved;
                    }
                }
            }
        }

        if ($totalSavedMoney < 0) {
            $totalSavedMoney = '0.00';
        }

        $json['saved_money'] = "Al cambiarse a Contugas usted ha ahorrado S/. {$totalSavedMoney} en los últimos seis meses";
        $response = new Response(json_encode($json), $http_result->getStatusCode(), ['Content-Type' => $http_result->getHeaderLine('Content-Type') ]);
        return $response;
    }

    public function getPaymentHistory(Request $request)
    {
        $customer_number = multiAuth('user')->user()->client_number;

        $http_client = new \GuzzleHttp\Client();
        $http_result = $http_client->request('POST', config('app.contugas_gateway_base_url').'/payment-history',[
                "headers" => [
                        'Content-Type' => 'application/json; charset=utf-8'
                    ],
                'body' => '{"customer_number" : "' . $customer_number . '"}'
            ]
        );

        $response = new Response($http_result->getBody(), $http_result->getStatusCode(), ['Content-Type' => $http_result->getHeaderLine('Content-Type') ]);
        return $response;
    }

    public function getFinancing(Request $request)
    {
        $customer_number = multiAuth('user')->user()->client_number;

        $http_client = new \GuzzleHttp\Client();
        $http_result = $http_client->request('POST', config('app.contugas_gateway_base_url').'/financing',[
                "headers" => [
                        'Content-Type' => 'application/json; charset=utf-8'
                    ],
                'body' => '{"customer_number" : "' . $customer_number . '"}'
            ]
        );

        $response = new Response($http_result->getBody(), $http_result->getStatusCode(), ['Content-Type' => $http_result->getHeaderLine('Content-Type') ]);
        return $response;
    }

    public function getLastBilling(Request $request)
    {
        $customer_number = multiAuth('user')->user()->client_number;
        $format = $request->has('format') ? $request->format : 'json'; 
        $http_client = new \GuzzleHttp\Client();

        $http_result = $http_client->request('POST', config('app.contugas_gateway_base_url').'/last-billing', [
                    "headers" => [
                        'Content-Type' => 'application/json'
                    ],
                    "body" => '{"customer_number":"'.$customer_number.'"}'
                ]
            );
        
        $data = json_decode($http_result->getBody(), true);

        if(isset($data['data']) && isset($data['data'][0]) &&isset($data['data'][0]['url'])){
                
                $http_result = $http_client->request('GET', $data['data'][0]['url']);
                $unique_pdf_file_name = uniqid(10).'.pdf';
                $pdf_path= 'invoices/'.$unique_pdf_file_name;
                $pdf_url= url('temp/invoices/'.$unique_pdf_file_name);
                Storage::put($pdf_path, $http_result->getBody());

                $data = [
                    "message" => "Recibo generado exitosamente",
                    "data" => [
                        "url" => $pdf_url
                    ]
                ];

                return response()->json($data);
        }else{
            $response = new Response($http_result->getBody(), $http_result->getStatusCode(), ['Content-Type' => $http_result->getHeaderLine('Content-Type') ]);
            return $response;
        }  
    }

    public function getAppComponents(Request $request, $format = 'json')
    {
        $user = multiAuth('user')->user();
        $now = Carbon::now();
        $query = AppComponent::orderBy('created_at', 'asc')
                    ->where('ends', '>', $now)
                    ->where('starts', '<', $now);

        if( isset( $user->province_id ) ) {
            $query = $query->where( 'ubigeo_code', $user->province_id);
        }

        $items = $query->get();

        $data = [];
        foreach( $items as $item ) {
            $data[ $item["section"] ] = $item["image_url"];
        }

        //Adding keys
        foreach (AppComponent::sections() as $section) {
            if ( !array_key_exists($section["machine_name"], $data) ){
                $data[ $section["machine_name"] ] = null;
            }
        }

        if( $format == 'json' ) {
            $data = [
                "message" => "Componentes de aplicación obtenidos exitosamente.",
                "data" => $data
            ];

            return response()->json($data);
        }
    }

}
