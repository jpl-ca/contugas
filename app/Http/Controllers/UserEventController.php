<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;
use Contugas\Models\Event;

use Carbon\Carbon;

class UserEventController extends Controller
{
	public function index(Request $request, $format = 'json')
	{
        $current_user = multiAuth('user')->user();

		$items = Event::orderBy('created_at', 'desc');

        if($request->has("type") && in_array($request->type, Event::types() )){
            $items = $items->where('type', $request->type);
        }

        if ($items) {
            $items = $items->where('date', '>', Carbon::now());
        }

        $items = $items->simplePaginate(20);
        foreach ($items as $item) {
        	$item->attending = ( $item->participations()->where('user_id', $current_user->id)->count() > 0 );
        }

        if($format == 'json'){
            $data = $items;
            return response()->json($data);
        }else if($format == 'html'){
            return view('events.index', compact('items'));
        }else if($format == 'xls'){
            Excel::create('Eventos', function($excel) use ($items) {
                $excel->sheet('eventos', function($sheet) use ($items) {
                    $sheet->row(1, ['id','Titulo', '', 'Creación', 'Actualización']);
                    $sheet->fromArray($items->toArray()['data'], null, 'A2', false, false);
                });

            })->download();
        }
	}
}
