<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;
use Contugas\Models\Suggestion;
use Validator;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class SuggestionController extends Controller
{
    public function index(Request $request, $format = 'json')
    {
        $items = Suggestion::orderBy('created_at', 'desc')->simplePaginate(20);

        if ($request->has('fields')) {

            $fields = explode(',',$request->fields);

            if (in_array('user', $fields)) {
                $items->load('user');
            }
        }

        if($format == 'json'){
            $data = $items;
            return response()->json($data);
        }else if($format == 'html'){
            return view('suggestions.index', compact('items'));
        }else if($format == 'xls'){
            Excel::create('Sugerencias', function($excel) use ($items) {
                $excel->sheet('suggestions', function($sheet) use ($items) {
                    $i = 1;
                    $sheet->row($i, [
                                    'id',
                                    'Creación',
                                    'Contenido',
                                    '# de cliente',
                                    'Nombre del cliente',
                                    ]
                                );
                    $i++;
                    foreach ($items as $item) {
                        $user = ($item->user ? $item->user->name : 'Anónimo');
                        $client_number = ($item->user ? $item->user->client_number : 'Anónimo');
                        $sheet->row($i, [
                                            $item->id,
                                            $item->created_at,
                                            $item->content,
                                            $client_number,
                                            $user
                                        ]);
                        $i++;
                    }
                    
                });

            })->download();
        }
    }

    public function create(Request $request, $format = 'html')
    {
        return view('suggestions.create');
    }

    public function store(Request $request, $format = 'json')
    {   
        $validator = Validator::make($request->all(), [
                'content' => 'required'
            ]);

        if($validator->fails()){
            return response()->json([
                    'message' => 'Error al crear',
                    'errors' => $validator->errors()
                ], 422);
        }

        if(multiAuth('user')->check()){
            $user_id = multiAuth('user')->user()->id;
        }

        $item = new Suggestion;
        $item->content = $request->content;
        $item->user_id = isset($user_id) ? $user_id :  null;
        $item->save();

        DB::update('update admin_dashboards set new_suggestions = new_suggestions + 1 ;');

        if($format == 'json'){
            return response()->json([
                    "message" => "Sugerencia registrada exitosamente",
                    "data" => [
                        "item" => $item
                    ]
                ]);
        }
    }

    public function show(Request $request, $id, $format = 'json')
    {
        $item = Suggestion::findOrFail($id);

        if($format == 'json'){
            return response()->json([
                    "data" => [
                        "item" => $item->toArray()
                    ]
                ]);
        }else{
            return view('suggestions.show');
        }
    }
    
    public function edit(Request $request, $id, $format = 'html')
    {
        $item = Suggestion::find($id);
        return view('suggestions.edit', compact('item'));
    }

    public function update(Request $request, $id, $format = 'json')
    {
        $validator = Validator::make($request->all(), [
                'content' => 'required'
            ]);

        if($validator->fails()){
            return response()->json([
                    'message' => 'Error al actualizar item',
                    'errors' => $validator->errors()
                ], 422);
        }

        $item = Suggestion::findOrFail($id);
        $item->content = $request->content;
        $item->save();

        if($format == 'json'){
            return response()->json([
                    "message" => "Item actualizado exitosamente",
                    "data" => [
                        "item" => $item
                    ]
                ]);
        }
    }

    public function destroy(Request $request, $id, $format = 'json')
    {
        $item = Suggestion::findOrFail($id);
        $item->delete();
        if($format == 'json'){
            return response()->json([
                    'message' => 'Eliminado exitosamente',
                ]
                );
        }
    }
}
