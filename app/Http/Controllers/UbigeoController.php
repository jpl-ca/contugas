<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;
use Contugas\Models\Ubigeo;

class UbigeoController extends Controller
{
    public function departaments(Request $request, $format = 'json')
    {
    	$items = Ubigeo::departaments();
    	if($request->has('fields')){
    		$fields = explode(',', $request->fields);
    		if(in_array('provinces', $fields)){
    			$items->with('provinces');
    		}

    		if(in_array('districts', $fields)){
    			$items->with('provinces.districts');
    		}
    	}

    	foreach ($items as $departament) {
    		$departament->provinces;
    	}
    	if($format == 'json'){
    		$data = [
    			"message" => "Lista de departamentos del Perú",
    			"data" => $items->get()
    		];

    		return response()->json($data);
    	}
    }

    public function showDepartament(Request $request,$id, $format = 'json')
    {
    	$item = Ubigeo::departaments()->where('code', '=', $id);
    	if($request->has('fields')){

    		$fields = explode(',', $request->fields);

    		if(in_array('provinces', $fields)){
    			$item = $item->with(array('provinces' => function($query) {
                    $query->orderBy('name', 'asc');
                }));
    		}

    		if(in_array("districts", $fields)){
    			$item->with(array('provinces.districts' => function($query) {
                    $query->orderBy('name', 'asc');
                }));
    		}
    	}

    	if($format == 'json'){
    		$data = [
    			"message" => "Lista de departamentos del Perú",
    			"data" => $item->first()
    		];

    		return response()->json($data);
    	}
    }
}
