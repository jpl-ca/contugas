<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;
use Validator;
use Contugas\Models\Tip;
use Maatwebsite\Excel\Facades\Excel;

class TipController extends Controller
{
    public function index(Request $request, $format = 'json')
    {
        $items = Tip::orderBy('created_at', 'desc')->simplePaginate(20);

        if($format == 'json'){
            $data = $items;
            return response()->json($data);
        }else if($format == 'html'){
            return view('tips.index', compact('items'));
        }else if($format == 'xls'){
            Excel::create('Consejos', function($excel) use ($items) {
                $excel->sheet('consejos', function($sheet) use ($items) {
                    $sheet->row(1, ['id','Titulo', 'Ruta imagen', 'Creación', 'Actualización', 'URL']);
                    $sheet->fromArray($items->toArray()['data'], null, 'A2', false, false);
                });

            })->download();
        }
    }

    public function create(Request $request, $format = 'html')
    {
        return view('tips.create');
    }

    public function store(Request $request, $format = 'json')
    {   
        $validator = Validator::make($request->all(), [
                'title' => 'required'
            ]);

        if($validator->fails()){
            return response()->json([
                    'message' => 'Error al crear',
                    'errors' => $validator->errors()
                ], 422);
        }

        $item = new Tip;
        $item->title = $request->title;
        $item->save();

        if($request->hasFile('image_file')){
            $request->file('image_file')->move(Tip::files_path(), $item->id);
            $item->image_path = Tip::prepareImageUrl($item->id);
            $item->save();
        }

        if($format == 'json'){
            return response()->json([
                    "message" => "Item creado exitosamente",
                    "data" => [
                        "item" => $item
                    ]
                ]);
        }
    }

    public function show(Request $request, $id, $format = 'json')
    {
        $item = Tip::findOrFail($id);

        if($format == 'json'){
            return response()->json([
                    "data" => [
                        "item" => $item->toArray()
                    ]
                ]);
        }else{
            return view('tips.show');
        }
    }
    
    public function edit(Request $request, $id, $format = 'html')
    {
        $item = Tip::find($id);
        return view('tips.edit', compact('item'));
    }

    public function update(Request $request, $id, $format = 'json')
    {
        $validator = Validator::make($request->all(), [
                'title' => 'required'
            ]);

        if($validator->fails()){
            return response()->json([
                    'message' => 'Error al actualizar item',
                    'errors' => $validator->errors()
                ], 422);
        }

        $item = Tip::findOrFail($id);
        $item->title = $request->title;
        $item->save();

        if($request->hasFile('image_file')){
            $request->file('image_file')->move(Tip::files_path(), $item->id);
            $item->image_path = Tip::prepareImageUrl($item->id);
            $item->save();
        }


        if($format == 'json'){
            return response()->json([
                    "message" => "Item actualizado exitosamente",
                    "data" => [
                        "item" => $item
                    ]
                ]);
        }
    }

    public function destroy(Request $request, $id, $format = 'json')
    {
        $item = Tip::findOrFail($id);
        $item->removeImageFileFromDisk();
        $item->delete();
        if($format == 'json'){
            return response()->json([
                    'message' => 'Eliminado exitosamente',
                ]
                );
        }
    }
}