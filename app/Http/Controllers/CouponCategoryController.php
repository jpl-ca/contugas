<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;
use Contugas\Models\CouponCategory;
class CouponCategoryController extends Controller
{
    public function index(Request $request, $format = 'json')
    {
    	$items = CouponCategory::orderBy('created_at', 'desc')->simplePaginate(20);

        if($format == 'json'){
            $data = $items;
            return response()->json($data);
        }
    }
}
