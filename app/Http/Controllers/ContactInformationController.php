<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;

class ContactInformationController extends Controller
{
    public function index()
    {
        $data = json_decode('
{
  "payment_places": [
    {
      "name": "CAC Ica",
      "address": "Av. Matías Manzanilla N° 200 Ica Cercado",
      "lat": -14.064547,
      "lng": -75.734161,
      "phone": "-",
      "attention_hour": "Lunes a Viernes: 08:00 am a 01:00 pm y 02:00 pm a 05:00 pm\nSábados: 09:00 am a 01:00 pm"
    },
    {
      "name": "CAC Pisco",
      "address": "Calle Pérez Figuerola N° 270 Pisco Cercado",
      "lat": -13.71063,
      "lng": -76.202153,
      "phone": "-",
      "attention_hour": "Lunes a Viernes: 08:00 am a 01:00 pm y 02:00 pm a 05:00 pm\nSábados: 09:00 am a 01:00 pm"
    },
    {
      "name": "CAC Chincha",
      "address": "Calle Lima N° 390 - Chincha Alta",
      "lat": -13.419277,
      "lng": -76.133665,
      "phone": "-",
      "attention_hour": "Lunes a Viernes: 08:00 am a 01:00 pm y 02:00 pm a 05:00 pm\nSábados: 09:00 am a 01:00 pm"
    }
  ],
  "attention_places": [
    {
      "name": "PAC Nasca",
      "address": "Calle Lima N° 392.",
      "lat": -14.829051,
      "lng": -74.93914,
      "phone": "-",
      "attention_hour": "Lunes a Viernes: 09:00 am a 01:00 pm y 02:00 pm a 06:00 pm\nSábados: 09:00 am a 01:00 pm"
    },
    {
      "name": "PAC Marcona",
      "address": "Av. Andrés Avelino Caceres N° 224",
      "lat": -15.362122,
      "lng": -75.16683,
      "phone": "-",
      "attention_hour": "Lunes a Viernes: 09:00 am a 01:00 pm y 02:00 pm a 06:00 pm\nSábados: 09:00 am a 01:00 pm"
    }
  ],
  "caja_ica_places": [
    {
      "region_name": "Apurímac",
      "region_places": [
        {
          "name": "Agencia Abancay",
          "address": "Jr. Arica Nº 120",
          "city": "Abancay",
          "lat": -13.635751,
          "lng": -72.880542,
          "phone": "-",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        },
        {
          "name": "Agencia Andahuaylas",
          "address": "Jr. Ramón Castilla Nº 438",
          "city": "Andahuaylas",
          "lat": -13.656771,
          "lng": -73.389827,
          "phone": "-",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        },
        {
          "name": "Of. Esp. Uripa",
          "address": "Av.Micaela Bastidas Nº 178 - Centro Poblado Uripa",
          "city": "Chincheros",
          "lat": -13.529364,
          "lng": -73.673929,
          "phone": "(083) 792-344",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        }
      ]
    },
    {
      "region_name": "Arequipa",
      "region_places": [
        {
          "name": "Agencia Camana",
          "address": "Av. Mariscal Castilla Nº 102",
          "city": "Camaná",
          "lat": -16.62307,
          "lng": -72.709532,
          "phone": "(054) 571-612",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        },
        {
          "name": "Of. Esp. Chala",
          "address": "Av. Emancipación Lote 12, Mz. 33",
          "city": "Caravelí",
          "lat": -15.851581,
          "lng": -74.252416,
          "phone": "-",
          "attention_hour": "Lunes a Viernes: 9:00 am a 1:00 pm y de 3:40 pm a 6:30 pm."
        },
        {
          "name": "Agencia Aplao",
          "address": "Calle Progreso Nº 107",
          "city": "Castilla",
          "lat": -16.412163,
          "lng": -71.573759,
          "phone": "(054) 471-367",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        },
        {
          "name": "Agencia Pedregal",
          "address": "El Pedregal Mz. 3EE Lote E02 - Centro Poblado El Pedregal",
          "city": "Caylloma",
          "lat": -16.354029,
          "lng": -72.188611,
          "phone": "(054) 586-508",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        }
      ]
    },
    {
      "region_name": "Ayacucho",
      "region_places": [
        {
          "name": "Agencia Huamanga",
          "address": "Portal Independencia Nº 64-65",
          "city": "Huamanga",
          "lat": -13.160885,
          "lng": -74.226368,
          "phone": "(066) 313-127",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        },
        {
          "name": "Agencia San Juan Bautista",
          "address": "Av. Ramón Castilla Nº 490",
          "city": "Huamanga",
          "lat": -13.173436,
          "lng": -74.223589,
          "phone": "-",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:15 pm"
        },
        {
          "name": "Of. Esp. San Francisco",
          "address": "Jr. Unión Nº 110 - San Francisco",
          "city": "Huamanga",
          "lat": -13.157733,
          "lng": -74.220391,
          "phone": "(066) 325 - 042  Anexo 59202",
          "attention_hour": "Lunes a Viernes: 9:00 am a 1:00 pm y de 3:30 pm a 6:30 pm."
        },
        {
          "name": "Agencia Puquio",
          "address": "Jr. Ayacucho Nº 248",
          "city": "Lucanas",
          "lat": -14.693422,
          "lng": -74.124127,
          "phone": "(066) 452 - 329",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        }
      ]
    },
    {
      "region_name": "Ica",
      "region_places": [
        {
          "name": "Agencia Chincha",
          "address": "Av. Mariscal Oscar Benavides Nº 299",
          "city": "Chincha",
          "lat": -13.417408,
          "lng": -76.135638,
          "phone": "(056) 263-329",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        },
        {
          "name": "Of. Esp. Pueblo Nuevo",
          "address": "Av. Mariscal Benavides Nº 700",
          "city": "Chincha",
          "lat": -13.416406,
          "lng": -76.139262,
          "phone": "(056) 504-608 / 267-693",
          "attention_hour": "Lunes a Viernes: 9:20 am a 6:15 pm"
        },
        {
          "name": "Agencia Cercado Ica",
          "address": "Ca. Castrovirreyna Nº 142 - Ica",
          "city": "Ica",
          "lat": -14.062661,
          "lng": -75.727826,
          "phone": "(056) 581-570",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:15 pm"
        },
        {
          "name": "Agencia Ica",
          "address": "Av. Municipalidad Nº 152",
          "city": "Ica",
          "lat": -14.063387,
          "lng": -75.728461,
          "phone": "(056) 581-430",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        },
        {
          "name": "Agencia Libertad",
          "address": "Calle Libertad Nº 291, Esquina con Av. San Martin Nº 201",
          "city": "Ica",
          "lat": -14.06488,
          "lng": -75.730386,
          "phone": "-",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        },
        {
          "name": "Agencia Parcona",
          "address": "Av. Pachacutec Yupanqui Nº 815",
          "city": "Ica",
          "lat": -14.04774,
          "lng": -75.706215,
          "phone": "(056) 526 - 448",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:15 pm"
        },
        {
          "name": "Agencia San Isidro",
          "address": "Av. San Martín C-104, Urb. San Isidro",
          "city": "Ica",
          "lat": -14.06446,
          "lng": -75.73069,
          "phone": "(056) 581-430",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        },
        {
          "name": "Of. Esp. La Tinguiña",
          "address": "Av. Victoria Gotuzzo N° 231",
          "city": "Ica",
          "lat": -14.03797,
          "lng": -75.70988,
          "phone": "-",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:15 pm"
        },
        {
          "name": "Of. Esp. Santiago",
          "address": "Carretera Panamericana Sur Km 318, Mza G - Lote 15",
          "city": "Ica",
          "lat": -14.185569,
          "lng": -75.714398,
          "phone": "-",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:15 pm"
        },
        {
          "name": "Agencia Marcona",
          "address": "Av. Andres Avelino Caceres N° 224 - Marcona",
          "city": "Nasca",
          "lat": -15.362086,
          "lng": -75.166753,
          "phone": "(056) 526 - 448",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        },
        {
          "name": "Agencia Nasca",
          "address": "Calle Lima Nº 392",
          "city": "Nasca",
          "lat": -14.828983,
          "lng": 74.939081,
          "phone": "(056) 522-693",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        },
        {
          "name": "Of. Esp. Palpa",
          "address": "Portal Escribanos Nº 131",
          "city": "Palpa",
          "lat": -14.53396,
          "lng": -75.1851,
          "phone": "(056) 581 - 570",
          "attention_hour": "Lunes a Viernes: 9:00 am a 1:00 pm y de 3:40 pm a 6:30 pm."
        },
        {
          "name": "Agencia Pisco",
          "address": "Av. Perez Figuerola Nº 218 - 222. Referencia Ex Reniec",
          "city": "Pisco",
          "lat": -13.710554,
          "lng": -76.202257,
          "phone": "(056) 534-476",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:15 pm"
        }
      ]
    },
    {
      "region_name": "Lima",
      "region_places": [
        {
          "name": "Agencia Barranca",
          "address": "Av. Grau Nº 118",
          "city": "Barranca",
          "lat": -10.754117,
          "lng": -77.761421,
          "phone": "(01) 235-2523",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        },
        {
          "name": "Agencia Imperial",
          "address": "Av. Ramos Nº 231",
          "city": "Cañete",
          "lat": -13.062148,
          "lng": -76.352371,
          "phone": "-",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:15 pm"
        },
        {
          "name": "Agencia Mala",
          "address": "Jr. Real Nº 378",
          "city": "Cañete",
          "lat": -12.657257,
          "lng": -76.633367,
          "phone": "(01) 530-8537",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        },
        {
          "name": "Agencia San Vicente",
          "address": "Calle Santa Rosa Nº 627",
          "city": "Cañete",
          "lat": -13.075879,
          "lng": -76.386232,
          "phone": "(01) 581 - 1337",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        },
        {
          "name": "Agencia Huaral",
          "address": "Calle Derecha Nº 328 - 332",
          "city": "Huaral",
          "lat": -11.495683,
          "lng": -77.207108,
          "phone": "(01) 246-1213",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        },
        {
          "name": "Agencia Huacho",
          "address": "Av. 28 de Julio Nº 166",
          "city": "Huaura",
          "lat": -11.493704,
          "lng": -77.205508,
          "phone": "(01) 232-1983 / 239-2822",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:45 pm"
        },
        {
          "name": "Agencia Lurin",
          "address": "Calle Bolognesi Nº 240",
          "city": "Lima",
          "lat": -12.27325,
          "lng": -76.868418,
          "phone": "(01) 205-7080",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:15 pm"
        },
        {
          "name": "Agencia Miraflores",
          "address": "Av. Larco Nº 648 - Miraflores",
          "city": "Lima",
          "lat": -12.123711,
          "lng": -77.029408,
          "phone": "(01) 2057080",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:00 pm"
        },
        {
          "name": "Agencia Villa El Salvador",
          "address": "Av. Juan Velazco Alvarado S/N Parcela II Mz. L Lt. 20 Parque Industrial",
          "city": "Lima",
          "lat": -12.204231,
          "lng": -76.933304,
          "phone": "-",
          "attention_hour": "Lunes a Viernes: 9:00 am a 6:00 pm"
        }
      ]
    }
  ]
}
            '
        , true);
        return response()->json($data);
    }
}
