<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;

use Contugas\Models\Swap;

class MerchantHomeController extends Controller
{
	public function index()
	{
		$logoutUrl = action('Auth\MerchantAuthController@getLogout');
		return view('merchant.home', compact('logoutUrl'));
	}

	public function swap()
	{
		$logoutUrl = action('Auth\MerchantAuthController@getLogout');
		return view('merchant.coupons.swap', compact('logoutUrl'));
	}

	public function history(Request $request, $format = 'json')
	{
		$merchant = multiAuth('merchant')->user();

		$logoutUrl = action('Auth\MerchantAuthController@getLogout');

		$items = $merchant->company->swaps()->simplePaginate();

		$items->load('user');
		switch ($format) {
			case 'json':
				$json = $items;
				return response()->json($json);
				break;
			case 'html':
				return view('merchant.coupons.history', compact('logoutUrl'));
				break;
		}
	}
}
