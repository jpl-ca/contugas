<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;

use DB;

use Carbon\Carbon;

class CouponReportController extends Controller
{
    public function index()
    {
    	return view('reports.coupons.index');
    }

    public function items(Request $request)
    {

    	$startDatetime = $request->has('start_datetime') ? Carbon::parse($request->start_datetime) : Carbon::now()->addyear(-1);
    	$endDatetime = $request->has('end_datetime') ? Carbon::parse($request->end_datetime) : Carbon::now();
        $companyId = $request->has('company_id') ? $request->company_id : null;
        $couponCategoryId = $request->has('coupon_category_id') ? $request->coupon_category_id : null;

        $whereStatement = "";
        $whereStatement .= "AND ( (coupons.start_date <= '{$startDatetime}' AND coupons.end_date >= '{$startDatetime}') OR (coupons.start_date <= '{$endDatetime}' AND coupons.end_date >= '{$endDatetime}') )";
        $whereStatement .= $companyId ? "AND coupons.company_id = '{$companyId}'" : '';
        $whereStatement .= $couponCategoryId ? "AND coupons.coupon_category_id = '{$couponCategoryId}'" : '';

    	$rawSql = "
            SELECT
                coupons.id coupon_id,
                coupons.title coupon_title,
                coupons.start_date coupon_start_date,
                coupons.end_date coupon_end_date,
                coupons.status,
                coupons.max_swaps coupon_max_swaps,
                coupon_categories.name coupon_category_name,
                count(swaps.id) coupon_total_swaps,
                companies.name company_name,
                concat(round((count(swaps.id)/coupons.max_swaps * 100)), '%') coupon_total_swaps_percent
            FROM
                swaps
            RIGHT JOIN
                coupons ON(coupons.id = swaps.coupon_id)
            LEFT JOIN
                companies ON(companies.id = coupons.company_id)
            LEFT JOIN
                coupon_categories ON(coupon_categories.id = coupons.coupon_category_id)
            WHERE 
                1=1
                {$whereStatement}
            GROUP BY
                coupons.id,
                coupons.title,
                coupons.start_date,
                coupons.end_date,
                coupons.max_swaps
    	";

    	$rows = DB::select($rawSql);

    	return response()->json($rows);
    }

    public function swapsOfEveryday(Request $request, $coupon_id, $format = 'json')
    {
    	$rawSql = "
    		SELECT
				year(swaps.created_at) year,
			    month(swaps.created_at) month,
			    day(swaps.created_at) day,
			    count(swaps.id) total
			FROM
				swaps
			WHERE
				swaps.coupon_id = '{$coupon_id}'
			GROUP BY
				year(swaps.created_at),
			    month(swaps.created_at),
			    day(swaps.created_at)
    	";

    	$rows = DB::select($rawSql);

		$rows = json_decode(json_encode($rows), true);
    	
    	$json = [
    			'data' => [
    				'labels' => [],
    				'datasets' => []
    			]
    		];

    	//Filling labels
    	$dataKeys = [];
    	foreach ($rows as $row) {
            $month = str_pad($row['month'], 2, "0", STR_PAD_LEFT);
    		$key = $row['day'] . '-' . $month . '-' . $row['year'];
    		$dataKeys[$key] = null;
    	}

    	$labels = array_keys($dataKeys);
    	//End filling labels

    	//Filling datasets
    	$data = [];
    	foreach ($rows as $key => $value) {
    		$data[] = (int)$value['total'];
    	}
    	$datasets = [];
    	$datasets[0] = [
    			'label' => '# Canjes',
                'tension' => 0,
                'fill' => false,
                'yScale' => 100,
                'beginAtZero' => true,
                'backgroundColor' => '#005496',
    			'data' => $data
    		];
    	//End filling datasets

    	$json['data']['labels'] = $labels;
    	$json['data']['datasets'] = $datasets;
    	switch ($format) {
    		case 'json':
    				return response()->json($json);
    			break;
    		
    		default:
    			# code...
    			break;
    	}
    }
}
