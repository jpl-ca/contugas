<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;
use Contugas\Models\SystemSetting;

class SystemConfigurationController extends Controller
{
	public function index()
	{
		$systemSetting = SystemSetting::first();
		return view('system-configurations.index', ['systemSetting' => $systemSetting]);
	}

	public function update(Request $request)
	{

		$systemSetting = SystemSetting::first();

		if($request->exists('main_mailbox')){
			$systemSetting->main_mailbox = $request->main_mailbox;
		}

		if($request->exists('recipient_mailbox_of_suggestions')){
			$systemSetting->recipient_mailbox_of_suggestions = $request->recipient_mailbox_of_suggestions;
		}

		if($request->exists('recipient_mailbox_of_quotation_requests')){
			$systemSetting->recipient_mailbox_of_quotation_requests = $request->recipient_mailbox_of_quotation_requests;
		}

		$systemSetting->save();

		return view('system-configurations.index', ['systemSetting' => $systemSetting]);
	}
}
