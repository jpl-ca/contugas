<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;
use Contugas\Models\UserEventParticipation;
use Validator;

class UserEventParticipationController extends Controller
{
    
    public function store(Request $request, $event_id, $format = 'json')
    {   
        $user = multiAuth('user')->user();
    	$user_id = $user->id;

        $errors = [];
        $validator = Validator::make(
        		[
        			'event_id' => $event_id
        		]
        		,[
                	'event_id' => 'required|exists:events,id',
            	]);
        if(UserEventParticipation::where('user_id', $user_id)->where('event_id', $event_id)->count() >= 1){
            $validator->getMessageBag()->add('user_id', 'El usuario ya está participando en este evento');
        }
        if( count($validator->errors()) > 0 ){
            return response()->json([
                    'message' => 'Error al crear',
                    'errors' => $validator->errors()
                ], 422);
        }

        $item = new UserEventParticipation;
        $item->user_id = $user_id;
        $item->event_id = $event_id;

        $item->save();

        if($format == 'json'){
            return response()->json([
                    "message" => "Tu asistencia ha sido registrada",
                    "data" => [
                        "item" => $item
                    ]
                ]);
        }
    }

    public function destroy(Request $request, $event_id, $format = 'json')
    {
        $user = multiAuth('user')->user();
        $user_id = $user->id;

        $item = UserEventParticipation::where('event_id', $event_id)->where('user_id', $user_id)->first();

        if(!$item || $user_id != $item->user_id)
        {
        	return response()->json([
                    'message' => 'Error al eliminar',
                    'errors' => [
                        'user' => 'Usted no tiene los suficientes privilegios para eliminar esta participación'
                    ]
                ], 422);
        }

        $item->delete();

        if($format == 'json'){
            return response()->json([
                    'message' => 'Tu participación ha sido removida exitosamente.',
                ]
                );
        }
    }
}
