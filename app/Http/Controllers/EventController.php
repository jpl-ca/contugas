<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;

use Contugas\Models\Event;

use Validator;

use Maatwebsite\Excel\Facades\Excel;

use Carbon\Carbon;

class EventController extends Controller
{
    public function index(Request $request, $format = 'json')
    {
        $items = Event::orderBy('created_at', 'desc');

        if($request->has("type") && in_array($request->type, Event::types() )){
            $items = $items->where('type', $request->type);
        }

        $items = $items->simplePaginate(20);

        if($format == 'json'){
            $data = $items;
            return response()->json($data);
        }else if($format == 'html'){
            return view('events.index', compact('items'));
        }else if($format == 'xls'){
            Excel::create('Eventos', function($excel) use ($items) {
                $excel->sheet('eventos', function($sheet) use ($items) {
                    $sheet->row(1, ['id','Titulo', '', 'Creación', 'Actualización']);
                    $sheet->fromArray($items->toArray()['data'], null, 'A2', false, false);
                });

            })->download();
        }
    }

    public function create(Request $request, $format = 'html')
    {
        return view('events.create');
    }

    public function store(Request $request, $format = 'json', $type = 'event')
    {   
        $validator = Validator::make($request->all(), [
                'title' => 'required'
            ]);

        if($validator->fails()){
            return response()->json([
                    'message' => 'Error al crear',
                    'errors' => $validator->errors()
                ], 422);
        }

        $item = new Event;
        $item->title = $request->title;
        $item->content = $request->content;
        $item->constraints = $request->constraints;
        $item->date = $request->date;
        $item->type = in_array($request->type, Event::types()) ? $request->type : Event::EVENT_TYPE;
        $item->save();

        if($request->hasFile('image_file')){
            $request->file('image_file')->move(Event::files_path(), $item->id);
            $item->image_path = Event::prepareImageUrl($item->id);
            $item->save();
        }

        if($format == 'json'){
            return response()->json([
                    "message" => "Item creado exitosamente",
                    "data" => [
                        "item" => $item
                    ]
                ]);
        }
    }

    public function show(Request $request, $id, $format = 'json')
    {
        $item = Event::findOrFail($id);

        if($format == 'json'){
            return response()->json([
                    "data" => [
                        "item" => $item->toArray()
                    ]
                ]);
        }else{
            return view('events.show');
        }
    }
    
    public function edit(Request $request, $id, $format = 'html')
    {
        $item = Event::find($id);
        return view('events.edit', compact('item'));
    }

    public function update(Request $request, $id, $format = 'json')
    {
        $validator = Validator::make($request->all(), [
                'title' => 'required'
            ]);

        if($validator->fails()){
            return response()->json([
                    'message' => 'Error al actualizar item',
                    'errors' => $validator->errors()
                ], 422);
        }

        $item = Event::findOrFail($id);
        $item->title = $request->title;
        $item->title = $request->title;
        $item->content = $request->content;
        $item->constraints = $request->constraints;
        $item->date = $request->date;
        $item->type = in_array($request->type, Event::types()) ? $request->type : Event::EVENT_TYPE;
        $item->save();

        if($request->hasFile('image_file')){
            $request->file('image_file')->move(Event::files_path(), $item->id);
            $item->image_path = Event::prepareImageUrl($item->id);
            $item->save();
        }


        if($format == 'json'){
            return response()->json([
                    "message" => "Item actualizado exitosamente",
                    "data" => [
                        "item" => $item
                    ]
                ]);
        }
    }

    public function destroy(Request $request, $id, $format = 'json')
    {
        $item = Event::findOrFail($id);
        $item->removeImageFileFromDisk();
        $item->delete();
        if($format == 'json'){
            return response()->json([
                    'message' => 'Eliminado exitosamente',
                ]
                );
        }
    }

    public function participations(Request $request, $id, $format = 'json')
    {
        $item = Event::findOrFail( $id );
        $item->participations->load('user');
        if($format == 'json'){
            return response()->json([
                    'message' => 'Item cargado exitosamente',
                    'data' => $item
                ]
                );
        }else if($format == 'xls'){
            Excel::create('Participantes', function($excel) use ($item) {
                $excel->sheet('participantes del evento', function( $sheet ) use ($item){
                    $i = 1;
                    
                    // Event's information
                    $sheet->row($i, ['Información del evento']);
                    $sheet->row($i, function($row) {
                        $row->setFontWeight('bold');
                    }); $i++;
                    
                    $sheet->row($i, ['Título del Evento', $item->title]); $i++;
                    $sheet->row($i, ['Descripción', $item->contenido]); $i++;
                    $sheet->row($i, ['Condición', $item->constraints]); $i++;
                    $sheet->row($i, ['Fecha', $item->date]); $i++;
                    $sheet->row($i, ['URL de Imagen', $item->image_url]); $i++;
                    $i += 2;

                    // list of attendees
                    $sheet->row($i, ['Codigo de cliente.', 'Nombre completo', 'E-mail']);
                    $sheet->row($i, function ($row) {
                        $row->setFontWeight('bold');
                    });
                    $i++;
                    foreach($item->participations as $participation){
                        $sheet->row($i, [$participation->user->client_number, $participation->user->name, $participation->user->email]);
                        $i++;
                    }

                });
            })->download();
        }
    }
}
