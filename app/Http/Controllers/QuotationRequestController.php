<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;
use Contugas\Http\Requests;
use Contugas\Models\QuotationRequest;
use Contugas\Models\SystemSetting;

use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Carbon\Carbon;
use DB;
use Log;
use Mail;

class QuotationRequestController extends Controller
{
    public function index(Request $request, $format = 'json')
    {
        $quotation_request_query = QuotationRequest::orderBy('created_at', 'desc');

        if($request->has('start_date')){
            $quotation_request_query = $quotation_request_query->where('created_at', '>=', Carbon::parse($request->start_date)->toDateTimestring());
        }

        if($request->has('end_date')){
            $quotation_request_query = $quotation_request_query->where(
                    'created_at',
                    '<=', Carbon::parse($request->end_date)->addDays(1)->toDateTimestring()
                );
        }

        if($request->has('province_id')){
            $quotation_request_query = $quotation_request_query->where('province_id', '=', $request->province_id);
        }

        if($request->has('fields')){
            $fields = explode(',', $request->fields);

            if(in_array('province', $fields)){
                $quotation_request_query->with('province');
            }

            if(in_array('district', $fields)){
                $quotation_request_query->with('district');
            }
        }

        $items = $quotation_request_query->simplePaginate(20);

        if($format == 'json'){
            $data = $items;
            return response()->json($data);
        }else if($format == 'html'){
            return view('quotation-requests.index', compact('items'));
        }else if($format == 'xls'){
            Excel::create('Solicitudes de presupuesto', function($excel) use ($items) {
                $excel->sheet('solicitudes de presupuesto', function($sheet) use ($items) {
                    $i = 1;
                    $sheet->row( $i, ['id',
                                      'Nombres',
                                      'Apellidos',
                                      'Dirección',
                                      'Provincia',
                                      'Distrito',
                                      'Télefono',
                                      'E-mail',
                                      'Comentario',
                                      'Latitud',
                                      'Longitud',
                                      'Fecha creación',
                                      'Fecha modificación'
                                      ]);

                    foreach ($items as $item) {
                       $i++;
                       $new_row = [ $item->id,  
                                    $item->name,
                                    $item->last_name,
                                    $item->address,
                                    $item->province->name,
                                    $item->district->name,
                                    $item->phone,
                                    $item->email,
                                    $item->comment,
                                    $item->lat,
                                    $item->lng,
                                    $item->created_at,
                                    $item->updated_at
                                  ];
                       $sheet->row( $i, $new_row );
                    }
                    
                });

            })->download();
        }
    }

    public function create(Request $request, $format = 'html')
    {
        return view('quotation-requests.create');
    }

    public function store(Request $request, $format = 'json')
    {   
        $validator = Validator::make($request->all(), [
                'name' => 'required',
                'province_id' => 'required|exists:ubigeo,code',
                'district_id' => 'required|exists:ubigeo,code',
                'phone' => 'required'
            ]);

        if($validator->fails()){
            return response()->json([
                    'message' => 'Error al crear',
                    'errors' => $validator->errors()
                ], 422);
        }

        $item = new QuotationRequest;
        $item->fill($request->all());
        $item->lat = $request->lat;
        $item->lng = $request->lng;
        $item->district_id = $request->district_id;

        if(multiAuth('user')->check()){
            $item->user_id = multiAuth('user')->user()->id;
        }

        $item->save();

        // Updating  Admin Users's dashboards
        DB::update('update admin_dashboards set new_quotation_requests = new_quotation_requests + 1 ;');

        //Sending mail to Marketing Area's email
        $systemSetting = SystemSetting::first();

        if( $systemSetting ){

            $recipient_email = $systemSetting->recipient_mailbox_of_quotation_requests;
            $sender_email = $systemSetting->main_mailbox;

            if( !empty( $recipient_email )){
                
                Log::info( 'Sender:' . $sender_email. ' sending mail to ' . $recipient_email. ' about quotation request.' );
                Mail::send('emails.new-quotation-request', ['item' => $item], function($m) use ($recipient_email, $sender_email){
                    $m->from($sender_email, 'Sistema App móvil Contugas');
                    $m->to($recipient_email, 'Sistema App móvil Contugas')->subject('Nueva solicitud de presupuesto');
                });
            }
            
        }



        if($format == 'json'){
            return response()->json([
                    "message" => "Item creado exitosamente",
                    "data" => [
                        "item" => $item
                    ]
                ]);
        }
    }

    public function show(Request $request, $id, $format = 'json')
    {
        $item = QuotationRequest::findOrFail($id);

        if($request->has('fields')){
            $fields = explode(',', $request->fields);
            if(in_array('province', $fields)){
                $item->load('province');
            }

            if(in_array('district', $fields)){
                $item->load('district');
            }
        }

        if($format == 'json'){
            return response()->json([
                    "data" => [
                        "item" => $item->toArray()
                    ]
                ]);
        }else{
            return view('quotation-requests.show');
        }
    }
    
    public function edit(Request $request, $id, $format = 'html')
    {
        $item = QuotationRequest::find($id);
        return view('quotation-requests.edit', compact('item'));
    }

    public function update(Request $request, $id, $format = 'json')
    {
        $validator = Validator::make($request->all(), [
                'name' => 'required',
                'province_id' => 'required|exists:ubigeo,code',
                'district_id' => 'required|exists:ubigeo,code',
                'phone' => 'required',
                'email' => 'email'
            ]);

        if($validator->fails()){
            return response()->json([
                    'message' => 'Error al crear',
                    'errors' => $validator->errors()
                ], 422);
        }

        $item = QuotationRequest::findOrFail($id);
        $item->fill($request->all());
        $item->lat = $request->lat;
        $item->lng = $request->lng;
        $item->save();

        if($format == 'json'){
            return response()->json([
                    "message" => "Item actualizado exitosamente",
                    "data" => [
                        "item" => $item
                    ]
                ]);
        }
    }

    public function destroy(Request $request, $id, $format = 'json')
    {
        $item = QuotationRequest::findOrFail($id);
        $item->delete();
        if($format == 'json'){
            return response()->json([
                    'message' => 'Eliminado exitosamente',
                ]
                );
        }
    }
}
