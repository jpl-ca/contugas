<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;
use Contugas\Models\Coupon;
use Contugas\Models\Ubigeo;

use Contugas\Models\Swap;

class UserCouponController extends Controller
{
	public function index(Request $request, $format = 'json')
	{

		$current_user = multiAuth('user')->user();

		$coupon_query = Coupon::where('status', 'RUNNING')->orderBy('created_at', 'desc');

		if($request->has('province_text')){
			$province = Ubigeo::whereNotNull('parent')->where('name', strtoupper($request->province_text))->first();
			if(isset($province)){
				$coupon_query = $province->coupons();
			}
		}

		if($request->has('province_id')){
			$province = Ubigeo::findOrFail($request->province_id);
			$coupon_query = $province->coupons();
		}

		if($request->has('fields')){
			$fields = explode(',', $request->fields);
			if(in_array('company', $fields)){
				$coupon_query->with('company.company_places');
			}
		}

		$items = $coupon_query->simplePaginate(20);
		$items = $items->toArray();

		$new_items = [];
		foreach ($items["data"] as $key => $item) {

			$item["coupon_code"] = $item["id"] .'-' . $current_user->id;
			$qOfSwapsOfUser = Swap::where('user_id', $current_user->id)->where('coupon_id', $item["id"])->count();
			$userSwaps =  (int)$item["user_swaps"];

			if ( $qOfSwapsOfUser < $userSwaps ) {
				$new_items[] = $item;	
			}
		}

		$items["data"] = $new_items;

		if($format == 'json'){
			$json_response = $items;
			$json_response['message'] = "Items obtenidos exitosamente";
			return response()->json($json_response);
		}
	}
}
