<?php

namespace Contugas\Http\Controllers\Auth;

use Contugas\Http\Controllers\Controller;
use MultiAuth\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $guard = 'admin';
    protected $passwordResetView = 'admin.auth.passwords.reset';
    protected $passwordEmailView = 'admin.auth.passwords.email';
    protected $subject =  'Cambio de contraseña Contugas';

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
}
