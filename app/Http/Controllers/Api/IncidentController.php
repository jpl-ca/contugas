<?php

namespace Contugas\Http\Controllers\Api;

use Illuminate\Http\Request;
use Contugas\Http\Requests;
use Contugas\Http\Controllers\Controller;
use Daylight\Routing\Api\CrudFunctions as ApiCrudFunctions;
use \Contugas\Models\User;
use \Contugas\Models\Incident;
use \Contugas\Models\IncidentType;
use \Carbon\Carbon;
use \DB;
use Illuminate\Support\Facades\Mail;

class IncidentController extends Controller
{
    use ApiCrudFunctions;
        
    protected $rules = [
        'user_id' => 'null_or_exists:users,id',
        'incident_type_id' => 'required|exists:incident_types,id',
        'comment' => 'required|min:25|max:255',
        'lat' => 'required|numeric',
        'lng' => 'required|numeric',
        'address' => 'required',
    ];

    protected $errorMsg = 'Se han encontrado errores en los datos';

    protected $okMsg = 'Registro creado correctamente';

    public function getCreate()
    {
        $incidentTypes = IncidentType::all()->toArray();
        return responseJsonOk([
            'message' => 'Datos para la creación de solicitud de cotización obtenidos correctamente',
            'data' => $incidentTypes
        ]);
    }

    public function postIndex(Request $request)
    {
        $data = [];
        if(multiAuth('user')->check())
        {
            $request->merge(array('user_id' => multiAuth('user')->user()->id));
            $data["name"] = multiAuth('user')->user()->name;
            $data["email"] = multiAuth('user')->user()->email;
            Mail::send('emails.new-incident', ["data"=> $data], function ($m) use ($data) {
                $m->to($data["email"])->subject('Tu reporte de incidencia ha sido recibido - Contugas');
                $m->from('soporte@sgtel.pe', 'Contugas');
            });
        }

        
        return $this->createOrFail(Incident::class, $request);
    }

    public function getIndex()
    {

        $incidents = Incident::with('type')->orderBy('updated_at', 'desc')->paginate(20);
        $incidents->currentPageResolver( function(){
            $currentPage = isset($request->page) ? $request->page : 1;
            return $currentPage;
        });

        return responseJsonOk([
            'message' => 'Todas las incidencias obtenidas exitosamente',
            'data' => $incidents->toArray()
        ]);
    }

    public function getShow(Request $request, $id)
    {   
        //return responseJsonUnauthorized(['message' => 'Sin autorizacion', 'errors' => 'no tiene una sesión iniciada']);

        $incident = Incident::find($id);

        if(is_null($incident))
        {
            return responseJsonNotFound(['message' => 'El recurso que esta buscando no ha sido encontrado', 'errors' => 'No se ha encontrado el incidente']);
        }

        $incident = $incident->toArray();
        return responseJsonOk([
            'message' => 'Incidencia obtenida exitosamente',
            'data' => $incident
        ]);
    }

    public function postReport(Request $request)
    {
        $startDate = Carbon::createFromFormat('d-m-Y',$request->get('startDate'))->startOfDay();
        $endDate = Carbon::createFromFormat('d-m-Y',$request->get('endDate'))->endOfDay();

        $incidents = DB::table('incidents')
                    ->select(DB::raw("distinct incident_types.name, count(incidents.id) as total, date_format(date(incidents.created_at), '%d-%m-%Y') as date"))
                    ->where('incidents.created_at', '>=', $startDate)
                    ->where('incidents.created_at', '<=', $endDate)
                    ->join('incident_types', 'incidents.incident_type_id', '=', 'incident_types.id')
                    ->groupBy('date')
                    ->groupBy('incidents.incident_type_id')
                    ->orderBy('date', 'desc')
                    ->get();

        $incidents = collect($incidents);

        if($incidents->count() == 0)
        {
            $result = null;
        }else{
            $incidentTypes = IncidentType::get()->lists('name', 'id')->toArray();            

            $groupedIncidents = $incidents->groupBy('date');

            $series = $groupedIncidents->keys();

            $data = [];

            $reverser = 0;

            foreach ($groupedIncidents as $gIncident) {

                $counter = 0;

                $dataCounter = [];

                foreach ($gIncident as $incident) {

                    foreach ($incidentTypes as $key => $value) {
                        if($incident->name == $value) {
                            $dataCounter[$key - 1] = $incident->total;
                        }
                    }
                }

                foreach ($incidentTypes as $key => $value) {
                    if(! isset($dataCounter[$key - 1])) {
                        $dataCounter[$key - 1] = "0";
                    }
                }

                ksort($dataCounter);

                foreach ($dataCounter as $key => $value) {
                    $dataCounter[$key] = intval($value);
                }
                
                array_push($data, $dataCounter);
            }

            $its = [];

            foreach ($incidentTypes as $it) {
                array_push($its, $it);
            }

            $series = $series->toArray();

            //volteando
            $arrayV = [];

            foreach ($its as $key => $value) {
                array_push($arrayV, []);
            }

            $iX = count($data);
            $jX = count($data[0]);

            for ($i=0; $i < $iX; $i++) { 
                for ($j=0; $j < $jX; $j++) { 
                    $arrayV[$j][$i] = $data[$i][$j];
                }
            }

            /*
            $result = [
                'labels' => $its,
                'series' => $series,
                'data' => $data
            ];
            */

            $result = [
                'labels' => $series,
                'series' => $its,
                'data' => $arrayV
            ];

        }
        
        return responseJsonOk([
            'message' => 'Reporte obtenido exitosamente',
            'data' => $result
        ]);
    }
}
