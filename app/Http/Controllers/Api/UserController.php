<?php

namespace Contugas\Http\Controllers\Api;

use Illuminate\Http\Request;
use Contugas\Http\Requests;
use Contugas\Http\Controllers\Controller;
use Contugas\Models\User;
use Contugas\Models\Ubigeo;

use Auth;
use Log;
use Validator;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    public function postLogin(Request $request)
    {
    	$email = $request->get('email', '');
    	$password = $request->get('password', '');
    	if (multiAuth('user')->attempt(['email' => $email, 'password' => $password])) {
        	return responseJsonOk( ['message' => 'sesión iniciada correctamente', 'data' => ['user' => Auth::user()->toArray()]] );
        }else{
        	return responseUnprocessableEntity( ['message' => 'Usuario o contraseña incorrectos', 'errors' => 'Datos inválidos'] );
        }
    }

    public function getLogout(Request $request)
    {
        multiAuth('user')->logout();
    	return responseJsonOk( ['message' => 'sesión cerrada'] );
    }

    public function postCreate(Request $request)
    {   
        $validator = Validator::make($request->all(),[
            'customer_number' => 'required|unique:users,client_number',
            'identification_document' => 'required|min:8',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:6'
        ]);
        
        if($validator->fails()){
            return response()->json(
                [
                    'message' => join(' ', $validator->errors()->all()),
                    'errors' => $validator->errors()->toArray()
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY
                );
        }

        $http_client = new \GuzzleHttp\Client();
        $url = config('app.contugas_gateway_base_url').'/validate';

        $http_result = $http_client->request('POST', $url, [
                'headers' => [
                                'Content-Type' => 'application/json'
                        ],
                'body' => '{"customer_number":"'.$request->customer_number.'","identification_document":"'.$request->identification_document.'"}'
            ]
        );

        if($http_result->getStatusCode()!=Response::HTTP_OK){
            $response = new Response($http_result->getBody(), $http_result->getStatusCode(), ['Content-Type' => $http_result->getHeaderLine('Content-Type') ]);
            return $response;
        };

	$response_body = $http_result->getBody();
        $json = json_decode($http_result->getBody(), true);

        if(!isset($json["data"])){
            $data = [
                'message' => 'Se han encontrado un error inesperado en el servidor',
                'errors' => ['el formato de respuesta es inválido']
            ];
            return response()->json($data, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $user = new User;
        $user->client_number = $request->customer_number;
        $user->id_number = $request->identification_document;
        $user->email = $request->email;
        $user->password = \Hash::make($request->password);
        $user->name = isset($json["data"]["customer_name"]) ? $json["data"]["customer_name"] : null;
        $user->address = isset($json["data"]["address"]) ? $json["data"]["address"] : null;
        $user->province_text = isset($json["data"]["province"]) ? $json["data"]["province"] : null;
        $user->district_text = isset($json["data"]["district"]) ? $json["data"]["district"] : null;
        $user->supply_state = isset($json["data"]["supply_state"]) ? $json["data"]["supply_state"] : null;

        //Filling ubigeo data
        if (isset($json["data"]["province"])){
            $province = Ubigeo::findProvincesFromText($json["data"]["province"])->get()->first();
            if( isset($province) && isset($province->code)){
                $user->province_id = $province->code;
            }
            
        }

        $user->save();

        return response()->json([
                'message' => 'Cuenta de usuario creada exitosamente',
                'data' => 
                    [
                        'user' => $user->toArray()
                    ]
            ],
            Response::HTTP_ACCEPTED
        );
    }

    public function putUpdate(Request $request)
    {
        if( empty( $request->all() ) ) {
            return response()->json([
                    'message' => 'No hay datos a actualizar. Ingrese algún parametro.'
                ], 422);
        }
        
        $validator = Validator::make([], []);

        $user = multiAuth('user')->user();

        if ( $request->exists('new_password') && $request->exists('last_password') ) {

            if (\Hash::check($request->last_password, $user->password)) {
                $user->password = \Hash::make($request->new_password);
            } else {
                $validator->getMessageBag()->add('last_password', 'Su clave actual no es correcta.');
            }
        }

        if (count($validator->errors()) > 0) {
            return response()->json([
                                        'message' => 'Error al actualizar usuario',
                                        'errors' => $validator->errors()
                                    ], 422);
        }

        $user->save();

        return response()->json([
            'message' => 'Datos actualizados correctamente',
            'data' => $user
            ]);
    }

    public function deleteDestroy(Request $request, $id)
    {

    }
}
