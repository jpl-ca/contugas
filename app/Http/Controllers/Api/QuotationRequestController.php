<?php

namespace Contugas\Http\Controllers\Api;

use Illuminate\Http\Request;
use Contugas\Http\Requests;
use Contugas\Http\Controllers\Controller;
use Daylight\Routing\Api\CrudFunctions as ApiCrudFunctions;
use \Contugas\Models\Ubigeo;
use \Contugas\Models\QuotationRequest;
use \Carbon\Carbon;
use \DB;

class QuotationRequestController extends Controller
{
    use ApiCrudFunctions;
        
    protected $rules = [
        'name' => 'required|max:255',
        'last_name' => 'max:255',
        'address' => 'required|max:255',
        'province_id' => 'required|exists:ubigeo,code',
        'phone' => 'required|alpha_dash|max:15',
        'email' => 'required|email',
        'comment' => 'required|string|min:25|max:255',
    ];

    protected $errorMsg = 'Se han encontrado errores en los datos';

    protected $okMsg = 'Registro creado correctamente';

    public function getCreate()
    {
        $provinces = Ubigeo::whereIn('code', ['1001', '1002', '1003', '1004'])->get()->toArray();
        return responseJsonOk([
            'message' => 'Datos para la creación de solicitud de cotización obtenidos correctamente',
            'data' => [
                'provinces' => $provinces
            ]
        ]);
    }

    public function postIndex(Request $request)
    {
        return $this->createOrFail(QuotationRequest::class, $request);
    }

    public function getIndex()
    {
        $quotationRequests = QuotationRequest::with('province')->orderBy('updated_at', 'desc')->paginate(20);
        $quotationRequests->currentPageResolver( function(){
            $currentPage = isset($request->page) ? $request->page : 1;
            return $currentPage;
        });

        return responseJsonOk([
            'message' => 'Todas las solicitudes de presupuesto obtenidas exitosamente',
            'data' => $quotationRequests->toArray()
        ]);
    }

    public function getShow(Request $request, $id)
    {

        $quotationRequest = QuotationRequest::with('province')->whereId($id)->first();

        if(is_null($quotationRequest))
        {
            return responseJsonNotFound(['message' => 'El recurso que esta buscando no ha sido encontrado', 'errors' => 'No se ha encontrado la solicitud de presupuesto']);
        }

        $quotationRequest = $quotationRequest->toArray();

        return responseJsonOk([
            'message' => 'Solicitud de presupuesto obtenida exitosamente',
            'data' => $quotationRequest
        ]);
    }

    public function postReport(Request $request)
    {
        $startDate = Carbon::createFromFormat('d-m-Y',$request->get('startDate'))->startOfDay();
        $endDate = Carbon::createFromFormat('d-m-Y',$request->get('endDate'))->endOfDay();

        $quotationRequests = QuotationRequest::select(DB::raw("distinct date_format(date(created_at), '%d-%m-%Y') as date, count(id) as total"))
                            ->where('created_at', '>=', $startDate)
                            ->where('created_at', '<=', $endDate)
                            ->groupBy('date')
                            ->get();

        if($quotationRequests->count() == 0)
        {
            $result = null;
        }else{
            $labelsPlucked = $quotationRequests->pluck('date');
            $totalPlucked = $quotationRequests->pluck('total');

            $labels = $labelsPlucked->all();
            $series = ['Solicitudes de Presupuesto'];

            $data = [
               $totalPlucked->all()
            ];

            $result = [
                'labels' => $labels,
                'series' => $series,
                'data' => $data,
            ];
        }
        
        return responseJsonOk([
            'message' => 'Reporte obtenido exitosamente',
            'data' => $result
        ]);
    }
}
