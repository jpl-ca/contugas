<?php

namespace Contugas\Http\Controllers\Api;

use Illuminate\Http\Request;
use Contugas\Http\Requests;
use Contugas\Http\Controllers\Controller;
use Contugas\Models\User;

class GasConsumptionController extends Controller
{
    public function getIndex()
    {
        return responseJsonOk(['gas-consumption' => multiAuth('user')->user()->gasConsumptionLastMonths()]);
    }
}
