<?php

namespace Contugas\Http\Controllers\Api;

use Illuminate\Http\Request;
use Auth;
use Validator;
use Contugas\Models\Coupon;
use Contugas\Http\Requests;
use Contugas\Http\Controllers\Controller;

class AdminAuthController extends Controller
{
    public $guard = 'admin';

    public function getMe()
    {
        if ($this->auth()->check()) {
            return responseJsonOk($this->auth()->user());
        }

        return responseJsonForbidden('No ha iniciado sesión');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required', 'password' => 'required',
        ]);

        $credentials = $this->getCredentials($request);

        if ($this->auth()->attempt($credentials, $request->has('remember'))) {
            return responseJsonOk($this->auth()->user());
        }

        return responseBadRequest('Usuario o contraseña incorrectos.');
    }

    public function getLogout()
    {
        if ($this->auth()->check()) {
            $this->auth()->logout();
            return responseJsonOk('Sesión cerrada correctamente');
        }

        return responseJsonForbidden('No ha iniciado sesión');
    }

    public function getCheck()
    {
        $isLoggedIn = $this->auth()->check();

        return responseJsonOk($isLoggedIn);
    }

    protected function getCredentials(Request $request)
    {
        return $request->only('email', 'password');
    }

    /**
     * Get current variable based guard or if property does not exist get default guard.
     *
     * @return Auth
     */
    protected function auth()
    {
        return Auth::guard( property_exists($this, 'guard') ? $this->guard : null );
    }
}
