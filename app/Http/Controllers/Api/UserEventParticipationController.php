<?php
namespace Contugas\Http\Controllers\Api;

use Illuminate\Http\Request;
use Contugas\Http\Requests;
use Contugas\Http\Controllers\Controller;
use Contugas\Models\User;
use Contugas\Http\Controllers\UserEventParticipationController as BaseEventParticipationController;

use Auth;
use Log;
use Validator;
use Symfony\Component\HttpFoundation\Response;

class UserEventParticipationController extends BaseEventParticipationController
{
}