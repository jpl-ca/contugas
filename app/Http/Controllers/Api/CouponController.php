<?php

namespace Contugas\Http\Controllers\Api;

use Contugas\Models\Merchant;
use Contugas\Models\Swap;
use Contugas\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Contugas\Models\Coupon;
use Contugas\Http\Requests;
use Contugas\Http\Controllers\Controller;
use Validator;

class CouponController extends Controller
{
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.api:user', ['only' => 'getIndex']);
        $this->middleware('auth.api:merchant', ['only' => ['getCoupon', 'postSwap']]);
    }

    public function getIndex(Request $request, $id = null)
    {
        $user = multiAuth('user')->user();

        if (! is_null($id)) {
            $result = Coupon::findOrFail($id);
            $result = $result->toArray();
            $result['coupon_code'] = $result['id'] . "-" . $user->id;
        } else {
            $coupons = Coupon::active()->get();

            $result = [];

            foreach ($coupons as $coupon) {
                if ($this->userCanSwapCoupon($user, $coupon)) {
                    array_push($result, array_merge($coupon->toArray(), ['coupon_code' => $coupon->id . "-" . $user->id]));
                }
            }
        }

        return responseJsonOk($result);
    }

    public function getCoupon(Request $request, $couponCode)
    {
        $merchant = multiAuth('merchant')->user();

        list($coupon_id, $user_id) = explode('-', $couponCode);

        $result = Coupon::findOrFail($coupon_id);

        if ($result->company_id != $merchant->company_id) {
            $json = 'Su establecimiento no está autorizado para canjear este cupón';
            return response($json, 422);
        }
    
        $result = $result->toArray();
        $result['coupon_code'] = $couponCode;
        return responseJsonOk($result);
    }

    public function postSwap(Request $request)
    {
        $merchant = multiAuth('merchant')->user();
        
        $validator = Validator::make([],[]);

        $data = $request->get('coupon_code');

        list($coupon_id, $user_id) = explode('-', $data);

        $user = User::find($user_id);

        $coupon = Coupon::findorFail($coupon_id);

        if (!$user) {
            $validator->getMessageBag()->add( 'user' , 'El usuario o cliente no existe en la base de datos.');
        }

        if( $user && !$this->userCanSwapCoupon( $user, $coupon ) ){
            $validator->getMessageBag()->add( 'max_coupon_per_user' , 'El usuario ha alcanzado el límite máximo de canje');
        }

        if( $coupon->company_id != $merchant->company->id) {
            $validator->getMessageBag()->add( 'merchant' , 'Usted no está autorizado para realizar el canje de este cupón.');
        }

        if(count( $validator->errors() ) > 0) {
            $data = [];

            // join messages
            $mainError = "";
            $i = 0;

            foreach ($validator->errors()->toArray() as $value) {
                $separator = ( $i == 0 ? '' : '. ');
                $mainError =  $mainError . $separator . $value[0];

                $i++;
            }
            
            $data['message'] = $mainError;

            $data['errors'] = $validator->errors();

            return response()->json( $data, 422 );
        }

        $swap = $user->swaps()->create([
            'coupon_id' => $coupon->id
        ]);

        $data = [];
        $data['message'] = 'Cupón canjeado exitosamente.';
        $data['data'] = [
            "item" => $swap
        ];

        return response()->json( $data );
        
    }

    protected function userCanSwapCoupon(User $user, Coupon $coupon)
    {

        $userSwappedCoupons = $user->swappedCoupons()->where('coupon_id', $coupon->id)->get()->count();
        $totalSwappedCoupons = $coupon->swaps->count();

        return $userSwappedCoupons < $coupon->user_swaps;
    }
}
