<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;
use Symfony\Component\HttpFoundation\Response;
use Validator;

class ContugasGatewayController extends Controller
{
    public function postCustomerValidate(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    						'customer_number' => 'required',
    						'identification_document' => 'required'
    					]);

    	if($validator->fails()){
    		return response()->json([
    				'message' => 'Se han encontrado errores',
    				'errors' => $validator->errors()->toArray(),
    			],
    			Response::HTTP_UNPROCESSABLE_ENTITY
    			);
    	}

        $http_client = new \GuzzleHttp\Client();
        $url = config('app.contugas_gateway_base_url').'/validate';

        $http_result = $http_client->request('POST', $url, [
                'headers' => [
                                'Content-Type' => 'application/json'
                        ],
                'body' => '{"customer_number":"'.$request->customer_number.'","identification_document":"'.$request->identification_document.'"}'
            ]
        );
	$http_status_code = $http_result->getStatusCode();
	$response_body = $http_result->getBody();

	//Fixing json
	$json = json_decode($http_result->getBody(), true);

        if (isset($json["RespuestaVerificarNumeroCliente_data"])) {
            $json["data"] = $json["RespuestaVerificarNumeroCliente_data"];
            unset($json["RespuestaVerificarNumeroCliente_data"]);
            $response_body = json_encode($json);
	}
	//End json fixing

	
        if (isset($json["message"]) && $json["message"] == "Los datos ingresados no corresponden a un cliente válido.") {
            $http_status_code = 422;            
        }

        $response = new Response($response_body, $http_status_code, ['Content-Type' => $http_result->getHeaderLine('Content-Type') ]);
        return $response;
    }
}
