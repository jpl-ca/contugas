<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;

use Carbon\Carbon;

use DB;

class ReportController extends Controller
{
    public function index() {
    	return response()->view('reports.index');
    }

    /*
	3.1
    */
    public function quotationRequestGroupByMonthAndDistrict(Request $request, $format = 'json')
    {
    	$startDatetime = $request->has('start_datetime') ? Carbon::parse($request->start_datetime) : Carbon::now()->addyear(-1);
    	$endDatetime = $request->has('end_datetime') ? Carbon::parse($request->end_datetime) : Carbon::now();

    	$rawSql = "
             SELECT
                year(quotation_requests.created_at) year,
                month(quotation_requests.created_at) month,
                provinces.code province_id,
                provinces.name province_name,
                sum(if(quotation_requests.province_id = provinces.code, 1, 0)) total
            FROM
                quotation_requests, 
                (
                    SELECT
                        * 
                    FROM
                        ubigeo
                    WHERE
                        /* solamente departamento de ICA*/
                        parent = '10'
                ) provinces
            WHERE   
                quotation_requests.created_at >= '{$startDatetime}' AND
                quotation_requests.created_at <= '{$endDatetime}'
            GROUP BY
                year(quotation_requests.created_at),
                month(quotation_requests.created_at),
                provinces.code,
                provinces.name
        ";

    	$rows = DB::select($rawSql);

        //No content
        if(!(count($rows) > 0)) {
            return response()->json([
                    'error' => 'Sin contenido'
                ], 204);
        }

        //Filling labels
        foreach ($rows as $key => $value) {
            $labels[$value->year . '-' . $value->month] = null;
        }

        $json['data']['labels'] = array_keys($labels);
        //End filling labels

        $datasets_keys = [];
        foreach ($rows as $key => $value) {
            $datasets_keys[$value->province_id]['label'] = $value->province_name;
            $datasets_keys[$value->province_id]['data'] = [];
        }

        foreach ($rows as $key => $value) {
            if (array_key_exists($value->province_id, $datasets_keys)) {
                $datasets_keys[$value->province_id]['data'][] = (int)$value->total;
            }
        }

        $datasets = [];
        $i = 0;
        foreach ($datasets_keys as $key => $value) {
            $value["fillColor"] = $this->getColor($i);
            $datasets[] = $value;
            $i++;
        }

        $json['data']['datasets'] = $datasets;



        switch ($format) {
            case 'json':
                    return response()->json($json);
                break;
        }
    }

    /*
	3.2
    */
    public function quotationRequestGroupByDistrict(Request $request, $format = 'json')
    {
    	$startDatetime = $request->has('start_datetime') ? Carbon::parse($request->start_datetime) : Carbon::now()->addyear(-1);
    	$endDatetime = $request->has('end_datetime') ? Carbon::parse($request->end_datetime) : Carbon::now();

    	$rawSql = "
            SELECT
                ub.code,
                (
                    SELECT name 
                    FROM ubigeo 
                    WHERE ubigeo.code = ub.parent
                    LIMIT 1
                ) as province,
                ub.name district,
                (
                    SELECT
                        count(*)
                    FROM
                        quotation_requests
                    WHERE
                        quotation_requests.district_id = ub.code
                ) total
            FROM
            ubigeo ub
            WHERE
                ub.parent in 
                    (
                        SELECT 
                            code 
                        FROM 
                            ubigeo
                        WHERE
                            parent = '10'
                    )
        ";

    	$rows = DB::select($rawSql);
        
        //No content
        if(!(count($rows) > 0)) {
            return response()->json([
                    'error' => 'Sin contenido'
                ], 204);
        }

        //Builing json
        $json = [ 'data' => 
                    [
                        'labels' => [],
                        'datasets' => []
                    ]
                ];

        //Filling labels
        foreach ($rows as $key => $value) {
            $labels[$value->code] = $value->province . ' - ' . $value->district;
        }

        $json['data']['labels'] = array_values($labels);
        //End filling labels

        $datasets_keys = [];
        foreach ($rows as $key => $value) {
            $datasets_keys[$value->code]['label'] = $value->province . ' - ' . $value->district;
            $datasets_keys[$value->code]['data'] = [];
        }

        foreach ($rows as $key => $value) {
            if (array_key_exists($value->code, $datasets_keys)) {
                $datasets_keys[$value->code]['data'][] = (int)$value->total;
            }
        }

       $datasets = [];
        $i = 0;
        foreach ($datasets_keys as $key => $value) {
            $value["fillColor"] = $this->getColor($i);
            $datasets[] = $value;
            $i++;
        }

        $json['data']['datasets'] = $datasets;
        $json['type'] = 'horizontal-bar';
    	switch ($format) {
    		case 'json':
    				return response()->json($json);
    			break;
    	}
    }

    /*
    4.1
    */
    public function tipsGroupByMonth(Request $request, $format = 'json')
    {
    	$startDatetime = $request->has('start_datetime') ? Carbon::parse($request->start_datetime) : Carbon::now()->addyear(-1);
    	$endDatetime = $request->has('end_datetime') ? Carbon::parse($request->end_datetime) : Carbon::now();

    	$rawSql = "
			SELECT
				YEAR(created_at) year,
				MONTH(created_at) month,
			    count(id) total
			FROM tips
			WHERE
				tips.created_at >= '{$startDatetime}'
				AND tips.created_at <= '{$endDatetime}'
			GROUP BY
				YEAR(tips.created_at),
				MONTH(tips.created_at)
			ORDER BY YEAR(tips.created_at), MONTH(tips.created_at)
			";

    	$rows = DB::select($rawSql);
        
        //No content
        if(!(count($rows) > 0)) {
            return response()->json([
                    'error' => 'Sin contenido'
                ], 204);
        }

        //Filling labels
        foreach ($rows as $key => $value) {
            $labels[$value->year . '-' . $value->month] = null;
        }
        
        $json['data']['labels'] = array_keys($labels);
        //end filling labels

        $datasets = [];
        foreach ($rows as $key => $value) {
            $datasets[0]['data'][] = (int)$value->total;
        }

        $json['data']['datasets'] = $datasets;

        switch ($format) {
            case 'json':
                    return response()->json($json);
                break;
        }
    }

	/*
	5.1, 5.3, 5.4, 5.5
	*/
    public function couponsSwapsByMonth(Request $request, $format = 'json')
    {
    	$startDatetime = $request->has('start_datetime') ? Carbon::parse($request->start_datetime) : Carbon::now()->addyear(-1);
    	$endDatetime = $request->has('end_datetime') ? Carbon::parse($request->end_datetime) : Carbon::now();

    	$rawSql = "
				SELECT
					year(swaps.created_at) year,
					month(swaps.created_at) month,
					coupon_categories.id coupon_category_id,
					coupon_categories.name coupon_category,
				    sum( if(swaps.coupon_id = coupons.id and coupons.coupon_category_id = coupon_categories.id, 1, 0)) total_swaps
				FROM coupons, coupon_categories, swaps
				WHERE
					swaps.created_at >= '{$startDatetime}' AND
					swaps.created_at <= '{$endDatetime}'
				GROUP BY
					year(swaps.created_at),
					month(swaps.created_at),
					coupon_categories.id,
					coupon_categories.name
    		";

		$rows = DB::select($rawSql);
        
        //No content
        if(!(count($rows) > 0)) {
            return response()->json([
                    'error' => 'Sin contenido'
                ], 204);
        }

    	//Builing json
    	$json = [ 'data' => 
    				[
    					'labels' => [],
    					'datasets' => []
    				]
    			];

    	//Filling labels
    	foreach ($rows as $key => $value) {
    		$labels[$value->year . '-' . $value->month] = null;
    	}

		$json['data']['labels'] = array_keys($labels);
		//End filling labels

		$datasets_keys = [];
    	foreach ($rows as $key => $value) {
    		$datasets_keys[$value->coupon_category_id]['label'] = $value->coupon_category;
    		$datasets_keys[$value->coupon_category_id]['id'] = $value->coupon_category_id;
    		$datasets_keys[$value->coupon_category_id]['data'] = [];
    	}

    	foreach ($rows as $key => $value) {
    		if (array_key_exists($value->coupon_category_id, $datasets_keys)) {
    			$datasets_keys[$value->coupon_category_id]['data'][] = (int)$value->total_swaps;
    		}
    	}

        $datasets = [];
        $i = 0;
        foreach ($datasets_keys as $key => $value) {
            $value["fillColor"] = $this->getColor($i);
            $datasets[] = $value;
            $i++;
        }

    	$json['data']['datasets'] = $datasets;



    	switch ($format) {
    		case 'json':
    				return response()->json($json);
    			break;
    	}
    }

    public function couponsSwapsByYearAndCategory(Request $request, $format = 'json')
    {
        $startDatetime = $request->has('start_datetime') ? Carbon::parse($request->start_datetime) : Carbon::now()->addyear(-1);
        $endDatetime = $request->has('end_datetime') ? Carbon::parse($request->end_datetime) : Carbon::now();

        $rawSql = "
            SELECT
                year(swaps.created_at) year,
                coupon_categories.id coupon_category_id,
                coupon_categories.name coupon_category,
                sum( if(swaps.coupon_id = coupons.id and coupons.coupon_category_id = coupon_categories.id, 1, 0)) total_swaps
            FROM coupons, coupon_categories, swaps
            WHERE
                swaps.created_at >= '{$startDatetime}' AND
                swaps.created_at <= '{$endDatetime}'
            GROUP BY
                year(swaps.created_at),
                coupon_categories.id,
                coupon_categories.name
        ";

        $rows = DB::select($rawSql);
        
        //No content
        if(!(count($rows) > 0)) {
            return response()->json([
                    'error' => 'Sin contenido'
                ], 204);
        }

        //Filling labels
        foreach ($rows as $key => $value) {
            $labels[$value->year] = null;
        }
        
        $json['data']['labels'] = array_keys($labels);
        //end filling labels

        $datasets_keys = [];
        foreach ($rows as $key => $value) {
            $datasets_keys[$value->coupon_category_id]['label'] = $value->coupon_category;
            $datasets_keys[$value->coupon_category_id]['id'] = $value->coupon_category_id;
            $datasets_keys[$value->coupon_category_id]['data'] = [];
        }

        foreach ($rows as $key => $value) {
            if (array_key_exists($value->coupon_category_id, $datasets_keys)) {
                $datasets_keys[$value->coupon_category_id]['data'][] = (int)$value->total_swaps;
            }
        }

        //Filling data
        $datasets = [];
        $i = 0;
        foreach ($datasets_keys as $key => $value) {
            $value["fillColor"] = $this->getColor($i);
            $datasets[] = $value;
            $i++;
        }

        $json['data']['datasets'] = $datasets;

        switch ($format) {
            case 'json':
                    return response()->json($json);
                break;
        }
    }

    /*
    5.2
    */
    public function couponsByMonthAndCategory(Request $request, $format = 'json')
    {
    	$startDatetime = $request->has('start_datetime') ? Carbon::parse($request->start_datetime) : Carbon::now()->addyear(-1);
    	$endDatetime = $request->has('end_datetime') ? Carbon::parse($request->end_datetime) : Carbon::now();

    	$rawSql = "
				SELECT
					year(coupons.created_at) year,
					month(coupons.created_at) month,
				    coupon_categories.id coupon_category_id,
				    coupon_categories.name coupon_category,
				    sum( if(coupons.coupon_category_id = coupon_categories.id, 1, 0) ) as total
				FROM coupons, coupon_categories
				WHERE
					coupons.created_at >= '2015-01-01' AND
					coupons.created_at <= '2016-12-31'
				GROUP BY
					year(coupons.created_at),
					month(coupons.created_at),
				    coupon_categories.id,
					coupon_categories.name
    		";

    	$rows = DB::select($rawSql);
        
        //No content
        if(!(count($rows) > 0)) {
            return response()->json([
                    'error' => 'Sin contenido'
                ], 204);
        }

    	//Filling labels
    	foreach ($rows as $key => $value) {
    		$labels[$value->year . '-' . $value->month] = null;
    	}
		
		$json['data']['labels'] = array_keys($labels);
    	//end filling labels

		$datasets_keys = [];
    	foreach ($rows as $key => $value) {
    		$datasets_keys[$value->coupon_category_id]['label'] = $value->coupon_category;
    		$datasets_keys[$value->coupon_category_id]['id'] = $value->coupon_category_id;
    		$datasets_keys[$value->coupon_category_id]['data'] = [];
    	}

    	foreach ($rows as $key => $value) {
    		if (array_key_exists($value->coupon_category_id, $datasets_keys)) {
    			$datasets_keys[$value->coupon_category_id]['data'][] = (int)$value->total;
    		}
    	}

        $datasets = [];
        $i = 0;
        foreach ($datasets_keys as $key => $value) {
            $value["fillColor"] = $this->getColor($i);
            $datasets[] = $value;
            $i++;
        }

    	$json['data']['datasets'] = $datasets;
    	
    	switch ($format) {
    		case 'json':
    				return response()->json($json);
    			break;
    	}
    }

    public function eventsByMonth(Request $request, $format = 'json')
    {
    	$startDatetime = $request->has('start_datetime') ? Carbon::parse($request->start_datetime) : Carbon::now()->addyear(-1);
    	$endDatetime = $request->has('end_datetime') ? Carbon::parse($request->end_datetime) : Carbon::now();

    	$rawSql = "
			SELECT
				year(events.created_at) year,
			    month(events.created_at) month,
				count(events.id) total
			FROM
				events
			WHERE
				events.type = 'event' AND
			    events.created_at >= '{$startDatetime}' AND
			    events.created_at <= '{$endDatetime}'
			GROUP BY
				year(events.created_at),
			    month(events.created_at)
			ORDER BY
				year(events.created_at),
			    month(events.created_at)
    	";

    	$rows = DB::select($rawSql);
        
        //No content
        if(!(count($rows) > 0)) {
            return response()->json([
                    'error' => 'Sin contenido'
                ], 204);
        }

    	//Builing json
    	$json = [ 'data' => 
    				[
    					'labels' => [],
    					'datasets' => []
    				]
    			];


		//Filling labels
    	foreach ($rows as $key => $value) {
    		$labels[$value->year . '-' . $value->month] = null;
    	}
		
		$json['data']['labels'] = array_keys($labels);
    	//end filling labels


    	//Filling datasets
    	$datasets = [];

    	foreach ($rows as $key => $value) {
    		$datasets[0]['data'][] = (int)$value->total;
    	}
    	//End filling datasets

    	$json['type'] = 'bar';
		$json['data']['datasets'] = $datasets;

		switch ($format) {
    		case 'json':
    				return response()->json($json);
    			break;
    	}
    }

    public function rafflesByMonth(Request $request, $format = 'json')
    {
    	$startDatetime = $request->has('start_datetime') ? Carbon::parse($request->start_datetime) : Carbon::now()->addyear(-1);
    	$endDatetime = $request->has('end_datetime') ? Carbon::parse($request->end_datetime) : Carbon::now();

    	$rawSql = "
			SELECT
				year(events.created_at) year,
			    month(events.created_at) month,
				count(events.id) total
			FROM
				events
			WHERE
				events.type = 'raffle' AND
			    events.created_at >= '{$startDatetime}' AND
			    events.created_at <= '{$endDatetime}'
			GROUP BY
				year(events.created_at),
			    month(events.created_at)
			ORDER BY
				year(events.created_at),
			    month(events.created_at)
    	";

    	$rows = DB::select($rawSql);
        
        //No content
        if(!(count($rows) > 0)) {
            return response()->json([
                    'error' => 'Sin contenido'
                ], 204);
        }

    	//Builing json
    	$json = [ 'data' => 
    				[
    					'labels' => [],
    					'datasets' => []
    				]
    			];


		//Filling labels
    	foreach ($rows as $key => $value) {
    		$labels[$value->year . '-' . $value->month] = null;
    	}
		
		$json['data']['labels'] = array_keys($labels);
    	//end filling labels


    	//Filling datasets
    	$datasets = [];

    	foreach ($rows as $key => $value) {
    		$datasets[0]['data'][] = (int)$value->total;
    	}
    	//End filling datasets

    	$json['type'] = 'bar';
		$json['data']['datasets'] = $datasets;   


		switch ($format) {
    		case 'json':
    				return response()->json($json);
    			break;
    	}

    }

    public function eventAttendeesByMonth(Request $request, $format = 'json')
    {

    	$startDatetime = $request->has('start_datetime') ? Carbon::parse($request->start_datetime) : Carbon::now()->addyear(-1);
    	$endDatetime = $request->has('end_datetime') ? Carbon::parse($request->end_datetime) : Carbon::now();

    	$rawSql = "
			SELECT
				year(coupons.created_at) year,
			    month(coupons.created_at) month,
				count(swaps.id) total
			FROM
				coupons
			INNER JOIN
				swaps
			    ON(swaps.coupon_id = coupons.id)
			WHERE
				coupons.created_at >= '{$startDatetime}' AND
				coupons.created_at <= '{$endDatetime}'
			GROUP BY
				year(coupons.created_at),
			    month(coupons.created_at)
    	";

    	$rows = DB::select($rawSql);
        
        //No content
        if(!(count($rows) > 0)) {
            return response()->json([
                    'error' => 'Sin contenido'
                ], 204);
        }

    	//Builing json
    	$json = [ 'data' => 
    				[
    					'labels' => [],
    					'datasets' => []
    				]
    			];


		//Filling labels
    	foreach ($rows as $key => $value) {
    		$labels[$value->year . '-' . $value->month] = null;
    	}
		
		$json['data']['labels'] = array_keys($labels);
    	//end filling labels


    	//Filling datasets
    	$datasets = [];

    	foreach ($rows as $key => $value) {
    		$datasets[0]['data'][] = (int)$value->total;
    	}
    	//End filling datasets

    	$json['type'] = 'bar';
		$json['data']['datasets'] = $datasets;   


		switch ($format) {
    		case 'json':
    				return response()->json($json);
    			break;
    	}
    }

    public function suggestionsByMonth(Request $request, $format = 'json')
    {
    	$startDatetime = $request->has('start_datetime') ? Carbon::parse($request->start_datetime) : Carbon::now()->addyear(-1);
    	$endDatetime = $request->has('end_datetime') ? Carbon::parse($request->end_datetime) : Carbon::now();

    	$rawSql = "
			SELECT
				year(suggestions.created_at) year,
			    month(suggestions.created_at) month,
			    count(suggestions.id) total
			FROM
				suggestions
			WHERE
				suggestions.created_at >= '{$startDatetime}' AND
			    suggestions.created_at <= '{$endDatetime}'
			GROUP BY
				year(suggestions.created_at),
			    month(suggestions.created_at)
			ORDER BY
				year(suggestions.created_at),
			    month(suggestions.created_at)
    	";

    	$rows = DB::select($rawSql);
        
        //No content
        if(!(count($rows) > 0)) {
            return response()->json([
                    'error' => 'Sin contenido'
                ], 204);
        }

    	//Builing json
    	$json = [ 'data' => 
    				[
    					'labels' => [],
    					'datasets' => []
    				]
    			];

    	//Filling labels
    	foreach ($rows as $key => $value) {
    		$labels[$value->year . '-' . $value->month] = null;
    	}
		
		$json['data']['labels'] = array_keys($labels);
    	//end filling labels


    	//Filling datasets
    	$datasets = [];

    	foreach ($rows as $key => $value) {
    		$datasets[0]['data'][] = (int)$value->total;
    	}
    	//End filling datasets

    	$json['type'] = 'bar';
		$json['data']['datasets'] = $datasets;    	

		switch ($format) {
    		case 'json':
    				return response()->json($json);
    			break;
    	}
    }

    public function usersByMonth(Request $request, $format = 'json')
    {
        $startDatetime = $request->has('start_datetime') ? Carbon::parse($request->start_datetime) : Carbon::now()->addyear(-1);
        $endDatetime = $request->has('end_datetime') ? Carbon::parse($request->end_datetime) : Carbon::now();

        $rawSql = "
            select 
                year(users.created_at) year,
                month(users.created_at) month,
                count(users.id) total
            from
                users
            where
                users.created_at >= ? AND
                users.created_at <= ?
            group by
                year(users.created_at), 
                month(users.created_at)
            order by
                year(created_at) desc,
                month(created_at) desc
            ";
        $rows = DB::select($rawSql, [$startDatetime, $endDatetime]);

        //No content
        if(!(count($rows) > 0)) {
            return response()->json([
                    'error' => 'Sin contenido'
                ], 204);
        }

        //Builing json
        $json = [ 'data' => 
                    [
                        'labels' => [],
                        'datasets' => []
                    ]
                ];

        //Filling labels
        foreach ($rows as $key => $value) {
            $labels[$value->year . '-' . $value->month] = null;
        }
        
        $json['data']['labels'] = array_keys($labels);
        //end filling labels

        //Filling datasets
        $datasets = [];
        foreach ($rows as $key => $value) {
            $datasets[0]['data'][] = (int)$value->total;
        }
        //End filling datasets

        $json['type'] = 'bar';
        $json['data']['datasets'] = $datasets;      

        switch ($format) {
            case 'json':
                    return response()->json($json);
                break;
        }
    }

    private function getColor($index = 0)
    {
    	$colors = ["#F1C44C", "#EE6D98", "#BBD2E8", "#FCFBE3", "#CDD7B6", "#74C4AC"];
        if(array_key_exists($index, $colors)) {
            return $colors[$index];    
        }
    	return "#EE6D98";
    }
}
