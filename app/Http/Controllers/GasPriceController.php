<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;

use Carbon\Carbon;

use Contugas\Models\GasPrice;

use Validator;

class GasPriceController extends Controller
{
    public function index(Request $request, $format = 'json')
    {
        $now = Carbon::now();
        $items = GasPrice::orderBy('created_at', 'desc')->get();
        $lastItem = GasPrice::orderBy('created_at', 'desc')->first();
        switch ($format) {
            case 'json':
                $json = [
                    'message' => 'Se cargo listado de precios exitosamente',
                    'data' => $items
                ];
                return response()->json($json);
                break;

            case 'html':
                return view('gas-prices.index', compact(['now', 'lastItem']));
                break;
        }
    }

    public function store(Request $request, $format = 'json')
    {
        $validator = Validator::make($request->all(), [
                'year' => 'required',
                'month' => 'required',
                'value' => 'required'
            ]);

        if ($validator->fails()) {
            return response()->json([
                    'message' => 'Se han encontrado errores.',
                    'errors' => $validator->errors()
                ], 422);
        }

        $gas = new GasPrice();
        $gas->year = $request->year;
        $gas->month = $request->month;
        $gas->value = $request->value;
        $gas->save();

        $json = [
            'message' => 'Fue registrado exitosamente.',
            'data' => $gas
        ];

        return response()->json($json);
    }

    public function destroy(Request $request, $id)
    {
    	$item = GasPrice::findOrFail($id);
        $item->delete();

        $json = [
            'message' => 'Eliminado exitosamente'
        ];

        return response()->json($json);
    }
}
