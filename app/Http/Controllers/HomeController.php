<?php

namespace Contugas\Http\Controllers;

use Contugas\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $user_module_is_visible = Auth::guard('admin')->user()->isRoot();
        return view('admin.home', ['user_module_is_visible' => $user_module_is_visible]);
    }
}
