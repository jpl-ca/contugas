<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;
use Contugas\Models\Company;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Contugas\Models\CompanyPlace;
use Contugas\Models\Merchant;

class CompanyController extends Controller
{
    public function index(Request $request, $format = 'json')
    {
        $items = Company::orderBy('created_at', 'desc')->simplePaginate(20);

        if($format == 'json'){
            $data = $items;
            return response()->json($data);
        }else if($format == 'html'){
            return view('companies.index', compact('items'));
        }else if($format == 'xls'){
            Excel::create('Alianzas', function($excel) use ($items) {
                $excel->sheet('alianzas', function($sheet) use ($items) {
                    $sheet->row(1, ['id','Titulo', '', 'Creación', 'Actualización']);
                    $sheet->fromArray($items->toArray()['data'], null, 'A2', false, false);
                });

            })->download();
        }
    }

    public function create(Request $request, $format = 'html')
    {
        return view('companies.create');
    }

    public function store(Request $request, $format = 'json')
    {   
        $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'unique:merchants,email'
            ]);

        if($validator->fails()){
            return response()->json([
                    'message' => 'Error al crear',
                    'errors' => $validator->errors()
                ], 422);
        }

        $item = new Company;
        $item->name = $request->name;
        $item->address = $request->address;
        $item->description = $request->description;
        $item->ruc = $request->ruc;
        $item->save();

        if($request->has('company_places') && is_array($request->company_places)){
            foreach ($request->company_places as $company_place) {
                if(isset($company_place['lat']) &&  isset($company_place['lng'])){
                    $new_company_place = new CompanyPlace;
                    $new_company_place->lat = $company_place['lat'];
                    $new_company_place->lng = $company_place['lng'];
                    $item->company_places()->save($new_company_place);
                }
            }
        }

        if($request->has('email') && $request->has('password')){
            $new_merchant = new Merchant;
            $new_merchant->name = $item->name;
            $new_merchant->email = $request->email;
            $new_merchant->password = \Hash::make($request->password);
            $item->merchants()->save($new_merchant);
        }

        if($format == 'json'){
            return response()->json([
                    "message" => "Item creado exitosamente",
                    "data" => [
                        "item" => $item
                    ]
                ]);
        }
    }

    public function show(Request $request, $id, $format = 'json')
    {
        $item = Company::findOrFail($id);

        if($request->has('fields')){
            $fields = explode(',', $request->fields);

            if(in_array('company_places', $fields)){
                $item->company_places;
            }

            if(in_array('merchants', $fields)){
                if($item->merchants()->exists()){
                    $merchant = $item->merchants->first();
                    $item->email = $merchant->email;
                }
            }
        }

        if($format == 'json'){
            return response()->json([
                    "data" => [
                        "item" => $item->toArray()
                    ]
                ]);
        }else{
            return view('companies.show');
        }
    }
    
    public function edit(Request $request, $id, $format = 'html')
    {
        $item = Company::find($id);
        return view('companies.edit', compact('item'));
    }

    public function update(Request $request, $id, $format = 'json')
    {
        $validator = Validator::make($request->all(), [
                'name' => 'required'
            ]);

        if($validator->fails()){
            return response()->json([
                    'message' => 'Error al actualizar item',
                    'errors' => $validator->errors()
                ], 422);
        }

        $item = Company::findOrFail($id);
        $item->name = $request->name;
        $item->address = $request->address;
        $item->description = $request->description;
        $item->ruc = $request->ruc;
        $item->save();

        if($request->has('company_places') && is_array($request->company_places)){
            $item->company_places()->delete();
            foreach ($request->company_places as $company_place) {
                if(isset($company_place['lat']) &&  isset($company_place['lng'])){
                    $new_company_place = new CompanyPlace;
                    $new_company_place->lat = $company_place['lat'];
                    $new_company_place->lng = $company_place['lng'];
                    $item->company_places()->save($new_company_place);
                }
            }
        }

        if($request->has('email')){

            $new_merchant = $item->merchants()->first();
            if(!isset($new_merchant)){
                $new_merchant = new Merchant;
            }
            $new_merchant->name = $item->name;
            $new_merchant->email = $request->email;

            if($request->has('password')){
                $new_merchant->password = \Hash::make($request->password);    
            }
            
            $item->merchants()->save($new_merchant);
        }

        if($format == 'json'){
            return response()->json([
                    "message" => "Item actualizado exitosamente",
                    "data" => [
                        "item" => $item
                    ]
                ]);
        }
    }

    public function destroy(Request $request, $id, $format = 'json')
    {
        $item = Company::findOrFail($id);
        $item->delete();
        if($format == 'json'){
            return response()->json([
                    'message' => 'Eliminado exitosamente',
                ]
                );
        }
    }
}
