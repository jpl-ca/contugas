<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;

use Contugas\Http\Requests;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

use Log;

use Contugas\Models\AppComponent;
use Contugas\Libraries\Pusher;


class TempController extends Controller
{
	public function invoice(Request $request, $filename)
    {	
    	if(empty($filename) || strlen($filename)>=20){
    		return response('no permitido');
    	}

        $path = storage_path('app/invoices/'. $filename);
        return response()->download($path);
    }
}
