<?php

namespace Contugas\Http\Requests;

use Contugas\Http\Requests\Request;

class StoreCouponRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|unique:coupons,id',
            'title' => 'required|max:255',
            'description' => 'required|max:1000',
            'img_url' => 'required|url',
            'max_swaps' => 'required|integer',
            'user_swaps' => 'required|integer',
            'start_date' => 'required|date_format:d-m-Y',
            'end_date' => 'required|date_format:d-m-Y|after:start_date',
            'active' => 'required|boolean',
            'merchants' => 'required|array'
        ];
    }
}
