<?php

namespace Contugas\Http\Middleware;

use Closure;

use Daylight\Http\Response;
use Contugas\Libraries\Api;

class VerifyContugasApiData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( Api::validateDeviceType($request) )
        {
            if(! Api::validateToken($request) )
            {
                if(  $request->has('client_id') && $request->has('client_secret') && Api::validateData($request) )
                {
                    return $next($request);
                }

                return responseJsonUnauthorized(['message' => 'No existe una sesión']);
            }else{
                return $next($request);
            }
        }else{
            return view('errors.401');
        }
    }
}
