'use strict';

var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module("app", ['hSweetAlert']);

app.controller('adminUserController', ['$scope', '$http', 'sweet', function($scope, $http, sweet){
	$scope.current_page = 1;
	$scope.items = [];
	$scope.item = null;

	$scope.load_items = function(){
		var params = {
			page: $scope.current_page
		};

		$http({
			method: 'GET',
			url: base_url + '/admin/admin-users/index.json',
			params: params 
		}).then(function(response){

			$scope.items = response.data.data;
		}, function(response){

			$scope.items = response;
		});
	}

	$scope.load_item = function(id){
		if(!id){
			throw new Error("Id parameter is not present");
		}
		$http({
			method: 'GET',
			url: base_url + '/admin/admin-users/' + id + '.json'
		}).then(function(response){
			// When response is successful
			$scope.item = response.data.data.item;

		}, function(response){
			// When response is bad

		})
	}

	$scope.save = function(){

		var default_method = 'POST';
		var default_url = base_url + '/admin/admin-users';

		// If item exists then it will be updated
		if($scope.item && $scope.item.id){
			default_method = 'PUT';
			default_url = base_url + '/admin/admin-users/' + $scope.item.id;
		}

		$http({
			method: default_method,
			url: default_url,
			data: $scope.item
		}).then(function (response){
			// Cleaning form
			$scope.clear_form();

			//Show success message
			sweet.show("", "¡ Guardado exitosamente !", "success");
		}, function(response){

			if(response.status == 422 && response.data && response.data.errors){
				var errors = response.data.errors;
				var error_text = "";
				for(var i in errors){
					error_text += errors[i] + "\n";
				}

				sweet.show("¡ Ups !", 'Errores:  \n' + error_text, "error");

			}else{
				sweet.show("¡ Ups !", 'Ha sucedido un error inesperado', "error");
				$scope.error = response.data;
			}
			
		});
	}

	$scope.destroy_item = function(id){

		if(!id){
			throw new Error("Id parameter is not present");
		}

		$http({
			method: 'DELETE',
			url: base_url + '/admin/admin-users/' + id
		}).then(function(response){
			//Show success message
			sweet.show("", "¡ Eliminado exitosamente !", "success");

			// If response is successfull
			if(!$scope.items || !($scope.items instanceof Array) ){
				//return false;
			}

			for(var i in $scope.items){

				if($scope.items[i].id == id){
					$scope.items.splice(i, 1);
				}
			}
		}, function(response){
			// If response is bad
			sweet.show("¡ Ups !", 'Ha sucedido un error inesperado', "error");
		});
	}

	$scope.clear_form = function(){

		document.getElementById('item-form').reset();
	}

	$scope.load_next_items = function(){
		$scope.current_page += 1;
		$scope.load_items();
	}

	$scope.load_previous_items = function(){
		$scope.current_page -= 1;
		if($scope.current_page<=0) $scope.current_page = 1;
		$scope.load_items();
	}

}]);