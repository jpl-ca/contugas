'use strict';

var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module("app", ['hSweetAlert']);


app.controller("GasPricesController", ['$scope', '$http', '$location', '$timeout', 'sweet', '$window',
  function($scope, $http, $location, $timeout, sweet, $window) {

  	$scope.year = null;
    $scope.month = null;
    $scope.consumptionExample = 10;

    $scope.save = function() {

      var url = base_url + '/admin/gas-prices';
      var params = {};

	  if ($scope.year) {
			params.year = $scope.year;
	  }

	  if ($scope.month) {
			params.month = $scope.month;
	  }

	  if ($scope.priceOfGas) {
	  	params.value = $scope.priceOfGas;
	  }

      $http({
	      	method: 'POST',
	      	url: url,
	      	data: params
      	})
      	.success(function(response) {
      		console.log("Succesful response");
      		console.log(response);
      		$scope.loadItems();
      		sweet.show('', response.message, 'success');
      	})
      	.error(function(response) {
      		var message = '';
      		if (response.errors) {
      			for (var i in response.errors) {
      				message += response.errors[i];
      			}	
      		}
      		
      		sweet.show('', message, 'error');
      	});
    }

    $scope.loadItems = function() {
    	var url = base_url + '/admin/gas-prices/index.json'
    	$http({
    		method: 'GET',
    		url: url
    	})
    	.success(function(response) {
    		$scope.items = response.data;
    	})
    	.error(function(response) {

    	});
    }

    $scope.deleteItem = function(id) {
    	var url = base_url + '/admin/gas-prices/' + id;

    	sweet.show({
             title: "",
             text: "¿Seguro que desea eliminar este cliente?",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Si, eliminar!",
             cancelButtonText: "No, cancelar!",
             closeOnConfirm: true,
             closeOnCancel: true }, 
          function(isConfirm){
          		if (isConfirm) {
          			$http({
			    		method: 'DELETE',
			    		url: url
			    	})
			    	.success(function(response) {
			    		console.log(response);
			    		$scope.loadItems();
			    	})
			    	.error(function(response) {
			    		console.log(response);
			    	});	
          		}
          });
    }

    $scope.calculateSavedMoney = function() {
      var itemsToCalculate = [];
      var totalSavedMoney = 0;

      totalSavedMoney = ($scope.consumptionExample * 0.1 * $scope.priceOfGas) * 6;

      return totalSavedMoney;
    }

}]);
