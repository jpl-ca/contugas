'use strict';

var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module("app", ['ngMap','hSweetAlert']);
/* controlador para ng-map*/
app.controller("mapCtrl", function($scope, NgMap) {
  var vm = this;
        NgMap.getMap().then(function(map) {
          vm.showCustomMarker= function(evt) {
            map.customMarkers.foo.setVisible(true);
            map.customMarkers.foo.setPosition(this.getPosition());
          };
          vm.closeCustomMarker= function(evt) {
            this.style.display = 'none';
          };
        });
});

/* controlador para  alianzas*/
app.controller("comapniesCtrl", function($scope, $http, sweet) {
      var vm = this;
      $scope.conpanies = [];
      $scope.pages = [];
      $scope.current_page = 1;    
      $scope.conpanie = {};
      $scope.conpanie["company_places"] = new Array();

      $scope.$watch('current_page', function(value) {
         if (value >= 1) {
            $scope.load_items();
         };
      })

      $scope.page_anterior = function() {
        $scope.current_page = $scope.current_page - 1;
      };

      $scope.page_siguiente = function() {
        $scope.current_page = $scope.current_page + 1;
      };

      $scope.load_items = function() {
         $http.get(base_url +'/api/companies?page=' + $scope.current_page).success(function(data) {
            $scope.conpanies = data.data;
            $scope.current_page = data.current_page;
          });   
      }

      /* agregar alianza*/
      $scope.addCompanie = function() {
             $http.post(base_url +'/api/companies', $scope.conpanie)      
                .success(function(res){
                    if (res.message == 'Item creado exitosamente') {
                        $scope.limpiar();
                        sweet.show('', res.message, 'success');
                    }
                    if (res.message == 'Error al crear') {
                        if(res.errors.name == 'The name field is required.'){
                            sweet.show('', 'Al menos complete el campo nombre.', 'error');
                        };
                        if(res.errors.email == 'The email has already been taken.'){
                            sweet.show('', 'El email ya ha sido registrado', 'error');
                        }; 
                    }                                                      
                })
                .error(function(res){
                  sweet.show('', 'Error inesperado', 'error');
                  });
          
      }

      $scope.delete_companie = function(id, index){
        sweet.show({
             title: "",
             text: "¿Seguro que desea eliminar esta alianza?",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Si, eliminar alianza!",
             cancelButtonText: "No, cancelar!",
             closeOnConfirm: true,
             closeOnCancel: true }, 
          function(isConfirm){ 
             if (isConfirm) {
                $http.delete(base_url +'/api/companies/' + id)
                .success(function(res) {
                  $scope.conpanies.splice(index, 1);
                       
                })
                .error(function(res) {
                    sweet.show('', 'Error inesperado', 'error');
                });
          } else {
                 
             }
          });          
       }

      $scope.limpiar = function() {
         $scope.conpanie = {};
         $scope.conpanie.company_places = [];
      }

      $scope.add_place_to_company = function(event){
        console.log("agregando place");
        if(event['latLng']){
          var new_company_place = {};
          new_company_place.lat = event.latLng.lat();
          new_company_place.lng = event.latLng.lng();
          if($scope.detcompanie && $scope.detcompanie['company_places']){
            $scope.detcompanie.company_places = [];
          }

          if($scope.conpanie && $scope.conpanie['company_places']){
            $scope.conpanie.company_places = [];
            $scope.conpanie.company_places.push(new_company_place);
          }

        }
      };
});
  
  /*detalle de alianza*/

app.controller("showCtrl", function($scope, $http, $location, sweet) {

      var path = window.location.pathname.split('/admin/companies/')[1];
      var id = path.split('/edit.html')[0];
      $scope.detcompanie = [];
      $scope.detcompanie.company_places = [];
      $scope.company_places = [];

      $scope.merchants = [];
          $http.get(base_url +'/api/companies/' + id + '?fields=company_places,merchants').success(function(data) {

            $scope.detcompanie = data.data.item;
          });


    /* editar alianza*/
      $scope.editCompanie = function() {

             $http.put(base_url +'/api/companies/' + id, $scope.detcompanie)      
                .success(function(res){

                    if (res.message == 'Item actualizado exitosamente') {
                        sweet.show('', res.message, 'success');
                    }
                    if (res.message == 'Error al actualizar item') {
                        if(res.errors.name == 'The name field is required.'){
                            sweet.show('', 'Al menos complete el campo nombre.', 'error');
                        };
                        if(res.errors.email == 'The email has already been taken.'){
                            sweet.show('', 'El email ya ha sido registrado', 'error');
                        }; 
                    }                                                      
                })
                .error(function(res){
                  sweet.show('', 'Error inesperado', 'error');
                  });  
      }

      $scope.add_place_to_company = function(event){

        if(event['latLng']){

          var new_company_place = {};
          new_company_place.lat = event.latLng.lat();
          new_company_place.lng = event.latLng.lng();
          $scope.detcompanie.company_places = [];
          $scope.detcompanie.company_places.push(new_company_place);  
          
        }

      };
});