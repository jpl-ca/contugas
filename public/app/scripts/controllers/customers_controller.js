'use strict';

var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module("app", ['hSweetAlert']);

app.controller('CustomersController', function($scope, $http, sweet, $window) {
	$scope.items = [];
	$scope.currentPage = 1;
	$scope.filterItems = [
			{
				name: 'DNI',
				field: 'id_number'
			},
			{
				name: 'Nombres',
				field: 'name'
			},
			{
				name: 'Número de Cliente',
				field: 'client_number'
			},
			{
				name: 'E-mail',
				field: 'email'
			},
		];

	$scope.loadItems = function() {
		var url = base_url + '/admin/customers/index.json';
		var response = [];
		var params = {};

		if ($scope.selectedFilter) {
			params.filter_field = $scope.selectedFilter.field;
			params.filter_value = $scope.searchText;
		}

		if ($scope.currentPage) {
			params.page = $scope.currentPage;
		}

		console.log("Params");
		console.log(params);
		$http({
			method: 'GET',
			url: url,
			params: params
		}).success(function(response) {
			console.log("Customers loaded");
			console.log(response);
			$scope.items = response.data
		}).error(function(response) {
			console.log("Error on load ");
		});
	}

	$scope.downloadXls = function() {
		var params = '';
		if ($scope.selectedFilter) {
			params += '&filter_field=' + $scope.selectedFilter.field + '&filter_value=' + $scope.searchText;
		}

		if ($scope.currentPage) {
			params += '&page=' + $scope.currentPage;
		}

		$window.open(base_url + '/admin/customers/index.xls?' + params);
	}

	$scope.destroyItem = function(id) {
		var url = base_url + '/admin/customers/' + id;
		var isConfirm = false;

		console.log(sweet);
		sweet.show({
             title: "",
             text: "¿Seguro que desea eliminar este cliente?",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Si, eliminar!",
             cancelButtonText: "No, cancelar!",
             closeOnConfirm: true,
             closeOnCancel: true }, 
          function(isConfirm){
          		if (isConfirm) {
          			$http({
						method: 'DELETE',
						url: url
					}).success(function(response) {
						sweet.show('', response.message, 'success');
					}).error(function(response) {
						sweet.show('', 'Error inexperado al eliminar cliente', 'error');
					});	
          		}
          });
	}

	$scope.loadPreviousItems = function() {
		$scope.currentPage -= 1;
		$scope.loadItems();
	}

	$scope.loadNextItems = function() {
		$scope.currentPage += 1;
		$scope.loadItems();
	}
});