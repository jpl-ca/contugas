'use strict';


var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module("app", []);

app.controller("appController", function($scope, $http,$timeout) {
      

         $scope.item = [];
         $http({
          url: base_url +'/api/admin/dashboard/index.json',
          method: 'get'
          })
         .success(function(data) {
            $scope.item = data;    
          });  

         

         $scope.update = function(item,x){
          $scope.amounts = {};
          $scope.amounts.amount_of_decrease_of_quotation_requests = item.new_quotation_requests;
          $scope.amounts.amount_of_decrease_of_suggestions = item.new_suggestions;
          $http.put(base_url +'/api/admin/dashboard', $scope.amounts)      
            .success(function(){
              $timeout ($scope.redirect(x), 500);   
            })    
          }

         $scope.redirect = function(x) {
          switch (x) {
            case "1":
              window.location="/admin/quotation-requests/index.html";  
              break;
            case "2":
              window.location="/admin/app-components/index.html";  
              break;
            case "3":
              window.location="/admin/coupons/index.html";  
              break;
            case "4":
              window.location="/admin/companies/index.html";  
              break;
            case "5":
              window.location="/admin/tips/index.html";  
              break;
            case "6":
              window.location="/admin/events/index.html";  
              break;
            case "7":
              window.location="/admin/suggestions/index.html";  
              break;
            case "8":
              window.location="/admin//index.html";  
              break;
            case "9":
              window.location="/admin//index.html";  
              break;
            case "10":
              window.location="/admin/admin-users/index.html";  
              break;
            case "11":
              window.location="/admin/system-configuration/index.html";  
              break;
            case "12":
              window.location="/admin/gas-prices/index.html";  
              break;
            case "13":
              window.location="/admin/customers/index.html";  
              break;
            default:
              window.location="/admin";  
          }          
        }

});
