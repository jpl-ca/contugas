'use strict';

var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module("app", ['hSweetAlert', 'ngFileUpload'  ]);


app.controller("addController", ['$scope', '$http', '$location', 'Upload', '$timeout', 'sweet', '$window',
  function($scope, $http, $location, Upload, $timeout, sweet, $window) {
        $scope.coupon = {user_swaps: 1, max_swaps: 1000};        
        $scope.companies = [];

         $scope.$watch('select_companie', function(value) {
            if(value){
              $scope.coupon.company_id = $scope.select_companie.id;
            }
           });

         $scope.categories = [];
         

         $scope.$watch('select_categorie', function(value) {
            if(value){
              $scope.coupon.coupon_category_id = $scope.select_categorie.id;
            }
           });

         $scope.$watch('coupon_id', function(value){
          if(value){
            $scope.load_coupon();
          }
         });

         $scope.$watch('coupon.coupon_category_id', function(value){
          if(value){
            $scope.load_categories();
          }
         }, true);

        $scope.$watch('coupon.company_id', function(value){
          if(value){
            $scope.load_companies();
          }
        }, true);
        $scope.provinces=[
              {name:'Ica', ubigeo_code:1001, selected: false},
              {name:'Chincha', ubigeo_code:1002, selected: false},
              {name:'Nazca', ubigeo_code:1003, selected: false},
              {name:'Pisco', ubigeo_code:1004, selected: false}];

          $scope.toggleSelection = function toggleSelection(provinceId) {

            if(!$scope.coupon['ubigeo_coupon_constraints']){
              $scope.coupon['ubigeo_coupon_constraints'] = new Array();
            }

            var constraints = $scope.coupon.ubigeo_coupon_constraints;

            for(var i in constraints){

              if(constraints[i].ubigeo_code == provinceId){
                constraints.splice(i, 1);
                return false;
              }
            }

            var new_constraint = {ubigeo_code: provinceId};
            constraints.push(new_constraint);

          };

        $scope.addcoupon = function(file) { 
            var start = document.getElementById("start").value;
            var end = document.getElementById("end").value;

            if(file){
              $scope.coupon.image_file = file;  
            }else{
              var file = {};
            }

            $scope.coupon.start_date = start;
            $scope.status = 'PENDING';
            $scope.coupon.end_date = end;

            var default_url = '/api/coupons';
            
           

            if($scope.coupon_id){
              default_url= '/api/coupons/' + $scope.coupon_id;
               $scope.coupon._method = 'PUT';
            }

            file.upload = Upload.upload({
              method: 'post',
              url: default_url,
              data: $scope.coupon,
            });

            file.upload.then(function (response) {
              file.result = response.data;
              sweet.show('', 'Guardado exitosamente', 'success');
              $timeout(function () {
                $scope.redirect();
              }, 2000);
            }, function (response) {
              if (response.status >= 500){
                  $scope.errorMsg = response.status + ': ' + response.data;
                  sweet.show('', 'Error Inesperado', 'error');
              }

              if (response.status >= 400){
                  $scope.errorMsg = response.status + ': ' + response.data;
                  sweet.show('', 'Error Inesperado', 'error');
              }

                if(response.status >= 200 && response.status< 300){
                  $timeout ($scope.redirect, 1000);
                }
            }, function (evt) {
              file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        }
        
        $scope.redirect = function() {
          window.location="/admin/coupons/index.html";  
        }

        $scope.fill_constraints_control = function(){
          console.log("Llenando contol");
          if($scope.coupon && $scope.coupon['ubigeo_coupon_constraints']){
            for(var i in $scope.coupon['ubigeo_coupon_constraints']){
              for(var j in $scope.provinces){
                console.log("Checking");
                console.log($scope.provinces[j].ubigeo_code);
                console.log("y");
                console.log($scope.coupon['ubigeo_coupon_constraints'][i]);
                if($scope.provinces[j].ubigeo_code == $scope.coupon['ubigeo_coupon_constraints'][i].ubigeo_code)
                {
                 $scope.provinces[j].selected = true; 
                 console.log("Se encontró uno");
                 console.log($scope.provinces[j]);
                }
              }
            }
          }
        }

        $scope.fill_categories_control = function(){
          console.log("fill categories control");
          if($scope.coupon){
            for(var j in $scope.categories){

              if( parseInt($scope.categories[j].id) == parseInt($scope.coupon.coupon_category_id )){
                  $scope.select_categorie = $scope.categories[j];
              }
            }
          }
        }

        $scope.fill_company_control = function(){
          console.log('Fill company control');
          if($scope.companies && $scope.coupon && $scope.coupon.company_id){
            for(var i in $scope.companies){
              console.log("comparando company control");
              console.log($scope.companies[i].id);
              console.log($scope.coupon.company_id);
              if(parseInt($scope.companies[i].id) == parseInt($scope.coupon.company_id)){
                $scope.select_companie = $scope.companies[i];
              }
            }
          }
        }

        $scope.load_categories = function(){
          $http({
            url: base_url +'/api/coupons/categories',
            method: 'get'
          })
         .success(function(data) {
            $scope.categories = data.data;
            $scope.fill_categories_control();
          });
        }

        $scope.load_coupon = function(){
          $http({
              method: 'get',
              url: base_url + '/api/coupons/' + $scope.coupon_id + '?fields=ubigeo_coupon_constraints,company'
            }).success(function(data){
                $scope.coupon = data.data.item;
                console.log("cargo cupon");
                console.log($scope.coupon);
                $scope.fill_constraints_control();
            });
        }

        $scope.load_companies = function(){
          $http({
          url: base_url +'/api/companies',
          method: 'get'
          })
         .success(function(data) {
            $scope.companies = data.data;
            $scope.fill_company_control();
          });
        }

        $scope.init = function(){
          $scope.load_categories();
          $scope.load_companies();
        }

        $scope.init();

}]);


app.controller("couponController",  function($scope, $http, sweet) {
      $scope.coupons = [];
      $scope.pages = [];
      $scope.current_page = 1;    
      $scope.$watch('current_page', function(value) {
         if (value >= 1) {
            $scope.load_items();
         };
      })

      $scope.page_anterior = function() {
        $scope.current_page = $scope.current_page - 1;
      };

      $scope.page_siguiente = function() {
        $scope.current_page = $scope.current_page + 1;
      };

      $scope.load_items = function() {
         $http.get(base_url +'/api/coupons?page=' + $scope.current_page + '&fields=ubigeo,company').success(function(data) {
            $scope.coupons = data.data;
            $scope.current_page = data.current_page;
          });   
      }


      $scope.delete_coupon = function(id, index){
        console.log(id, index);
        sweet.show({
             title: "",
             text: "¿Seguro que desea eliminar este cupon?",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Si, eliminar!",
             cancelButtonText: "No, cancelar!",
             closeOnConfirm: true,
             closeOnCancel: true }, 
          function(isConfirm){ 
             if (isConfirm) {
                $http.delete(base_url +'/api/coupons/' + id)
                .success(function(res) {
                  $scope.coupons.splice(index, 1);
                       
                })
                .error(function(res) {
                    sweet.show('', 'Error inesperado', 'error');
                });
          } else {
                 
             }
          });          
       }

       $scope.activa_desactiva = function(id, param){
          $scope.params = [];
          $scope.params._method = 'put';
          $scope.params.status = param;
          $http.put(base_url +'/api/coupons/' + id, {status:param})
          .success(function() {
             $scope.load_items();
          });
       }
      
});
  
