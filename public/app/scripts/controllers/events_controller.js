'use strict';

var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module("app", ['hSweetAlert', 'ngFileUpload' ]);



app.controller('MyCtrl', ['$scope', 'Upload', '$timeout', 'sweet', '$window', function ($scope, Upload, $timeout,sweet, $window) {
    $scope.addEvent = function(file) {
      $scope.evento.type = $scope.type;   
      var fecha = document.getElementById("date").value;
      $scope.evento.image_file = file;
      $scope.evento.date = fecha;
      if (file == null) {
            sweet.show('', 'Item creado exitosamente', 'success');
          }
      file.upload = Upload.upload({
        url: '/api/events',
        data: $scope.evento,
      });

      file.upload.then(function (response) {
        $scope.limpiar();  
        sweet.show('', 'Item creado exitosamente', 'success');
        file.result = response.data;
        $timeout(function () {
          $window.location.href = base_url + '/admin/events/index.html';
        }, 2000);
      }, function (response) {
        if (response.status > 0)
          $scope.errorMsg = response.status + ': ' + response.data;
          sweet.show('', 'Error Inesperado', 'error');
      }, function (evt) {
        file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        
      });
    }

    $scope.changet = function(value) {
         $scope.type = null;
         $scope.type = value;
      }



    $scope.limpiar = function() {
         $scope.type = null;
         $scope.picFile = null;
         document.getElementById("date").value = '';
         $scope.evento = '';
      }


}]);


app.controller("eventsController", function($scope, $http, sweet) {
      $scope.events = [];
      $scope.pages = [];
      $scope.current_page = 1;    
      $scope.$watch('current_page', function(value) {
         if (value >= 1) {
            $scope.load_items();
         };
      })

      $scope.page_anterior = function() {
        $scope.current_page = $scope.current_page - 1;
      };

      $scope.page_siguiente = function() {
        $scope.current_page = $scope.current_page + 1;
      };

      $scope.load_items = function() {
         $http.get(base_url +'/api/events?page=' + $scope.current_page).success(function(data) {
            $scope.events = data.data;
            $scope.current_page = data.current_page;
          });   
      }

      


      

      $scope.delete_event = function(id, index){
        sweet.show({
             title: "",
             text: "¿Seguro que desea eliminar este evento/sugerencia?",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Si, eliminar!",
             cancelButtonText: "No, cancelar!",
             closeOnConfirm: true,
             closeOnCancel: true }, 
          function(isConfirm){ 
             if (isConfirm) {
                $http.delete(base_url +'/api/events/' + id)
                .success(function(res) {
                  $scope.events.splice(index, 1);
                       
                })
                .error(function(res) {
                    sweet.show('', 'Error inesperado', 'error');
                });
          } else {
                 
             }
          });          
       }

    
      
});


app.controller("showController", ['$scope', '$http', '$location', 'Upload', '$timeout', 'sweet', '$window',
  function($scope, $http, $location, Upload, $timeout, sweet, $window) {
      var path = window.location.pathname.split('/admin/events/')[1];
      var id = path.split('/edit.html');
      $scope.dummy = [];
      $scope.detevent = [];
          $http.get(base_url +'/api/events/' + id).success(function(data) {
            $scope.detevent = data.data.item;
            if ($scope.detevent.type == 'raffle'){
            	$scope.type = 'Sorteo';
            }else if ($scope.detevent.type == 'event') {
            	$scope.type = 'Evento';
            };
            $scope.prov = [];
            $scope.prov.images = [$scope.detevent.image_url];
          });  


          $scope.changet = function(value) {
	      	 $scope.type = null;
	      	 $scope.type = value;	      	 
	      	 console.log($scope.type);
	      }
          
        $scope.editEvent = function(file) {
            $scope.detevent.type = $scope.type;   
            var fecha = document.getElementById("date").value;
            $scope.detevent.image_file = file;
            $scope.detevent.date = fecha;
            $scope.detevent._method = 'put';
            console.log($scope.detevent);
            if (file == null) {
                  sweet.show('', 'Item creado exitosamente', 'success');
                }
            file.upload = Upload.upload({
              method: 'post',
              url: '/api/events/' + id,
              data: $scope.detevent,
            });

            file.upload.then(function (response) {
              sweet.show('', 'Item actualizado exitosamente', 'success');
              file.result = response.data;
              $timeout(function () {
                $window.location.href = base_url + '/admin/events/index.html';
              }, 2000);
            }, function (response) {
              if (response.status > 0)
                $scope.errorMsg = response.status + ': ' + response.data;
                sweet.show('', 'Error Inesperado', 'error');
            }, function (evt) {
              file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
          }

}]);
