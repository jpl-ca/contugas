'use strict';


app.controller("appController", function($scope, $http) {
      $scope.suggestions = [];
      $scope.pages = [];
      $scope.current_page = 1;
      

      $scope.$watch('current_page', function(value) {
         if (value >= 1) {
            $scope.load_items();
         };
      })

      $scope.page_anterior = function() {
        $scope.current_page = $scope.current_page - 1;
      };

      $scope.page_siguiente = function() {
        $scope.current_page = $scope.current_page + 1;
      };

      $scope.load_items = function() {
         $http.get(base_url +'/api/suggestions?page=' + $scope.current_page + '&fields=user').success(function(data) {
            $scope.suggestions = data.data;
            $scope.current_page = data.current_page;
            console.log($scope.current_page);
          });   
      }

});

