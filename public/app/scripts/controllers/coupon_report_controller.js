'user strict';

var app = angular.module('app', ['chart.js']);

app.controller('CouponReportController', function($scope, $http) {
	$scope.load = function () {
		$scope.isLoadingItem = true;
		var params = {};

		if ($scope.startDate) {
			params.start_datetime = $scope.startDate;
		}

		if ($scope.endDate) {
			params.end_datetime = $scope.endDate;
		}

		if ($scope.selectedCompany) {
			params.company_id = $scope.selectedCompany.id;
		}

		if ($scope.selectedCouponCategory) {
			params.coupon_category_id = $scope.selectedCouponCategory.id;
		}

		console.log("Params to send");
		console.log(params);

		$http({
			method: 'GET',
			url: '/admin/reports/coupons/items.json',
			params: params
		}).success( function (response, status_code) {
			$scope.isLoadingItem = false;
			$scope.items = response;
			console.log($scope.items);

		}).error(function () {
			$scope.isLoadingItem = false;
		});
	}

	$scope.filterItems = function() {

		$scope.startDate = $("#start-date").val();
		$scope.endDate = $("#end-date").val();

		$scope.load();
	}

	$scope.loadCompanies = function() {

		$http({
			url: '/admin/companies/index.json'
		}).success(function (response) {
			console.log('Companies loaded');
			console.log(response);
			$scope.companies = response.data;
		}).error(function (response) {

		});
	}

	$scope.loadCouponCategories = function() {

		$http({
			url: '/api/coupons/categories'
		}).success(function (response) {
			console.log('Categories loaded');
			console.log(response);
			$scope.categories = response.data;
		}).error(function (response) {

		});
	}
	
	$scope.loadCouponSwapsOfEveryday = function(coupon_id)
	{
		console.log('Loading coupon:' + coupon_id);
		var sharpLineOptions = {
	          animation:false,
			  scaleOverride:true,
			  scaleSteps:9,
			  scaleStartValue:0,
			  scaleStepWidth:100
        };


		$http({
			method: 'GET',
			url: '/admin/reports/coupons/' + coupon_id + '/swaps/everyday.json',
		}).success(function (response) {
			var ctx = document.getElementById("myChart").getContext("2d");
			var myChart = null;
			myChart = new Chart(document.getElementById("myChart").getContext("2d"), {
			    type: 'line',
			    data: response.data,
			    options: sharpLineOptions
			});
		})
		.error(function (response) {

		});
	}

	$scope.setDefaultValues = function() {
		console.log('Setting default values');
		var today = new Date();
		var startDate = today.getFullYear() + '-01-01';
		var endDate = today.toISOString().slice(0,10);
		$scope.startDate = startDate;
		$scope.endDate = endDate;
		$('#start-date').val(startDate);
		$('#end-date').val(endDate);
	}

	$scope.openPopUp = function(item) {
		$('#chartModal').off();
		$('#chartModal').modal({show: true});
		$('#chartModal').on('shown.bs.modal', function () {
		  $scope.loadCouponSwapsOfEveryday(item.coupon_id);
		});
	}

	$scope.init = function() {
		console.log("setting default values");
		$scope.setDefaultValues();
		$scope.loadCompanies();
		$scope.loadCouponCategories();
		$scope.load();
	}

	$scope.init();
});


