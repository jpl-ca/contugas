'use strict';

var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module("app", ['hSweetAlert']);

app.controller('MerchantDashboardController', function($scope, $http, $window, sweet) {
    $scope.redirect = function(path) {
    	$window.location.href = path;
    }
});

app.controller('MerchantSwapController', function($scope, $http, $window, sweet) {

  $scope.$watch('userCoupon', function(value) {
    if (value && value.length > 0) {
      value = value.toUpperCase();
      $scope.userCoupon = value;
      $scope.coupon = null;
    }
  });

  $scope.findCoupon = function() {
    if (!$scope.userCoupon) {
       sweet.show('', "Ingrese un código de cupón", 'error');
       return false;
    }

    var url = base_url + '/api/merchant/coupons/' + $scope.userCoupon;
    $http({
      method: 'GET',
      url: url
    })
    .success(function(response) {
      console.log("successful response");
      console.log(response);
      $scope.coupon = response;
      $scope.showCoupon();
    })
    .error(function(response, statusCode) {
        var errorMessage = '"' + $scope.userCoupon + '"' + 'es un cupón no válido';
        sweet.show('', errorMessage, 'error');
        $scope.userCoupon = null;  
    });
  }

  $scope.showCoupon = function() {
    $('#couponModal').off();
    $('#couponModal').modal({show: true});
    $('#couponModal').on('shown.bs.modal', function () {
    });
  }

  $scope.swapCoupon = function () {
    var url = base_url + '/api/merchant/coupons/swap';
    var data = {
      coupon_code: $scope.coupon.coupon_code
    };

    $http({
      url: url,
      method: 'POST',
      data: data
      })
      .success(function() {
        $('#couponModal').modal('hide');
        sweet.show('', 'El cupón ' + $scope.coupon.coupon_code + ' ha sido canjeado exitosamente ', 'success');
      })
      .error(function(response, statusCode) {
        if (statusCode >= 400 && statusCode < 500) {
          var errorMessage = "";
          
          for (var i in response.errors) {
            for (var j in response.errors[i]) {
              errorMessage += response.errors[i][j] + '\n';
            }
          }

          sweet.show('', errorMessage, 'error');
          $scope.userCoupon = null;  
        } else {
          var errorMessage = '"' + $scope.userCoupon + '"' + ' no es un cupón válido';
          sweet.show('', errorMessage, 'error');
          $scope.userCoupon = null;  
        }
      });

      
  }
});

app.controller('MerchantHistoryController', function($scope, $http, $window, sweet) {
  $scope.currentPage = 1;

  $scope.loadItems = function() {
    var params = {};

    if ($scope.currentPage) {
      params.page = $scope.currentPage;
    }

    var url = base_url + '/merchant/coupons/history.json';

    $http({
      url: url,
      params: params
    })
    .success(function(response) {
      $scope.items = response.data;
    })
    .error(function(response) {

    });
  }
  
  $scope.loadNextPage = function() {
    $scope.currentPage += 1;
    $scope.loadItems();
  }

  $scope.loadPreviousPage = function() {
    $scope.currentPage -= 1;
    $scope.loadItems();
  }
});