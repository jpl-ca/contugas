'use strict';

var app = angular.module('app', ['chart.js']);

app.controller('ReportController', function($scope, $http) {
	$scope.selectedReport = null;
	$scope.listOfReports = {
		categories: [
			{
				name: 'Solicitudes de presupuesto',
				items: [
					{
						url: '/admin/reports/quotation-request/month-and-district.json',
						name: 'Solicitudes de Presupuesto por meses y por provincia'
					},
					{
						url: '/admin/reports/quotation-request/district.json',
						name: 'Solicitudes de Presupuesto por Distrito'
					}
				]
			},
			{
				name: 'Consejos',
				items: [
					{
						url: '/admin/reports/tips/month.json',
						name: 'Consejos por meses'
					}
				]
			},
			{
				name: 'Cupones',
				items: [
					{
						url: '/admin/reports/coupons/month-category.json',
						name: 'Cupones por tipo/alianza por meses'
					},
					{
						url: '/admin/reports/coupons/swaps/month-category.json',
						name: ' Canjes de cupones por tipo/alianza por meses'
					},
					{
						url: '/admin/reports/coupons/swaps/year-category.json',
						name: 'Cupones canjeados por alianzas y por año'
					},
					{
						url: '/admin/reports/coupons/index.html',
						type: 'link',
						name: 'Canjes de cupones por alianza y por tipo'
					}
				]
			},
			{
				name: 'Eventos y sorteos',
				items: [
					{
						url: '/admin/reports/events/month.json',
						name: 'Eventos por meses'
					},
					{
						url: '/admin/reports/raffles/month.json',
						name: 'Sorteos por meses'
					},
					{
						url: '/admin/reports/events/attendees/month.json',
						name: 'Asistentes a eventos por meses'
					}
				]
			},
			{
				name: 'Sugerencias',
				items: [
					{
						url: '/admin/reports/suggestions/month.json',
						name: 'Sugerencias por meses'
					}
				]
			},
			{
				name: 'Clientes',
				items: [
					{
						url: '/admin/reports/users/month.json',
						name: 'Gráfico de barras por meses'
					}
				]
			}
		]
	};

	$scope.setSelectedReport = function (report) {
		$scope.selectedReport = report;
		$scope.loadChart(report.url);
	}

	$scope.loadChart = function (url) {
		$scope.noData = false;
		$("#myChart").hide();
		var params = {};
		if ($scope.startDate) {
			params.start_datetime = $scope.startDate;
		}

		if ($scope.endDate) {
			params.end_datetime = $scope.endDate;
		}
		console.log("Params:");
		console.log(params);

		$http({
			method: 'GET',
			url: url,
			params: params
		}).success(function (response, status) {
			if (status == 200) {
				$("#myChart").show();
				$scope.drawChart(response);
			}else{
				$scope.noData = true;	
			}
		}).error(function (response) {
			console.log("Se obtuvo un error inesperado");
		});
	}

	$scope.drawChart = function (data) {
		if (!data.type) {
			data.type = 'bar';
		}

		switch(data.type) {
			case 'bar' :
				var myLine = new Chart(document.getElementById("myChart").getContext("2d")).Bar(data.data);	
				break;
			case 'line' :
				var myLine = new Chart(document.getElementById("myChart").getContext("2d")).Line(data.data);	
				break;
			case 'horizontal-bar' :
				var myLine = new Chart(document.getElementById("myChart").getContext("2d")).HorizontalBar(data.data);
				break;
			default :
				throw Error('Tipo de chart inválido');	
		}
	}

	$scope.filterByDate = function () {
		console.log("Filtering by date");
		$scope.startDate = $('#startDate').val();
		$scope.endDate = $('#endDate').val();
		if ($scope.selectedReport) {
			$scope.loadChart($scope.selectedReport.url);
		} 
	}

	$scope.setDefaults = function() {
		console.log('Setting default values');
		var today = new Date();
		var startDate = today.getFullYear() + '-01-01';
		var endDate = today.toISOString().slice(0,10);
		$scope.startDate = startDate;
		$scope.endDate = endDate;
		$('#startDate').val(startDate);
		$('#endDate').val(endDate);
	}

	$scope.downloadPdfOfSelectedReport = function() {
		console.log("Downloading pdf file");
		html2canvas($("#myChart"), {
            onrendered: function(canvas) {         
                var imgData = canvas.toDataURL(
                    'image/png');

                var doc = new jsPDF('landscape');
                
                if ($scope.selectedReport) {
                	doc.text(20, 20, $scope.selectedReport.name);
                }

                doc.addImage(imgData, 'PNG', 10, 30);
                doc.save('stats.pdf');
            }
        });
	}

	$scope.setDefaults();
});

$(document).on('ready', function () {
	$('#startDate').datepicker({
            language: 'es'
          });
	$('#endDate').datepicker({
            language: 'es'
          });
});