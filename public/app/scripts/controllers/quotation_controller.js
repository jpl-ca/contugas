'use strict';

var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module("app", ['ngMap','hSweetAlert']);
/* controlador para ng-map*/
app.controller("mapCtrl", function($scope) {
     

});


app.controller("quotationsController", function($scope, $http, sweet, $interval, $window) {

      $scope.quotations = [];
      $scope.places = [];
      $scope.pages = [];
      $scope.current_page = 1;    
      $scope.$watch('current_page', function(value) {
         if (value >= 1) {
            $scope.load_items();
         };
      })

      $scope.page_anterior = function() {
        $scope.current_page = $scope.current_page - 1;
      };

      $scope.page_siguiente = function() {
        $scope.current_page = $scope.current_page + 1;
      };

      $scope.load_items = function(province, start_date,end_date) {
          var params = [];
          params.page = $scope.current_page;
          params.fields = 'province,district';
          if(province != null){
            params.province_id = province;
          }
          if(start_date != null){
            params.start_date = start_date;
          }
          if(end_date != null){
            params.end_date = end_date;
          }

         $http({
          url: base_url +'/api/quotation-requests',
          method: 'get',
          params: params
          })
         .success(function(data) {
            $scope.quotations = data.data;
            $scope.current_page = data.current_page;                   
          });

      }



      $scope.delete_quotation = function(id, index){
        sweet.show({
             title: "",
             text: "¿Seguro que desea eliminar esta solicitud?",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Si, eliminar solicitud!",
             cancelButtonText: "No, cancelar!",
             closeOnConfirm: true,
             closeOnCancel: true }, 
          function(isConfirm){ 
             if (isConfirm) {
                $http.delete(base_url +'/api/quotation-requests/' + id)
                .success(function(res) {
                  $scope.quotations.splice(index, 1);
                       
                })
                .error(function(res) {
                    sweet.show('', 'Error inesperado', 'error');
                });
          } else {
                 
             }
          });          
       };

       /* filtros*/
       $scope.provinces = [];
       $scope.selected_province = [];
       var params = [];
          params.fields = 'provinces';
         $http({
          url: base_url +'/api/ubigeo/departaments/10',
          method: 'get',
          params: params
          })
         .success(function(data) {
            $scope.provinces = data.data.provinces;           
          });  

         $scope.search = function() {
            var start = document.getElementById("start").value;
            var end = document.getElementById("end").value;
            $scope.load_items($scope.selected_province.code,start,end);
         }


         /*mapa*/

          $scope.$on('mapInitialized', function (event, map) {
            $scope.objMapa = map;
         });

         $scope.showInfoWindow = function (event, id) {
            var infowindow = new google.maps.InfoWindow();
            var center = new google.maps.LatLng(id[0].lat,id[0].lng);
            infowindow.setContent(
                '<h5>' + id[0].name + '</h5>');

            infowindow.setPosition(center);
            infowindow.open($scope.objMapa);
         };

         $scope.downloadFileUrlOfCurrentSelection = function() {
            var params = [];
            var start_date = document.getElementById("start").value;
            var end_date = document.getElementById("end").value;
            params.page = $scope.current_page;
            params.fields = 'province,district';
            

            var url = base_url + '/admin/quotation-requests/index.xls?fields=province,district'
            
            if (start_date) {
              url += '&start_date=' + start_date;
            }

            if (end_date) {
              url += '&end_date=' + end_date;
            }

            if ($scope.selected_province && $scope.selected_province.code) {
              url += '&province_id=' + $scope.selected_province.code;
            }

            $window.open(url);
         }
});


app.controller("showCtrl", function($scope, $http, $location, sweet) {

      var path = window.location.pathname.split('/admin/quotation-requests/')[1];
      var id = path.split('/edit.html');
      $scope.detquotation = [];
      $scope.company_places = [];
      $scope.merchants = [];
          $http.get(base_url +'/api/quotation-requests/' + id + '?fields=province,district' ).success(function(data) {
            $scope.detquotation = data.data.item;
            $scope.company_places = data.data.item.company_places;
            $scope.merchants = data.data.item.merchants;
          });  


});