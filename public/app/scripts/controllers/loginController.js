'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .controller('LoginCtrl', ['$scope', '$http', '$state', '$cookieStore', '$window', function ($scope, $http, $state, $cookieStore, $window) {
    $scope.password = null;
    $scope.email = null;
    $scope.invalidData = false;

    $scope.doLogin = function()
    {
		$http.post(
			'/api/admin/auth/login',
			{
				email: $scope.email,
				password: $scope.password
			}
		)
		.then(function(response){
			console.log('Login response: ' + response.status);
			if(response.status == 200) {
				$cookieStore.put('user', response.data);
				$window.location.href = '/admin';
			}
			else
			{
				$scope.invalidData = true;
			}
		},function(error){
			console.log('error');
			console.log(error);
		});
    }

    $scope.doLogout = function()
    {
		$http.get('/api/admin/auth/logout')
		.then(function(response){
			$state.transitionTo('login');
		},function(error){
			console.log('error');
			console.log(error);
		});
    }

}]);