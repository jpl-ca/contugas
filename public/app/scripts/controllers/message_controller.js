'use strict';

var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module("app", ['hSweetAlert', 'ngFileUpload'  ]);


app.controller("addController", ['$scope', '$http', '$location', 'Upload', '$timeout', 'sweet', 
  function($scope, $http, $location, Upload, $timeout, sweet) {

        $scope.provinces = [];
       var params = [];
          params.fields = 'provinces';
         $http({
          url: base_url +'/api/ubigeo/departaments/10',
          method: 'get',
          params: params
          })
         .success(function(data) {
            $scope.provinces = data.data.provinces;           
          }); 

      $scope.sections = [];
         $http({
          url: base_url +'/api/app-components/sections',
          method: 'get'
          })
         .success(function(data) {
            $scope.sections = data.data;           
          });  

         

       $scope.message = {};
       $scope.select_section = {};
       $scope.select_prov = {};
      $scope.$watch('select_section', function() {
            $scope.message.section = $scope.select_section.machine_name;
           })
      $scope.$watch('select_prov', function() {
            $scope.message.ubigeo_code = $scope.select_prov.code;
           })


        $scope.addMessage = function(file) { 
            var starts = document.getElementById("starts").value;
            var ends = document.getElementById("ends").value;
            $scope.message.image_file = file;
            $scope.message.starts = starts;
            $scope.message.ends = ends;
            
            file.upload = Upload.upload({
              method: 'POST',
              url: '/api/app-components',
              data: $scope.message,
            });

            file.upload.then(function (response) {
              $timeout(function () {
                file.result = response.data;
              });
              console.log(response);
              sweet.show('', 'Item guardado exitosamente', 'success');

            }, function (response) {
              if (response.status > 0)
                $scope.errorMsg = response.status + ': ' + response.data;
                sweet.show('', 'Error Inesperado', 'error');
            }, function (evt) {
              file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
          }


          /*editar*/

          var path = window.location.pathname.split('/admin/app-components/')[1];
          var id = path.split('/edit.html');
          $scope.detmessage = [];
              $http.get(base_url +'/api/app-components/' + id).success(function(data) {
                $scope.detmessage = data.data.item;
                $scope.prov = [];
                $scope.prov.images = [$scope.detmessage.image_url];
              });  

           $scope.editMessage = function(file) {
              var starts = document.getElementById("starts").value;
              var ends = document.getElementById("ends").value;
              $scope.detmessage.image_file = file;
              $scope.detmessage._method = 'put';
              $scope.detmessage.section = $scope.select_section.machine_name;
              $scope.detmessage.ubigeo_code = $scope.selected_province.code;
              $scope.detmessage.starts = starts;
              $scope.detmessage.ends = ends;
              if (file == null) {
              sweet.show('', 'Item guardado exitosamente', 'success');
            }
              file.upload = Upload.upload({
                method: 'post',
                url: '/api/app-components/' + id,
                data: $scope.detmessage,
              });

              file.upload.then(function (response) {
                $timeout(function () {
                  file.result = response.data;
                });
              }, function (response) {
                if (response.status > 0)
                  $scope.errorMsg = response.status + ': ' + response.data;
                  sweet.show('', 'Error Inesperado', 'error');
              }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                sweet.show('', 'Item actualizado exitosamente', 'success');
              });
           }

}]);

app.controller("messageController",  function($scope, $http, sweet) {
      $scope.messages = [];
      $scope.pages = [];
      $scope.current_page = 1;    
      $scope.$watch('current_page', function(value) {
         if (value >= 1) {
            $scope.load_items();
         };
      })

      $scope.page_anterior = function() {
        $scope.current_page = $scope.current_page - 1;
      };

      $scope.page_siguiente = function() {
        $scope.current_page = $scope.current_page + 1;
      };

      $scope.load_items = function() {
         $http.get(base_url +'/api/app-components?page=' + $scope.current_page + '&fields=ubigeo').success(function(data) {
            $scope.messages = data.data;
            $scope.current_page = data.current_page;
          });   
      }


      $scope.delete_message = function(id, index){
        sweet.show({
             title: "",
             text: "¿Seguro que desea eliminar este mensaje?",
             type: "warning",
             showCancelButton: true,
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Si, eliminar!",
             cancelButtonText: "No, cancelar!",
             closeOnConfirm: true,
             closeOnCancel: true }, 
          function(isConfirm){ 
             if (isConfirm) {
                $http.delete(base_url +'/api/app-components/' + id)
                .success(function(res) {
                  $scope.messages.splice(index, 1);
                       
                })
                .error(function(res) {
                    sweet.show('', 'Error inesperado', 'error');
                });
          } else {
                 
             }
          });          
       }
      
});
  
