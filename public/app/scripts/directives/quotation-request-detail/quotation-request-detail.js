'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('sbAdminApp')
	.directive('quotationRequestDetail',function(){
		return {
	        templateUrl:'app/scripts/directives/quotation-request-detail/quotation-request-detail.html',
	        restrict: 'E',
	        replace: true,
	        scope: {},
	        controller: ['$scope', '$http', '$attrs', function($scope, $http, $attrs) {
	        	$scope.incidents = {};
	        	$attrs.$observe('quotationRequestId', function(value) {
				    if (value) {
	        			$scope.quotationRequestId = $attrs.quotationRequestId;
	        			$http.get('/api/quotation-request/show/'+$scope.quotationRequestId).then(function(response){
							$scope.quotationRequest = response.data.data;
						},function(error){
							console.log('error');
							console.log(error);
						});
				    }
				  });


		    }]
    	}
	});


