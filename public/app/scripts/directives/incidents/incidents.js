'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('sbAdminApp')
	.directive('incidents',function(){
		return {
	        templateUrl:'app/scripts/directives/incidents/incidents.html',
	        restrict: 'E',
	        replace: true,
	        scope: {},
	        controller: ['$scope', '$http', function($scope, $http) {
	        	$scope.incidents = {};

	        	$http.get('/api/incident').then(function(response){
					$scope.incidents = response.data.data.data;
				},function(error){
					console.log('error');
					console.log(error);
				});
		    }]
    	}
	});


