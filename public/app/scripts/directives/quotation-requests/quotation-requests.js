'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('sbAdminApp')
	.directive('quotationRequests',function(){
		return {
	        templateUrl:'app/scripts/directives/quotation-requests/quotation-requests.html',
	        restrict: 'E',
	        replace: true,
	        scope: {},
	        controller: ['$scope', '$http', function($scope, $http) {
	        	$scope.quotationRequests = {};

	        	$http.get('/api/quotation-request').then(function(response){
					$scope.quotationRequests = response.data.data.data;
				},function(error){
					console.log('error');
					console.log(error);
				});
		    }]
    	}
	});


