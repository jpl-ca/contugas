'use strict';
/**
 * @ngdoc overview
 * @name sbAdminApp
 * @description
 * # sbAdminApp
 *
 * Main module of the application.
 */
angular
  .module('sbAdminApp', [
    'oc.lazyLoad',
    'ui.router',
    'ui.bootstrap',
    'angular-loading-bar',
    'ngCookies',
    'uiGmapgoogle-maps'
  ])
  .factory('httpAuthInterceptor',['$q', '$injector', '$cookieStore', function ($q, $injector, $cookieStore) {
    return {
      request: function(config) {
            //config.headers = config.headers || {};
            config.headers['Device-Type'] = 'contugas-web';
            return config;
      },
      'responseError': function (response) {
        // NOTE: detect error because of unauthenticated user
        if ([401, 403].indexOf(response.status) >= 0) {
          // redirecting to login page
          console.log(response.data.message);
          $injector.get('$state').transitionTo('login');
          return response;
        } else {
          $cookieStore.remove('user');
          return response;
          //return $q.reject(rejection);
        }
      }
    };
  }])
  .run(['$rootScope', '$location', 'Auth', function ($rootScope, $location, Auth) {
      $rootScope.$on('$locationChangeStart', function (event) {
          if (!Auth.isLoggedIn()) {
              console.log('DENY');
              $location.path('/login');
          }
          else {
            var currentPath = $location.path();
            if (currentPath == '' || currentPath == '/login') {
              $location.path('/dashboard/home');
            }
          }
      });
  }])
  .config(['$stateProvider','$urlRouterProvider','$ocLazyLoadProvider','uiGmapGoogleMapApiProvider','$httpProvider',function ($stateProvider,$urlRouterProvider,$ocLazyLoadProvider,GoogleMapApiProvider,$httpProvider) {
    
    $httpProvider.interceptors.push('httpAuthInterceptor');

    GoogleMapApiProvider.configure({
      //key: 'your api key',
      v: '3.20',
      libraries: 'weather,geometry,visualization'
    });

    $ocLazyLoadProvider.config({
      debug:false,
      events:true,
    });

    $urlRouterProvider.otherwise('/login');

    $stateProvider
      .state('dashboard', {
        url:'/dashboard',
        templateUrl: 'app/views/dashboard/main.html',
        resolve: {
            loadMyDirectives:function($ocLazyLoad){
                return $ocLazyLoad.load(
                {
                    name:'sbAdminApp',
                    files:[
                    'app/scripts/directives/header/header.js',
                    'app/scripts/directives/header/header-notification/header-notification.js',
                    'app/scripts/directives/sidebar/sidebar.js',
                    'app/scripts/directives/sidebar/sidebar-search/sidebar-search.js'
                    ]
                }),
                $ocLazyLoad.load(
                {
                   name:'toggle-switch',
                   files:["app/bower_components/angular-toggle-switch/angular-toggle-switch.min.js",
                          "app/bower_components/angular-toggle-switch/angular-toggle-switch.css"
                      ]
                }),
                $ocLazyLoad.load(
                {
                  name:'ngAnimate',
                  files:['app/bower_components/angular-animate/angular-animate.js']
                }),
                $ocLazyLoad.load(
                {
                  name:'ngCookies',
                  files:['app/bower_components/angular-cookies/angular-cookies.js']
                }),
                $ocLazyLoad.load(
                {
                  name:'ngResource',
                  files:['app/bower_components/angular-resource/angular-resource.js']
                }),
                $ocLazyLoad.load(
                {
                  name:'ngSanitize',
                  files:['app/bower_components/angular-sanitize/angular-sanitize.js']
                }),
                $ocLazyLoad.load(
                {
                  name:'ngTouch',
                  files:['app/bower_components/angular-touch/angular-touch.js']
                })
            }
        }
    })
      .state('dashboard.home',{
        url:'/home',
        controller: 'MainCtrl',
        templateUrl:'app/views/dashboard/home.html',
        resolve: {
          loadMyFiles:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'sbAdminApp',
              files:[
              'app/scripts/controllers/main.js',
              'app/scripts/directives/notifications/notifications.js'
              ]
            })
          }
        }
      })
      .state('login',{
        templateUrl:'app/views/pages/login.html',
        url:'/login',
        controller: 'LoginCtrl',
        resolve: {
          loadMyFiles:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'sbAdminApp',
              files:[
              'app/scripts/controllers/loginController.js'
              ]
            })
          }
        }
    })
      .state('logout',{
        url:'/logout',
        controller: function($http, $state, $cookieStore){
          $http.get('/api/admin/auth/logout')
          .then(function(response){
            $cookieStore.remove('user');
            $state.transitionTo('login');
          },function(error){
            console.log('error');
            console.log(error);
          });
        }
    })
      .state('dashboard.incidents',{
        abstract: true,
        template: '<ui-view/>',
        url:'/incidentes'
    })
      .state('dashboard.incidents.list',{
        templateUrl:'app/views/dashboard/incidents/incidents.html',
        url:'/lista',
        resolve: {
          loadMyFiles:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'sbAdminApp',
              files:[
              'app/scripts/directives/incidents/incidents.js'
              ]
            })
          }
        }
    })
      .state('dashboard.incidents.detail',{
        templateUrl:'app/views/dashboard/incidents/incident-detail.html',
        url:'/detalle/:incidentId',
        controller: function ($scope, $stateParams) {
          $scope.incidentId = $stateParams.incidentId;
        },
        resolve: {
          loadMyFiles:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'sbAdminApp',
              files:[
              'app/scripts/directives/incident-detail/incident-detail.js'
              ]
            })
          }
        }
    })
      .state('dashboard.incidents.report',{
        templateUrl:'app/views/dashboard/incidents/incident-report.html',
        url:'/reporte',
        controller:'IncidentReportCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'chart.js',
              files:[
                'app/bower_components/angular-chart.js/dist/angular-chart.min.js',
                'app/bower_components/angular-chart.js/dist/angular-chart.css'
              ]
            }),
            $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['app/scripts/controllers/incidentReportContoller.js']
            })
          }
        }
    })
      .state('dashboard.quotation-requests',{
        abstract: true,
        template: '<ui-view/>',
        url:'/solicitudes-de-presupuesto'
    })
      .state('dashboard.quotation-requests.list',{
        templateUrl:'app/views/dashboard/quotation-requests/quotation-requests.html',
        url:'/lista',
        resolve: {
          loadMyFiles:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'sbAdminApp',
              files:[
              'app/scripts/directives/quotation-requests/quotation-requests.js'
              ]
            })
          }
        }
    })
      .state('dashboard.quotation-requests.detail',{
        templateUrl:'app/views/dashboard/quotation-requests/quotation-request-detail.html',
        url:'/detalle/:quotationRequestId',
        controller: function ($scope, $stateParams) {
          $scope.quotationRequestId = $stateParams.quotationRequestId;
        },
        resolve: {
          loadMyFiles:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'sbAdminApp',
              files:[
              'app/scripts/directives/quotation-request-detail/quotation-request-detail.js'
              ]
            })
          }
        }
    })
      .state('dashboard.quotation-requests.report',{
        templateUrl:'app/views/dashboard/quotation-requests/quotation-request-report.html',
        url:'/reporte',
        controller:'QuotationRequestsReportCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'chart.js',
              files:[
                'app/bower_components/angular-chart.js/dist/angular-chart.min.js',
                'app/bower_components/angular-chart.js/dist/angular-chart.css'
              ]
            }),
            $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['app/scripts/controllers/quotationRequestsReportContoller.js']
            })
          }
        }
    })

  }]);

    
