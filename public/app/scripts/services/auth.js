'use strict';

angular.module('sbAdminApp')
	.factory('Auth', ['$cookieStore', function($cookieStore){
		var user;

		return{
		    setUser : function(aUser){
		        $cookieStore.put('user', aUser);
		    },
		    isLoggedIn : function(){
		        return $cookieStore.get('user') != null;
		    }
		}
	}]);
