<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDistrictIdToQuotationRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotation_requests', function(Blueprint $table){
            $table->string('district_id')->nullable()->after('province_id');

            $table->foreign('district_id')
                ->references('code')
                ->on('ubigeo')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotation_requests', function(Blueprint $table){
            $table->dropForeign('quotation_requests_district_id_foreign');
            $table->dropColumn('district_id');
        });
    }
}
