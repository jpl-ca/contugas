<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRecipientMailboxOfQuotationRequestsFieldIntoSystemSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'system_settings' , function( Blueprint $table ){
            $table->string( 'recipient_mailbox_of_quotation_requests' )->after('recipient_mailbox_of_suggestions');

        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'system_settings' , function( Blueprint $table ) {
            $table->dropColumn( 'recipient_mailbox_of_quotation_requests' );
        } );
    }
}
