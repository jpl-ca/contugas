<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCouponCategoryIdToCouponTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coupons', function(Blueprint $table){
            $table->integer('coupon_category_id')->unsigned()->nullable()->after('id');

            $table->foreign('coupon_category_id')->references('id')->on('coupon_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coupons', function(Blueprint $table){
            $table->dropColumn(['coupon_category_id']);
        });
    }
}
