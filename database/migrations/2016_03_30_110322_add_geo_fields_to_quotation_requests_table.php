<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGeoFieldsToQuotationRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotation_requests', function(Blueprint $table){
            $table->decimal('lat', 17, 15)->after('comment');
            $table->decimal('lng', 17, 15)->after('lat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotation_requests', function(Blueprint $table){
            $table->dropColumn(['lat', 'lng']);
        });
    }
}
