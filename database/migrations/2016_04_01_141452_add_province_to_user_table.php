<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProvinceToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table){

            $table->string('province_text')->nullable()->after('id_number');
            $table->string('province_id')->nullable()->after('id_number');

            $table->string('district_text')->nullable()->after('id_number');
            $table->string('district_id')->nullable()->after('id_number');

            $table->string('address')->nullable()->after('name');
            $table->string('supply_state')->nullable()->after('name');

            $table->foreign('province_id')
                ->references('code')
                ->on('ubigeo')
                ->onCascade('cascade');

            $table->foreign('district_id')
                ->references('code')
                ->on('ubigeo')
                ->onCascade('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table){
            
            $table->dropForeign('users_province_id_foreign');
            $table->dropColumn('province_id');
            $table->dropColumn('province_text');

            $table->dropForeign('users_district_id_foreign');
            $table->dropColumn('district_id');
            $table->dropColumn('district_text');

            $table->dropColumn('address');
            $table->dropColumn('supply_state');
        });
    }
}
