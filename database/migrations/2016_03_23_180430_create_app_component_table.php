<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppComponentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_components', function(Blueprint $table){
            $table->increments('id');
            $table->string('section');
            $table->text('description')->nullable();
            $table->string('ubigeo_code')->nullable();
            $table->string('image_path');
            $table->dateTime('starts')->nullable();
            $table->dateTime('ends')->nullable();
            $table->boolean('visible')->default(true);
            $table->foreign('ubigeo_code')->references('code')->on('ubigeo');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('app_components');
    }
}
