<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminDashboardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_dashboards', function(Blueprint $table){

            $table->increments('id');
            $table->integer('admin_id')->unsigned()->unique();
            $table->integer('new_quotation_requests')->unsigned()->default(0);
            $table->integer('new_suggestions')->unsigned()->default(0);;

            $table->timestamps();

            $table->foreign('admin_id')
                ->references('id')
                ->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admin_dashboards');
    }
}
