<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;
use Contugas\Models\SystemSetting;
class SystemConfigTableSeeder extends Seeder
{
    public function run()
    {
        SystemSetting::create([
        		"main_mailbox" => "correo@contugas.com.pe",
        		"recipient_mailbox_of_suggestions" => "sugerencias@contugas.com.pe",
        		"recipient_mailbox_of_quotation_requests" => "ventas@contugas.com.pe"
        	]);
    }
}
