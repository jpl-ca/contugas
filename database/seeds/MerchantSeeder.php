<?php

use Illuminate\Database\Seeder;

class MerchantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$date = \Carbon\Carbon::now()->toDateTimeString();

		DB::table('merchants')->insert(array(
            [
                'name' => 'Pollo Gordo - La victoria',
                'email' => 'comercio@yopmail.com',
                'password' => \Hash::make('password'),
                'company_id' => 1,
                'created_at' => $date,
                'updated_at' => $date
            ]
		));
    }
}
