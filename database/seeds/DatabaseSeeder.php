<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UbigeoSeeder::class);
        $this->call(IncidentTypeSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(CompanySeeder::class);
        $this->call(MerchantSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(GasConsumptionTypeSeeder::class);
        $this->call(CouponCategoryTableSeeder::class);
        $this->call(CouponCategoryTableSeeder::class);
        $this->call(SystemConfigTableSeeder::class);
        Model::reguard();
    }
}
