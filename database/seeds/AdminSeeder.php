<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$date = \Carbon\Carbon::now()->toDateTimeString();

		DB::table('admins')->insert(array(
            [
                'name' => 'Admin',
                'email' => 'admin@yopmail.com',
                'password' => \Hash::make('password'),
                'created_at' => $date,
                'updated_at' => $date
            ],
            [
                'name' => 'Contugas',
                'email' => 'contugas@yopmail.com',
                'password' => \Hash::make('password'),
                'created_at' => $date,
                'updated_at' => $date
            ]
		));
    }
}
