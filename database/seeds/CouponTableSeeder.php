<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;
use Contugas\Models\Coupon;
class CouponTableSeeder extends Seeder
{
    public function run()
    {
        Coupon::create([
        	'id' => 'NYB56FAF208573B8',
        	'title' => 'Título del cupón',
        	'description' => 'Descripción del cupón'
        	]);
    }
}
