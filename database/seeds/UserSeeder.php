<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$date = \Carbon\Carbon::now()->toDateTimeString();

		DB::table('users')->insert(array(
            [
                'name' => 'Usuario',
                'client_number' => 111111,
                'id_number' => 22222222,
                'email' => 'usuario@yopmail.com',
                'password' => \Hash::make('password'),
                'created_at' => $date,
                'updated_at' => $date
            ],
            [
                'name' => 'Luis Sanchez',
                'client_number' => 852963,
                'id_number' => 44556677,
                'email' => 'luis.sanchez@yopmail.com',
                'password' => \Hash::make('password'),
                'created_at' => $date,
                'updated_at' => $date
            ],
            [
                'name' => 'Administrador Contugas',
                'client_number' => null,
                'id_number' => null,
                'email' => 'contugas@yopmail.com',
                'password' => \Hash::make('password'),
                'created_at' => $date,
                'updated_at' => $date
            ]
		));
    }
}
