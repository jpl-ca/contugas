<?php

use Illuminate\Database\Seeder;

class GasConsumptionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$date = \Carbon\Carbon::now()->toDateTimeString();

		DB::table('gas_consumptions')->insert(array(
			array(
				'user_id' => 1,
				'date' => '2015-07-30 00:00:00',
				'consumption' => 155.00,
				'amount_paid' => 74.00,
				'amount_saved' => 11.00,
				'created_at' => $date,
				'updated_at' => $date
			),
			array(
				'user_id' => 1,
				'date' => '2015-08-30 00:00:00',
				'consumption' => 188.00,
				'amount_paid' => 99.00,
				'amount_saved' => 18.00,
				'created_at' => $date,
				'updated_at' => $date
			),
			array(
				'user_id' => 1,
				'date' => '2015-09-30 00:00:00',
				'consumption' => 146.00,
				'amount_paid' => 64.00,
				'amount_saved' => 12.00,
				'created_at' => $date,
				'updated_at' => $date
			),
			array(
				'user_id' => 1,
				'date' => '2015-10-30 00:00:00',
				'consumption' => 120.00,
				'amount_paid' => 45.00,
				'amount_saved' => 15.00,
				'created_at' => $date,
				'updated_at' => $date
			),
			array(
				'user_id' => 1,
				'date' => '2015-11-30 00:00:00',
				'consumption' => 145.00,
				'amount_paid' => 65.00,
				'amount_saved' => 18.00,
				'created_at' => $date,
				'updated_at' => $date
			),
			array(
				'user_id' => 1,
				'date' => '2015-12-30 00:00:00',
				'consumption' => 170.00,
				'amount_paid' => 85.00,
				'amount_saved' => 14.00,
				'created_at' => $date,
				'updated_at' => $date
			),
		));
    }
}