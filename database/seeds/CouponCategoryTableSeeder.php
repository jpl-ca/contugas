<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Contugas\Models\CouponCategory;

class CouponCategoryTableSeeder extends Seeder
{
    public function run()
    {
        CouponCategory::create([
        	'name' => 'Restaurantes',
        	]);
        CouponCategory::create([
        	'name' => 'Otros',
        	]);
    }
}
