<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$date = \Carbon\Carbon::now()->toDateTimeString();

		DB::table('companies')->insert(array(
            [
                'name' => 'Pollo Gordo - La victoria',
                'ruc' => '12345678901',
                'created_at' => $date,
                'updated_at' => $date
            ]
		));
    }
}
