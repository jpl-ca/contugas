@extends('layouts.admin')
@section('js')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
<script type="text/javascript" src="{{ asset('app/scripts/controllers/gas_prices_controller.js') }}"></script>

@stop
@section('content')
    <section class="content-header">
      <h1>
        Ahorro en el consumo
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li class="active"><a>Ahorro en el consumo</a></li>
      </ol>
    </section>
    <section class="content" ng-app="app" ng-controller="GasPricesController" ng-init="year={{$now->year}};month={{$now->month}};loadItems();{{( $lastItem ? "priceOfGas={$lastItem->value}" : '')}}">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">

                    </div>                     
                    <div class="panel-body">
                        <div>
                            <div class="col-sm-12">
                                <div class='col-sm-6' class='text-center'>
                                    <div class='col-sm-1'>
                                    </div>
                                    <div class='col-sm-10'>
                                        <form class="form-inline" style='margin-top:15px;font-size:1.2em;'>
                                          <div class="form-group">
                                            <label for="exampleInputName2">Factor:</label>
                                            <input type="text" class="form-control" id="exampleInputName2" placeholder="0.00" ng-model='priceOfGas' size='4'>
                                            soles <span style='font-size:1.4em'>/</span> mᶟ
                                          </div>

                                          <button type="submit" class="btn btn-default" ng-click='save();' style='margin-left:15px;'>Guardar</button>
                                        </form>
                                    </div>
                                    <div class='col-sm-1'>
                                    </div>
                                </div>
                                <div class='col-sm-6'>
                                    <p>
                                    Ejemplo: Si el cliente consumió <span style='font-weight:bold;font-style:italic'>@{{consumptionExample}}mᶟ</span> el mensaje sería:
                                    </p>
                                    <blockquote style='font-style:italic'>
                                        <p>
                                            Al cambiarse a Contugas usted ha ahorrado S/. @{{calculateSavedMoney() | number:2}} en los últimos seis meses
                                        </p>
                                    </blockquote>
                                </div>
                                <div class='col-sm-12'>
                                    <h2>Historial</h2>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th style="">Fecha de registro</th>
                                                <th style="">Factor</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="" ng-repeat="item in items">
                                                <td>@{{item.created_at}}</td> 
                                                <td>@{{item.value}}</td>
                                                <td>
                                                    <a ng-click="deleteItem(item.id)" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="eliminar">
                                                        <span class="fa fa-trash" aria-hidden="true"></span>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop