<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-{{ $class }}">
            <i class="fa fa-btn fa-{{ $icon  }}"></i>{{ $text  }}
        </button>
    </div>
</div>