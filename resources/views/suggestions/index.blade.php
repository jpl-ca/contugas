@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/app2.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/suggestion_controller.js') }}"></script>
@yield('js')
@section('content')
    <section class="content-header">
      <h1>
        Sugerencias
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li class="active"><a>Sugerencias</a></li>
      </ol>
    </section>
    <section class="content" ng-app="app">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                    <a href="/admin/suggestions/index.xls?page=@{{current_page}}" class="btn btn-primary pull-right">Exportar</a>
                    </div>
                    <div class="panel-body">
                        <div>
                            <div class="col-sm-12" ng-controller="appController">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Contenido</th>
                                            <th>Número de cliente</th>
                                            <th>Nombre</th>
                                            <th>Provincia</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         
                                        <tr class="" ng-repeat="suggestion in suggestions">
                                            <td>@{{suggestion.id}}</td>
                                            <td>@{{suggestion.content}}</td>
                                            <td>@{{suggestion.user.client_number}}</td>
                                            <td>@{{suggestion.user.name || 'Anónimo'}}</td>
                                            <td>@{{suggestion.user.province_text}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <ul class="pagination pagination-sm">
                                    <li ng-class="{active:0}">
                                        <a href="#" ng-click="page_anterior()">Anterior</a>
                                    </li>
                                    <li>
                                        <a >@{{current_page}}</a>
                                    </li>
                                    <li><a href="#" ng-click="page_siguiente()">Siguiente</a>
                                    </li>
                                </ul>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop