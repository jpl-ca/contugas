@extends('layouts.admin')
@yield('content')
<script type="text/javascript"  src="{{ asset('jquery/2.1.4/jquery.min.js') }}"></script>
<script src="{{ asset('app/js/docs.js') }}"></script>
@yield('js')
@section('content')
	<style>
		.cont{
			overflow: hidden;
    		margin-bottom: 15px;
    		border-bottom: 1px solid silver;
    		padding-bottom: 15px;
		}
		.nolist{
			list-style: none;
			padding-left: 0;
			line-height: 1.6;
		}
		.nolist li span{
			margin-right: 5px;
		}
		.rowint{
			overflow: hidden;
    		margin-bottom: 15px;
    		padding-bottom: 15px;
		}
		.label-success {
		    background-color: #7BB635 !important;
		}
		.label {
		    border-radius: 6.25em;
		}
		legend{
			font-size: 27px;
		}
	</style>
 	<section class="content-header">
      <h1>
       	Ayuda
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li class="active"><a><i class="fa fa-question-circle"></i> Ayuda</a></li>
      </ol>
    </section>	
    <section class="content" ng-app="app">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
					<h3>Indice</h3>
					</div>
					<div class="box-body">
						<ul>
							<li><a href="#login">Inicio de Sesión</a></li>
			                <li><a href="#panelc">Panel de Control</a></li>
			                <li><a href="#solicitud">Solicitudes de Presupuesto</a></li>
			                <li><a href="#mensajes">Mensajes</a></li>
			                <li><a href="#cupones">Gestión de Cupones</a></li>
			                <li><a href="#alianzas">Gestión de Alianzas</a></li>
			                <li><a href="#consejos">Consejos</a></li>
			                <li><a href="#eventos">Eventos y Sorteos</a></li>
			                <li><a href="#sugerencias">Sugerencias</a></li>
			                <li><a href="#config">Reportes</a></li>
			                <li><a href="#usuarios">Usuarios</a></li>
			                <li><a href="#config">Configuración</a></li>
			                <li>
			                	<a href="{{url('/docs/Aplicaci%C3%B3n%20para%20Clientes%20-%20Medidas%20Imagenes.pdf')}}">
			                	Especificaciones de las imagenes
			                	</a>
			                </li>
			            </ul>
			            <hr>
			            <div id="login" class="cont">
			            	<div class="col-sm-12">
					            <legend>Inicio de sesión</legend>
					        </div>
			            	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-danger">1</span> Campo e-mail, deberá de escribir una dirección 
					                válida de correo electrónico</li>
					                <li><span class="label label-danger">2</span> Campo contraseña, deberá de escribir su contraseña.</li>
					                <li><span class="label label-danger">3</span> Checkbox de no cerrar sesión, servirá para 
					                mantener la sesión despues de cerrar la ventana del navegador.</li>
					                <li><span class="label label-danger">4</span> Botón de inicio de sesión, pulse este botón 
					                cuando haya completado los campos anteriores.</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/login.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
			            </div>
			            <div id="panelc" class="cont">
			            	<div class="col-sm-12">
					            <legend>Panel de Control</legend>
					        </div>
			            	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Menú de navegación, desde donde se podrá 
					                navegar entre los distintos módulos de la aplicación.</li>
					                <li><span class="label label-success">2</span> Servirá para minimizar y maximizar 
					                el menú de navegación.</li>
					                <li><span class="label label-success">3</span> Enlaces de acceso rápido, te llevarán a 
					                las acciones principales de la aplicación.</li>
					                <li><span class="label label-success">4</span> Botón de usuario y panel de acción, 
					                el usuario podrá editar datos de su cuenta en su perfil, así mismo podrá cerrar sesión.</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/dashboard.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
			            </div>
			            <div id="solicitud" class="cont">
			            	<div class="col-sm-12">
					            <legend>Solicitudes de Presupuesto</legend>
					        </div>
			            	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Listado de todas las solicitudes de presupuesto.</li>
					                <li><span class="label label-success">2</span> Al hacer click en cada una de las opciones podrá 
					                ver los resultados en el mapa o en un listado.</li>
					                <li><span class="label label-success">3</span> Servirá para filtrar las solictudes de presupuesto
					                por rango de fechas y provincias.</li>
					                <li><span class="label label-success">4</span> Servirá para ver la información completa de cada registro</li>
					                <li><span class="label label-success">5</span> El ícono del "tacho" sirve para eliminar el registro seleccionado.</li>
					                <li><span class="label label-success">6</span> Para exportar los resultados mostrados, hacer click en el botón "Exportar".</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/solicitud1.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
			            </div>
			            <div id="mensajes" class="cont">
			            	<div class="col-md-12">
					            <legend>Mensajes</legend>
					        </div>
			            	<div class="rowint col-md-12">
			            	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Listado de todos los mensajes.</li>
					                <li><span class="label label-success">2</span> Servirá para ver la información completa de cada registro.</li>
					                <li><span class="label label-success">3</span> El ícono del "lápiz" sirve para editar el registro seleccionado.</li>
					                <li><span class="label label-success">4</span> El ícono del "tacho" sirve para eliminar el registro seleccionado.</li>
					                <li><span class="label label-success">5</span> Al hacer click descargara un archivo XLS con todos los registros.</li>
					                <li><span class="label label-success">6</span> Para agregar un nuevo registro, hacer click en el botón "Crear Nuevo".</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/mensajes1.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
                        	<div class="rowint col-md-12">
                        	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Elegir una sección.</li>
					                <li><span class="label label-success">2</span> Elegir una provincia.</li>
					                <li><span class="label label-success">3</span> Establecer una fecha de inicio.</li>
					                <li><span class="label label-success">4</span> Establecer una fecha de fin.</li>
					                <li><span class="label label-success">5</span> Ingresar una descripción correspondiente al mensaje por crear.</li>
					                <li><span class="label label-success">6</span> Seleccionar una imagen desde su ordenador(debe tener las dimensiones
					                especificadas).</li>
					                <li><span class="label label-success">7</span> Puede cancerlar la creación del mensaje.</li>
					                <li><span class="label label-success">8</span> Cuando todo este listo hacer click en el botón "Crear Ahora"</li>
					                
					            </ul>
					            	<div style="font-style:italic">
					                	Recuerde que debe considerar las siguientes medidas: 
					                	<a href="{{url('/docs/Aplicaci%C3%B3n%20para%20Clientes%20-%20Medidas%20Imagenes.pdf')}}">
			                				Especificaciones de las imagenes
			                			</a>.
					                </div>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/mensajes2.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
                        	<div class="rowint col-md-12">
                        	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Elegir una sección.</li>
					                <li><span class="label label-success">2</span> Elegir una provincia.</li>
					                <li><span class="label label-success">3</span> Establecer una fecha de inicio.</li>
					                <li><span class="label label-success">4</span> Establecer una fecha de fin.</li>
					                <li><span class="label label-success">5</span> Ingresar una descripción correspondiente al mensaje por crear.</li>
					                <li><span class="label label-success">6</span> Seleccionar una imagen desde su ordenador(debe tener las dimensiones
					                especificadas).</li>
					                <li><span class="label label-success">7</span> Puede cancerlar la edición del mensaje.</li>
					                <li><span class="label label-success">8</span> Cuando todo este listo hacer click en el botón "Actualizar"</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/mensajes3.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
			            </div>
			            <div id="cupones" class="cont">
			            	<div class="col-md-12">
					            <legend>Gestión de Cupones</legend>
					        </div>
			            	<div class="rowint col-md-12">
			            	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Listado de todos los cupones.</li>
					                <li><span class="label label-success">2</span> Servirá para ver la información completa de cada registro</li>					                
					                <li><span class="label label-success">3</span> El ícono del "lápiz" sirve para editar el registro seleccionado.</li>
					                <li><span class="label label-success">4</span> Al hacer click cambia el estado del cupón a "En ejecución"</li>		
					                <li><span class="label label-success">5</span> Al hacer click cambia el estado del cupón a "Parado"</li>					                
					                <li><span class="label label-success">6</span> Para agregar un nuevo registro, hacer click en el botón "Crear Nuevo".</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/cupones.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
                        	<div class="rowint col-md-12">
                        	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Cambia la vista al paso 1.</li>
					                <li><span class="label label-success">2</span> Cambia la vista al paso 2.</li>
					                <li><span class="label label-success">3</span> Cambia la vista al paso 3.</li>
					                <li><span class="label label-success">4</span> Seleccione una alianza.</li>
					                <li><span class="label label-success">5</span> Cambia la vista al paso 2.</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/cupones1.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
                        	<div class="rowint col-md-12">
                        	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Seleccionar una imagen desde su ordenador(debe tener las dimensiones
					                especificadas).</li>
					                <li><span class="label label-success">2</span> Cambia la vista al paso 3.</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/cupones2.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
                        	<div class="rowint col-md-12">
                        	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Ingrese el título del cupón.</li>
					                <li><span class="label label-success">2</span> Ingrese la descripción del cupón.</li>
					                <li><span class="label label-success">3</span> Establecer una fecha de inicio.</li>
					                <li><span class="label label-success">4</span> Establecer una fecha de fin.</li>
					                <li><span class="label label-success">5</span> Ingrese una cantidad de cupones por cliente.</li>
					                <li><span class="label label-success">6</span> Ingrese el total de cupones.</li>
					                <li><span class="label label-success">7</span> Seleccione una categoría.</li>
					                <li><span class="label label-success">8</span> Seleccione una o varias provincias.</li>
					                <li><span class="label label-success">9</span> Para agregar un nuevo registro, hacer click en el botón "Crear Cupón".</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/cupones3.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
			            </div>
						<div id="alianzas" class="cont">
							<div class="col-md-12">
					            <legend>Gestión de Alianzas</legend>
					        </div>
			            	<div class="rowint col-md-12">
			            	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Listado de todas las alianzas.</li>
					                <li><span class="label label-success">2</span> Servirá para ver la información completa de cada registro</li>
					                <li><span class="label label-success">3</span> El ícono del "lápiz" sirve para editar el registro seleccionado.</li>
					                <li><span class="label label-success">4</span> El ícono del "tacho" sirve para eliminar el registro seleccionado.</li>
					                <li><span class="label label-success">5</span> Para agregar un nuevo registro, hacer click en el botón "Crear Nuevo".</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/alianza1.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
                        	<div class="rowint col-md-12">
                        	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Ingrese el nombre de la alianza.</li>
					                <li><span class="label label-success">2</span> Ingrese la dirección</li>
					                <li><span class="label label-success">3</span> Para agregar un marcador, de click sobre un punto en el mapa.</li>
					                <li><span class="label label-success">4</span> Ingrese otros detalles.</li>
					                <li><span class="label label-success">5</span> Email para la alianza.</li>
					                <li><span class="label label-success">6</span> Clave para la alianza.</li>
					                <li><span class="label label-success">7</span> Puede cancerlar la creación de la alianza.</li>
					                <li><span class="label label-success">8</span> Para agregar un nuevo registro, hacer click en el botón "Crear ahora".</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/alianza2.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
                        	<div class="rowint col-md-12">
                        	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Ingrese el nombre de la alianza.</li>
					                <li><span class="label label-success">2</span> Ingrese la dirección</li>
					                <li><span class="label label-success">3</span> Para agregar un marcador, de click sobre un punto en el mapa.</li>
					                <li><span class="label label-success">4</span> Ingrese otros detalles.</li>
					                <li><span class="label label-success">5</span> Email para la alianza.</li>
					                <li><span class="label label-success">6</span> Clave para la alianza.</li>
					                <li><span class="label label-success">7</span> Puede cancerlar la edición de la alianza.</li>
					                <li><span class="label label-success">7</span> Para guardar el registro, hacer click en el botón "Actualizar".</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/alianza3.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
			            </div>
			            <div id="consejos" class="cont">
			            	<div class="col-md-12">
					            <legend>Consejos</legend>
					        </div>
			            	<div class="rowint col-md-12">
			            	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Listado de todas los consejos.</li>
					                <li><span class="label label-success">2</span> Al hacer click descargara el archivo stats.</li>
					                <li><span class="label label-success">3</span> Servirá para ver la información completa de cada registro.</li>
					                <li><span class="label label-success">4</span> El ícono del "lápiz" sirve para editar el registro seleccionado.</li>
					                <li><span class="label label-success">5</span> El ícono del "tacho" sirve para eliminar el registro seleccionado.</li>
					                <li><span class="label label-success">6</span> Al hacer click descargara un archivo XLS con todos los registros.</li>
					                <li><span class="label label-success">7</span> Para agregar un nuevo registro, hacer click en el botón "Crear Nuevo".</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/consejos1.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
                        	<div class="rowint col-md-12">
                        	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Ingrese el título del consejo.</li>
					                <li><span class="label label-success">2</span> Seleccionar una imagen desde su ordenador(debe tener las dimensiones
					                especificadas).</li>
					                <li><span class="label label-success">3</span> Puede cancerlar la creación del mensaje.</li>
					                <li><span class="label label-success">6</span> Para agregar un nuevo registro, hacer click en el botón "Agregar".</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/consejos2.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
                        	<div class="rowint col-md-12">
                        	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Ingrese el título del consejo.</li>
					                <li><span class="label label-success">2</span> Seleccionar una imagen desde su ordenador(debe tener las dimensiones
					                especificadas).</li>
					                <li><span class="label label-success">3</span> Puede cancerlar la edición del mensaje.</li>
					                <li><span class="label label-success">6</span> Para guardar el registro, hacer click en el botón "Actualizar".</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/consejos3.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
			            </div>
			            <div id="eventos" class="cont">
			            	<div class="col-md-12">
					            <legend>Eventos y Sorteos</legend>
					        </div>
			            	<div class="rowint col-md-12">
			            	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Listado de todas los eventos y sorteos.</li>
					                <li><span class="label label-success">2</span> Servirá para exportar los asistentes.</li>
					                <li><span class="label label-success">3</span> Servirá para ver la información completa de cada registro.</li>
					                <li><span class="label label-success">4</span> El ícono del "lápiz" sirve para editar el registro seleccionado.</li>
					                <li><span class="label label-success">5</span> El ícono del "tacho" sirve para eliminar el registro seleccionado.</li>
					                <li><span class="label label-success">7</span> Para agregar un nuevo registro, hacer click en el botón "Crear Nuevo".</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/eventos1.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
                        	<div class="rowint col-md-12">
                        	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Seleccionar el tipo.</li>
					                <li><span class="label label-success">2</span> Ingresar el título.</li>
					                <li><span class="label label-success">1</span> Seleccionar una imagen desde su ordenador(debe tener las dimensiones
					                especificadas).</li>
					                <li><span class="label label-success">4</span> Ingresar otros detalles.</li>
					                <li><span class="label label-success">5</span> Ingresar restricciones/condiciones.</li>
					                <li><span class="label label-success">5</span> Ingresar la fecha de caducidad.</li>
					                <li><span class="label label-success">7</span> Puede cancerlar la creación del mensaje.</li>
					                <li><span class="label label-success">6</span> Para guardar el registro, hacer click en el botón "Crear Ahora".</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/eventos2.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
                        	<div class="rowint col-md-12">
                        	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Seleccionar el tipo.</li>
					                <li><span class="label label-success">2</span> Ingresar el título.</li>
					                <li><span class="label label-success">1</span> Seleccionar una imagen desde su ordenador(debe tener las dimensiones
					                especificadas).</li>
					                <li><span class="label label-success">4</span> Ingresar otros detalles.</li>
					                <li><span class="label label-success">5</span> Ingresar restricciones/condiciones.</li>
					                <li><span class="label label-success">5</span> Ingresar la fecha de caducidad.</li>
					                <li><span class="label label-success">7</span> Puede cancerlar la creación del mensaje.</li>
					                <li><span class="label label-success">6</span> Para guardar el registro, hacer click en el botón "Actualizar".</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/eventos3.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
			            </div>
			            <div id="sugerencias" class="cont">
			            	<div class="col-md-12">
					            <legend>Sugerencias</legend>
					        </div>
			            	<div class="rowint col-md-12">
			            	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Listado de todas las sugerencias.</li>
					                <li><span class="label label-success">6</span> Para exportar los resultados mostrados, hacer click en el botón "Exportar"</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/sugerencias1.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
			            </div>
			            <div id="reportes" class="cont" style="display:none;">
			            	<div class="col-md-12">
					            <legend>Reportes</legend>
					        </div>
			            	<div class="rowint col-md-12">
			            	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Listado de todos los usuarios.</li>
					                <li><span class="label label-success">3</span> El ícono del "lápiz" sirve para editar el registro seleccionado.</li>
					                <li><span class="label label-success">4</span> El ícono del "tacho" sirve para eliminar el registro seleccionado.</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/sugerencias1.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
			            </div>
			            <div id="usuarios" class="cont">
			            	<div class="col-md-12">
					            <legend>Usuarios</legend>
					        </div>
			            	<div class="rowint col-md-12">
			            	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Listado de todos los usuarios.</li>
					                <li><span class="label label-success">2</span> El ícono del "lápiz" sirve para editar el registro seleccionado.</li>
					                <li><span class="label label-success">3</span> El ícono del "tacho" sirve para eliminar el registro seleccionado.</li>
					                <li><span class="label label-success">4</span> Para agregar un nuevo registro, hacer click en el botón "Crear Nuevo".</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/usuarios1.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
                        	<div class="rowint col-md-12">
                        	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Ingrese el nombre.</li>
					                <li><span class="label label-success">2</span> Ingrese el email.</li>
					                <li><span class="label label-success">3</span> Ingrese la contraseña.</li>
					                <li><span class="label label-success">6</span> Para guardar el registro, hacer click en el botón "Crear Ahora".</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/usuarios2.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
                        	<div class="rowint col-md-12">
                        	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Ingrese el nombre.</li>
					                <li><span class="label label-success">2</span> Ingrese el email.</li>
					                <li><span class="label label-success">3</span> Ingrese la contraseña.</li>
					                <li><span class="label label-success">6</span> Para guardar el registro, hacer click en el botón "Guardar".</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/usuarios3.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
			            </div>
			            <div id="config" class="cont">
			            	<div class="col-md-12">
					            <legend>Configuración</legend>
					        </div>
			            	<div class="rowint col-md-12">
			            	<div class="col-md-4">
                            	<ul class="nolist">
					                <li><span class="label label-success">1</span> Ingrese el correo principal de emisión</li>
					                <li><span class="label label-success">2</span> Ingrese el correo destinario del buzón de sugerencias</li>
					                <li><span class="label label-success">3</span> Ingrese el correo destinario del buzón de solicitudes de presupuesto</li>
					                <li><span class="label label-success">4</span> Para guardar el registro, hacer click en el botón "Guardar".</li>
					            </ul>
                        	</div>
			            	<div class="col-md-8">
                            	<img src="{{ asset('app/img/help/configuracion.jpg') }}" alt="imagen" class="img-responsive pull-right">
                        	</div>
                        	</div>
			            </div>

					</div>
				</div>
			</div>
        </div>
    </section>
@stop
