@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/events_controller.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload-shim.js"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload.js"></script>
@yield('js')
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/datetimepiker/datepicker.min.css') }}">
<style>
	form .progress {
    line-height: 15px;
}
}

.progress {
    display: inline-block;
    width: 100px;
    border: 3px groove #CCC;
}

.progress div {
    font-size: smaller;
    background: orange;
    width: 0;
}
</style>
@section('content')
    <section class="content-header">
      <h1>
        Eventos y Sorteos
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li><a href="/admin/events/index.html">Eventos y Sorteos</a></li>
        <li class="active"><a>Crear Eventos o Sorteos</a></li>
      </ol>
    </section>
    <section class="content" ng-app="app">
        <div class="row" ng-controller="MyCtrl">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border"><h4>Crear Eventos o Sorteos</h4>
                    </div>                     
                    <div class="box-body">
                        <div>
                            <div class="col-sm-12">
								<form role="form" name="myForm" autocomplete="off">
									<div class="form-group">
									    <label class="radio-inline"><input type="radio" name="optradio" ng-click="changet('raffle')" required>Sorteo</label>
										<label class="radio-inline"><input type="radio" name="optradio" ng-click="changet('event')" required>Evento</label>
										<i ng-show="myForm.optradio.$error.required" style="color:red">* Requerido</i>
									</div>
									<div class="form-group">
									    <label class="control-label" for="exampleInputEmail1">Titulo</label>
									    <input type="text" name="title" class="form-control" ng-model="evento.title" required>
									    <i ng-show="myForm.title.$error.required" style="color:red">* Requerido</i>
									</div>
									<div class="row">
										<div class="col-md-6 ">
											<div class="form-group">
									 	<label class="control-label">Imagen</label>
								      <input type="file" ngf-select ng-model="picFile" name="file"    
								             accept="image/*" ngf-max-size="2MB" required
								             ngf-model-invalid="errorFile">
								      <p class="help-block">La imagen debe tener las siguientes dimensiones: 600px por 500px</p>
								      <i ng-show="myForm.file.$error.required" style="color:red">* Requerido</i><br>
								      <i ng-show="myForm.file.$error.maxSize"  style="color:red">Archivo demasiado grande 
								          (@{{errorFile.size / 1000000|number:1}}MB): máximo  2MB</i>
								      <div class="row">    
			                            <div class="col-sm-10">
			                            	<img ng-show="myForm.file.$valid" ngf-thumbnail="picFile" 
								      			class="img-responsive">
								      	</div>
			                            <div class="col-sm-2">
			                            	<button ng-click="picFile = null" ng-show="picFile" 
										      class="btn btn-danger"><i class="fa fa-remove"></i> Quitar</button>
										      </div>
			                            </div>	
			                           </div>					       
								      
								      <div class="form-group">
								      	  <span class="progress" ng-show="picFile.progress >= 0">
								            <div style="width:@{{picFile.progress}}%" 
								            ng-bind="picFile.progress + '%'"></div>
									      </span>
									      <span ng-show="picFile.result">	</span>
									      <span class="err" ng-show="errorMsg">@{{errorMsg}}</span>
								      </div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
											    <label class="control-label" for="exampleInputEmail1">Otros Detalles</label>
											    <textarea  class="form-control" rows="5" style="resize:none" ng-model="evento.content"></textarea><br>
											    <label class="control-label" for="exampleInputEmail1">Restricciones/Condiciones</label>
											    <textarea  class="form-control" rows="5" style="resize:none" ng-model="evento.constraints"></textarea><br>
											    <label class="control-label" for="exampleInputEmail1">Fecha de Caducidad</label>
					                            <input type="text" name="fecha" class="form-control" id="date" 
					                            required data-date-format="yyyy-mm-dd">
					                            <i ng-show="myForm.fecha.$error.required" style="color:red">* Requerido</i>
											</div>
										</div>
									</div>                                              
                            </div>
                        </div>
                    </div>
					<div class="box-footer">
						<a href="/admin/events/index.html" 
						class="btn btn-danger pull-left">Cancelar</a>
						<button class="btn btn-success pull-right" ng-click="addEvent(picFile)" ng-disabled="!myForm.$valid" >Crear ahora</button>
					</div>							
					</form>    
                </div>
            </div>
        </div>
    </section>
@stop