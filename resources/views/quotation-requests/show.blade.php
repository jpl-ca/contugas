@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/quotation_controller.js') }}"></script>
<script src="https://maps.google.com/maps/api/js?libraries=placeses,visualization,drawing,geometry,places"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/ngmap/build/scripts/ng-map.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
@yield('js')
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
@section('content')
<div ng-app="app">
    <section class="content-header">
      <h1>
        Solicitudes de Presupuesto
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li><a href="/admin/quotation-requests/index.html">Solicitudes de Presupuesto</a></li>
        <li class="active" ng-controller="showCtrl">Solicitud de Presupuesto #@{{detquotation.id}}</li>
      </ol>
    </section>
    <section class="content">
        <div class="row" ng-controller="showCtrl">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border"><h4>Solicitud de Presupuesto #@{{detquotation.id}}</h4>
                    </div>                     
                    <div class="panel-body">
                        <div>
                            <div class="col-md-6">
                            	<strong>Fecha</strong>
								<p>@{{detquotation.created_at}}</p>   
								<strong>Nombres</strong>
								<p>@{{detquotation.name}}</p>    
								<strong>Apellidos</strong>
								<p>@{{detquotation.last_name}}</p>    
								<strong>Dirección</strong>
								<p>@{{detquotation.address}}</p>
								<strong>Provincia</strong>
								<p>@{{detquotation.province.name}}</p>
								<strong>Distrito</strong>
								<p>@{{detquotation.district.name}}</p>
								<strong>Teléfono</strong>
								<p>@{{detquotation.phone}}</p>
								<strong>Email</strong>
								<p>@{{detquotation.email}}</p>
								<strong>Comentario</strong>
								<p>@{{detquotation.comment}}</p>								                                        
                            </div>
                            <div class="col-md-6">
                            	<br>
                            	<div ng-controller="mapCtrl as vm">
									    <ng-map zoom="13" center="[@{{detquotation.lat}}, @{{detquotation.lng}}]" style="height:300px"
								        on-center-changed="centerCustomMarker()">
								       	<marker  position="[@{{detquotation.lat}}, @{{detquotation.lng}}]" ></marker>									        
								    </ng-map>
								</div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop