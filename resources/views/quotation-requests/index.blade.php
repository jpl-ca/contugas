@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/quotation_controller.js') }}"></script>
<script src="https://maps.google.com/maps/api/js?libraries=placeses,visualization,drawing,geometry,places"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/ngmap/build/scripts/ng-map.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
@yield('js')
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/datetimepiker/datepicker.min.css') }}">
<style>

.datepicker.-from-bottom- {
	z-index: 10000;
}	
.contme{
		border-radius: 2px 2px 2px 2px;
		-moz-border-radius: 2px 2px 2px 2px;
		-webkit-border-radius: 2px 2px 2px 2px;
		border: 1px solid silver;
		padding: 10px;
	}
</style>
@section('content')
	<section class="content-header">
      <h1>
        Solicitudes de Presupuesto
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li class="active"><a>Solicitudes de Presupuesto</a></li>
      </ol>
    </section>
    <section class="content" ng-app="app">
        <div class="row"  ng-controller="quotationsController">
            <div class="col-md-12">
            	<div class="box box-primary">
            		<div class="box-header with-border">
		              <div class="col-md-12 form-inline " style="margin-bottom:25px;margin-top:25px;">
								<div class="form-group">
									Desde: <input type="text" class="form-control datex" 
						                            required data-date-format="yyyy-mm-dd"
						                            ng-model="first_date" id="start">
								</div>
								<div class="form-group">
									Hasta: <input type="text" class="form-control datex" 
						                            required data-date-format="yyyy-mm-dd"
						                            ng-model="end_date" id="end">
								</div>
								<div class="form-group">
									Provincias: 
									 <select class="form-control"
				                      ng-options="province.name for province in provinces" 
				                      ng-model="selected_province">
				                        </select>
								</div>
								<div class="form-group">
									<button ng-click="search()" class="btn btn-info">Buscar</button>
								</div>
							</div>
		            </div>
		            <div class="box-header with-border">
						<div class="nav-tabs-custom">
	                    	<ul class="nav nav-tabs">
							  <li class="active"><a data-toggle="tab" href="#listado">Listado</a></li>
							  <li id="vmapa"><a data-toggle="tab" href="#mapa">Mapa</a></li>
							  <a ng-click="downloadFileUrlOfCurrentSelection()"
                        class="btn btn-primary pull-right">
                        <i class="fa fa-download" style="margin-right:5px"></i>Exportar</a>
							</ul>
							<div class="tab-content" style="overflow: hidden;">
							  <div id="listado" class="tab-pane fade in active" style="z-index: 1000;position: relative;">
								    <div>
			                            <div class="col-sm-12 table-responsive">
			                                <table class="table">
			                                <br>
			                                    <thead>
			                                        <tr>
			                                            <th style="width: 20%;">Nombre</th>
			                                            <th style="width: 20%;">Apellido</th>
			                                            <th style="width: 30%;">Provincia</th>
			                                            <th style="width: 30%;">Distrito</th>
			                                            <th>Acciones</th>
			                                        </tr>
			                                    </thead>
			                                    <tbody>
			                                         
			                                        <tr class="" ng-repeat="quotation in quotations">
			                                            <td>@{{quotation.name}}</td>
			                                            <td>@{{quotation.last_name}}</td>
			                                            <td>@{{quotation.province.name}}</td>
			                                            <td>@{{quotation.district.name}}</td>
			                                            <td>
			                                                <a href="/admin/quotation-requests/@{{quotation.id}}.html" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="ver">
			                                                    <span class="fa fa-eye" aria-hidden="true"></span>
			                                                </a>
			                                                <a ng-click="delete_quotation(quotation.id, $index)" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="eliminar">
			                                                    <span class="fa fa-trash" aria-hidden="true"></span>
			                                                </a>
			                                            </td>
			                                        </tr>
			                                    </tbody>
			                                </table>
			                                <ul class="pagination pagination-sm">
			                                    <li ng-class="{active:0}"><a href="#" ng-click="page_anterior()">Anterior</a>

			                                    </li>
			                                    <li> <a >@{{current_page}}</a>
			                                       
			                                    </li>
			                                    <li><a href="#" ng-click="page_siguiente()">Siguiente</a>

			                                    </li>
			                                </ul>                               
			                            </div>
		                        </div>
							  </div>
							  <div id="mapa" class="tab-pane fade active">
							    <divs>
								    <ng-map zoom="11" center="Lima" style="height:350px">
								        	<marker
								        		ng-repeat="quotation in quotations"
								        		position="[@{{quotation.lat}}, @{{quotation.lng}}]"
										        on-click="showInfoWindow(event, [@{{quotation}}])">
										      </marker>						        
											
								    </ng-map>
								</div> 
							  </div>	
							</div>                        
	                    </div>
		            </div>
            </div>
        </div>
    </section>
@stop