@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/tips_controller.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload-shim.js"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload.js"></script>
@yield('js')
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
@section('content')
<div ng-app="app">
    <section class="content-header">
      <h1>
        Consejos
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li><a href="/admin/tips/index.html">Consejos</a></li>
        <li class="active" ng-controller="showController"><a>Consejo: @{{detatip.title}}</a></li>
      </ol>
    </section>
    <section class="content">
        <div class="row" ng-controller="showController">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border"><h4>Consejo:  @{{detatip.title}}</h4>
                    </div>                     
                    <div class="panel-body">
                        <div>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-8">
                            	<img ng-src="@{{detatip.image_url}}" alt="imagen" class="img-responsive">
                            </div>
                            <div class="col-sm-2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop