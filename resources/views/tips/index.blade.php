@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/tips_controller.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload-shim.js"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload.js"></script>
@yield('js')
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
@section('content')
    <section class="content-header">
      <h1>
        Consejos
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li class="active"><a>Consejos</a></li>
      </ol>
    </section>
    <section class="content" ng-app="app">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <a href="/admin/tips/create.html" 
                        class="btn btn-success pull-right">
                        <i class="fa fa-plus" style="margin-right:5px"></i>Crear Nuevo</a>
                        <a href="{{url('/admin/tips/index.xls')}}" title="Descargar archivo XLS" class='pull-right btn btn-primary' style='margin-right:7px;'><i class='fa fa-download'></i> Exportar</a>
                    </div>                     
                    <div class="panel-body">
                        <div>
                            <div class="col-sm-12" ng-controller="tipsController">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width: 80%;">Título</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         
                                        <tr class="" ng-repeat="tip in tips">
                                            <td>@{{tip.title}}</td> 
                                            <td>
                                                <a href="/admin/tips/@{{tip.id}}.html" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="ver">
                                                    <span class="fa fa-eye" aria-hidden="true"></span>
                                                </a>
                                                <a href="/admin/tips/@{{tip.id}}/edit.html" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="editar">
                                                    <span class="fa fa-pencil" aria-hidden="true"></span>
                                                </a>
                                                <a ng-click="delete_tip(tip.id, $index)" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="eliminar">
                                                    <span class="fa fa-trash" aria-hidden="true"></span>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <nav>
                                    <ul class="pagination pagination-sm">
                                        <li ng-class="{active:0}"><a href="#" ng-click="page_anterior()">Anterior</a>

                                        </li>
                                        <li> <a >@{{current_page}}</a>
                                           
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop