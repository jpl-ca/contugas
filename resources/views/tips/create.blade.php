@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/tips_controller.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload-shim.js"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload.js"></script>
@yield('js')
<style>
	form .progress {
    line-height: 15px;
}
}

.progress {
    display: inline-block;
    width: 100px;
    border: 3px groove #CCC;
}

.progress div {
    font-size: smaller;
    background: orange;
    width: 0;
}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
@section('content')
    <section class="content-header">
      <h1>
        Consejos
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li><a href="/admin/tips/index.html">Consejos</a></li>
        <li class="active"><a>Crear Consejo</a></li>
      </ol>
    </section>
    <section class="content" ng-app="app">
        <div class="row" ng-controller="MyCtrl">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border"><h4>Crear Consejo</h4>
                    </div>                     
                    <div class="box-body">
                    <br><br>
                        <div>
                            <div class="col-sm-12"> 
								<form name="myForm" autocomplete="off">
								     <div class="form-group">
									    <label class="control-label">Titulo</label>
									    <input type="text" class="form-control" name="title" ng-model="title"required>
								        <i ng-show="myForm.title.$error.required" style="color:red">* Requerido</i>
									 </div>
									 <div class="form-group">
									 	<label class="control-label">Imagen</label>
								      <input type="file" ngf-select ng-model="picFile" name="file"    
								             accept="image/*" ngf-max-size="2MB" required
								             ngf-model-invalid="errorFile">
								      <p class="help-block">La imagen debe tener las siguientes dimensiones: 470px por 700px</p>
								      <i ng-show="myForm.file.$error.required" style="color:red">* Requerido</i><br>
								      <i ng-show="myForm.file.$error.maxSize"  style="color:red">Archivo demasiado grande 
								          (@{{errorFile.size / 1000000|number:1}}MB): máximo  2MB</i>
								      <div class="row">    
			                            <div class="col-sm-10">
			                            	<img ng-show="myForm.file.$valid" ngf-thumbnail="picFile" 
								      			class="img-responsive">
								      	</div>
			                            <div class="col-sm-2">
			                            	<button ng-click="picFile = null" ng-show="picFile" 
										      class="btn btn-danger"><i class="fa fa-remove"></i> Quitar</button>
										      </div>
			                            </div>	
			                           </div>					       
								      
								      <div class="form-group">
								      	  <span class="progress" ng-show="picFile.progress >= 0">
								            <div style="width:@{{picFile.progress}}%" 
								            ng-bind="picFile.progress + '%'"></div>
									      </span>
									      <span ng-show="picFile.result">	</span>
									      <span class="err" ng-show="errorMsg">@{{errorMsg}}</span>
								      </div>                                          
                            </div>
                        </div>
                    </div>
									<div class="box-footer">
								      	<a href="/admin/tips/index.html" 
									    class="btn btn-danger pull-left">Cancelar</a>
								      	<button class="btn btn-success pull-right" ng-disabled="!myForm.$valid" 
								              ng-click="uploadPic(picFile)">Agregar
								      	</button>								      
								    </div>
								</form> 
                </div>
            </div>
        </div>
    </section>
@stop