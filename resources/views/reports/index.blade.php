@extends('layouts.admin')
@section('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script src="{{url('app/bower_components/Chart.js/Chart.js')}}"></script>
<script type="text/javascript" src="{{url('app/bower_components/jspdf/dist/jspdf.debug.js')}}"></script>
<script type="text/javascript" src="{{url('app/bower_components/html2canvas/build/html2canvas.min.js')}}"></script>
<script src="{{url('app/bower_components/angular-chart.js/angular-chart.js')}}"></script>
<script src="{{url('app/bower_components/angular-chart.js/angular-chart.js')}}"></script>
<script type="text/javascript" src="{{url('app/bower_components/Chart.HorizontalBar.js/Chart.HorizontalBar.js')}}"></script>
<script type="text/javascript"  src="{{url('jquery/2.1.4/jquery.min.js')}}"></script>
<script type="text/javascript"  src="{{url('app/datetimepiker/datepicker.min.js')}}"></script>
<script type="text/javascript"  src="{{url('app/datetimepiker/datepicker.en.js')}}"></script>
<script src="{{url('app/scripts/controllers/report_controller.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{url('app/datetimepiker/datepicker.min.css')}}">
 	<section class="content-header">
      <h1>
        Reportes
      </h1>
       <ol class="breadcrumb">
        <li><a href="{{url('/admin/reports/index.html')}}"><i class="fa fa-dashboard"></i> Reportes</a></li>
      </ol>
    </section>
        <section class="content" ng-app='app' ng-controller='ReportController'>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                    </div>
                    <div class="panel-body">
                        <div>
                            <div class="col-sm-12">

                                <section ng-repeat="category in listOfReports.categories" ng-class="{'hide':selectedReport.name}">
                                    <h4>@{{category.name}}</h4>
                                    <ul >
                                        <li ng-repeat='report in category.items'>
                                            <a ng-click='setSelectedReport(report)' ng-class="{'hide':(report.type=='link')}" style='cursor:pointer;'>@{{report.name}}</a>
                                            <a ng-href='@{{report.url}}' ng-class="{'hide':!(report.type=='link')}">@{{report.name}}</a>
                                        </li>
                                    </ul>
                                </section>
                            	
						    	<div ng-class="{'hide':!selectedReport.name}">
                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Exportar <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu">
                                        <li><a href="#" ng-click="downloadPdfOfSelectedReport();"> <i class='fa fa-pdf-o'> </i> PDF</a></li>
                                      </ul>
                                    </div>
                                    <!--End Single button-->
	                                <p>
										@{{selectedReport.name}}
									</p>
									<form class="form-inline">
									  <div class="form-group">
									    <label for="exampleInputName2">Inicio</label>
									    <input type="text" class="form-control datex" data-date-format="yyyy-mm-dd" id="startDate" placeholder="AAAA-MM-DD">
									  </div>
									  <div class="form-group">
									    <label for="exampleInputEmail2">Fin</label>
									    <input type="text" class="form-control datex" data-date-format="yyyy-mm-dd" id="endDate" placeholder="AAAA-MM-DD">
									  </div>
									  <button type="submit" class="btn btn-default" ng-click="filterByDate();">Filtrar</button>
									</form>
									<p ng-if="noData==true" class='text-center' style='padding-top:30px;'>Sin información para mostrar</p>
									<canvas id="myChart" height="200" width="500"></canvas>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@stop