@extends('layouts.admin')
@section('content')
    <script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.0.1/Chart.min.js"></script>
    <script src="{{url('app/bower_components/angular-chart.js/angular-chart.js')}}"></script>
    <script src="{{url('app/scripts/controllers/coupon_report_controller.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{url('app/datetimepiker/datepicker.min.css')}}">

    <section class="content-header">
      <h1>
        Cupones
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Reportes</a></li>
        <li class="active"><a>Cupones</a></li>
      </ol>
    </section>
    <section class="content" ng-app="app" ng-controller="CouponReportController">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3>
                          Canjes por cupón
                        </h3>
                        <form class="form-inline" ng-class="{'hide':selectedItem}">
                          <div class="form-group">
                            <label for="exampleInputName2">Inicio</label>
                            <input type="text" class="form-control datex" data-date-format="yyyy-mm-dd" id="start-date" placeholder="AAAA-MM-DD">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail2">Fin</label>
                            <input type="text" class="form-control datex" data-date-format="yyyy-mm-dd" id="end-date" placeholder="AAAA-MM-DD">
                          </div>
                          
                          <div class="form-group">
                            <label class="control-label">Alianza</label>  
                            <select  class="form-control"
                              ng-options="company.name for company in companies" 
                              ng-model="selectedCompany">
                              <option value=""> -- Todos -- </option>
                            </select>
                          </div>

                          <div class="form-group">
                            <label class="control-label">Categoría</label>  
                            <select  class="form-control"
                              ng-options="category.name for category in categories" 
                              ng-model="selectedCouponCategory">
                              <option value=""> -- Todos -- </option>
                            </select>
                          </div>

                          <button type="submit" class="btn btn-default" ng-click="filterItems();">Filtrar</button>
                        </form>
                    </div>                     
                    <div class="panel-body">
                        <div class='search-form' ng-class="{'hide':selectedItem}">
                            <div class="col-sm-12">
                              <div ng-repeat="item in items" class='row' style='margin-bottom:17px;border-bottom:1px solid rgba(0,0,0,0.1); padding-bottom:15px;'>
                                <div class='col-md-2'>
                                  <a ng-href="/admin/coupons/@{{item.coupon_id}}.html">
                                  <img ng-src="/app/models/coupons/@{{item.coupon_id}}" class="img-responsive"/>
                                  </a>
                                </div>
                                <div class='col-md-4'>
                                  <h4 style='margin-top:2px'>@{{item.coupon_title}}</h4>
                                  <ul class="list-unstyled">
                                    <li>
                                      <strong>Periodo de validez: </strong> <i class='fa fa-calendar'></i> @{{item.coupon_start_date | limitTo : 10 }} al @{{item.coupon_end_date | limitTo : 10}}
                                    </li>
                                    <li>
                                      <strong>Alianza:</strong> @{{item.company_name}}
                                    </li>
                                    <li>
                                      <strong>Categoría:</strong>  <i class='fa fa-tag'></i> @{{item.coupon_category_name}}
                                    </li>
                                    <li>
                                      <strong>Canjeados:</strong> @{{item.coupon_total_swaps}} de @{{item.coupon_max_swaps}} cupones
                                    </li>
                                    <li>
                                      <strong>Estado:</strong>
                                        <div class='status-icon @{{item.status}}'></div>
                                    </li>
                                  </ul>
                                </div>
                                <div class='col-md-3'>
                                  <h4 class='text-center'>@{{item.coupon_total_swaps_percent}} Canjeados</h4>

                                  <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100" style="width: @{{item.coupon_total_swaps_percent}};">
                                      
                                    </div>
                                  </div>
                                </div>

                                <div class='col-md-3'>
                                  <div class='text-center'>
                                    <a ng-click='openPopUp(item);' type="button" class="btn btn-primary" title="Ver canjes por día" style='background-color:#F7F9FA; border-color:#ddd'>
                                      <img src="/app/img/connection.png" alt="" style='width: 30px;'>
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
      
                        <div id='coupon-swaps' ng-class="{'hide':!selectedItem}">
                          <ol class="breadcrumb">
                            <li><a ng-click='selectedItem=null;' style='cursor:pointer;'><i class="fa fa-tag"></i> Inicio</a></li>
                            <li class='coupon'><a>Cupón @{{selectedItem.coupon_id}}</a></li>
                          </ol>
                          <div ng-class="{'hide':isLoadingItem}">
                            
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--- Modal -->
        <div class="modal fade" id="chartModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Canjes de cupón @{{selectedItem.coupon_id}}</h4>
                    </div>
                    <div class="modal-body">
                        <canvas id="myChart"></canvas>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" ng-click>Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <!--End modal-->
    </section>
<style>
  .status-icon{
      background: #ffffff;
    width: 16px;
    height: 16px;
    border-radius: 50%;
    display: inline-block;
  }
  .status-icon.RUNNING{
      background: #A8D26E;
  }
  .status-icon.RUNNING:after{
      content: ' Ejecución';
      margin-left: 20px;
  }

  .status-icon.STOPPED{
      background: #E8C34C;
  }
  .status-icon.STOPPED:after{
      content: ' Pausado';
      margin-left: 20px;
  }

  .status-icon.FINISHED{
      background: #b3b3b3;
  }

  .status-icon.WAITING{
      background: #C5D9E3;
  }

  .status-icon.FINISHED:after{
      content: ' Finalizado';
      margin-left: 20px;
  }
  
  .status-icon.WAITING:after{
      content: 'Esperando';
      margin-left: 20px;
  }
  

</style>
@stop