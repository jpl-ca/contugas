@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/coupons_controller.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload-shim.js"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload.js"></script>
@yield('js')
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/datetimepiker/datepicker.min.css') }}">
<style>
	form .progress {
    line-height: 15px;
}
}

.progress {
    display: inline-block;
    width: 100px;
    border: 3px groove #CCC;
}

.progress div {
    font-size: smaller;
    background: orange;
    width: 0;
}
.itemm{
	text-align: center;
	line-height: 2;
}
.contitem{
	border: 1px solid #ddd;
	font-size: 25px;
    text-align: center;
    height: 70px;
    border-radius: 34px;
    width: 70px;
    line-height: 2.8;
    cursor: pointer;
    margin:0 auto;
}
</style>
@section('content')
	<section class="content-header">
      <h1>
        Gestión de Cupones
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li><a href="/admin/coupons/index.html">Gestión de Cupones</a></li>
        <li class="active"><a>Crear de Cupón</a></li>
      </ol>
    </section>
    <section class="content" ng-app="app">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border"><h4>Crear de Cupón</h4>
                    </div>                     
                    <div class="panel-body" ng-controller="addController">
                    <br><br>
	                    <div class="row">	                    
	                    	<div class="col-md-12" style="height:50px;">
	                            <div class="itemm col-md-4 col-xs-4" >
	                            	<div class="contitem" id="ai1" data-cont="i1" style="background:#f5f5f5">
	                            		1
	                            	</div>
	                            	Alianza
	                            </div>
	                            <div class="itemm col-md-4 col-xs-4" >
	                            	<div class="contitem" id="ai2" data-cont="i2">
	                            		2
	                            	</div>
									Diseño
	                            </div>
	                            <div class="itemm col-md-4 col-xs-4" >
	                            	<div class="contitem" id="ai3" data-cont="i3">
	                            		3
	                            	</div>
									Datos y Condiciones
	                            </div>
	                        </div>                                                
	                    </div>  
	                    <br><br>
	                    <div class="col-md-12">
	                    	<form name="myForm" autocomplete="off">
	                        	<div class="hidee" style="display:block;" id="i1">
	                        		<h3 align="center">Escoja Alianza</h3>
									<div class="form-group">
										<select  class="form-control"
					                      ng-options="companie.name for companie in companies" 
					                      ng-model="select_companie">
					                    </select>
									</div>
									<div class="col-md-4"></div>
									<div class="col-md-6">
										<p>¿El Comercio no está en la lista?
										 <a href="/admin/companies/index.html">
										 registrar nuevo comercio
										 </a>
										</p>
									</div>
									<div class="col-md-2"></div>
									<div class="col-md-12">
										<br>
										<div class="form-group">
											<a class="btn btn-info btnpaso pull-right" style="margin-top: -10px;" 
												data-cont="i2">
	                        					Ir al Paso 2 - Diseño
	                        				</a>
										</div>
									</div>																		
	                        	</div>
	                        	<div class="hidee" style="display:block;" id="i2">
	                        		<h3 align="center">Diseño</h3>
									<div class="col-md-12 ">
											<div class="form-group">
											 	<label class="control-label">Imagen</label>
										      	<input type="file" ngf-select ng-model="picFile" name="file"    
										             accept="image/*" ngf-max-size="2MB" 
										             ngf-model-invalid="errorFile">
										             <p class="help-block">La imagen debe tener las siguientes dimensiones: 600px por 500px</p><br>
										      	<i ng-show="myForm.file.$error.maxSize"  style="color:red">Archivo demasiado grande 
										          (@{{errorFile.size / 1000000|number:1}}MB): máximo  2MB</i>
										      	<div class="row">    
						                            <div class="col-sm-10">
						                            	<img ng-show="myForm.file.$valid" ngf-thumbnail="picFile" 
											      			class="img-responsive">
											      	</div>
						                            <div class="col-sm-2">
						                            	<button ng-click="picFile = null" ng-show="picFile" 
													      class="btn btn-danger"><i class="fa fa-remove"></i> Quitar</button>
													      </div>
						                            </div>	
					                         </div>					       
										      
										    <div class="form-group">
										      	  <span class="progress" ng-show="picFile.progress >= 0">
										            <div style="width:@{{picFile.progress}}%" 
										            ng-bind="picFile.progress + '%'"></div>
											      </span>
											      <span ng-show="picFile.result">	</span>
											      <span class="err" ng-show="errorMsg">@{{errorMsg}}</span>
										    </div>
										</div>	
	                        		<div class="col-md-12">
										<br>
										<div class="form-group">
											<a class="btn btn-info btnpaso pull-right" style="margin-top: -10px;" 
												data-cont="i3">
	                        					Ir al Paso 3 - Datos y Condiciones
	                        				</a>
										</div>
									</div>
	                        	</div>
	                        	<div class="hidee" style="display:block;" id="i3">
	                        		<div class="form-group">
	                        			<label class="control-label">Título</label>
	                        			<input type="text" class="form-control" ng-model="coupon.title">
	                        		</div>
	                        		<div class="form-group">
	                        			<textarea rows="5" ng-model="coupon.description" 
	                        			style="resize:none;" class="form-control" placeholder="Descripción"></textarea>
	                        		</div>
	                        		 
	                        		<div class="col-md-6 col-xs-6">	
	                        			<label class="control-label">Periodo de Canje</label>                        			
	                        			<div class="form-inline">	                        				
	                        				<div class="form-group">		                        				
		                        				<input type="text" class="form-control datex"
		                        				required data-date-format="yyyy-mm-dd" id="start">
		                        				<p class="help-block">Inicia</p>
		                        			</div>
		                        			<div class="form-group">
		                        				<input type="text" class="form-control datex"
		                        				required data-date-format="yyyy-mm-dd" id="end">
		                        				<p class="help-block">Termina</p>
		                        			</div>
	                        			</div>
	                        			<label class="control-label">Condiciones</label>
	                        			<div class="form-inline">
	                        				<div class="form-conrtrol">
	                        					<input min="1" class="form-control"
	                        					ng-model="coupon.user_swaps">
	                        					Cupones por cliente
	                        				</div>
	                        			</div>
	                        			<div class="form-inline">
	                        				<div class="form-conrtrol">
	                        					<input min="1" class="form-control"
	                        					ng-model="coupon.max_swaps">
	                        					Total de cupones
	                        				</div>
	                        			</div>
	                        		</div>
	                        		<div class="col-md-6 col-xs-6">
	                        			<div class="form-group">
	                        			<label class="control-label">Categoría</label>  
		                        			<select  class="form-control"
						                      ng-options="categorie.name for categorie in categories" 
						                      ng-model="select_categorie">
						                    </select>
		                        		</div>
		                        		<div class="form-group">
		                        			<label class="control-label">Provincias</label>  
		                        			<div ng-repeat="province in provinces">
												<div class="action-checkbox">
													<input id="@{{province.ubigeo_code}}" type="checkbox" value="@{{province.ubigeo_code}}" 
														ng-checked="province.selected" 
														ng-click="toggleSelection(province.ubigeo_code)" />
													<label for="@{{province.name}}"></label>
													@{{province.name}}
												</div>
											</div>
		                        		</div>
	                        		</div>

	                        		<div class="col-md-12">
										<br>
										<div class="form-group">
											<a class="btn btn-success pull-right" style="margin-top: -10px;" 
												 ng-click="addcoupon(picFile)">
	                        					Crear Cupón
	                        				</a>
										</div>
									</div>
	                        	</div>
	                        	<!-- <div class="hidee" style="display:block;" id="i4">
	                        		
	                        	</div> -->
	                        </form>
	                    </div>                      
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
