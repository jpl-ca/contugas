@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/coupons_controller.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload-shim.js"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload.js"></script>
@yield('js')
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
@section('content')
    <section class="content-header">
      <h1>
        Gestión de Cupones
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li class="active"><a>Gestión de Cupones</a></li>
      </ol>
    </section>
    <section class="content" ng-app="app">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <a href="/admin/coupons/create.html" 
                        class="btn btn-success pull-right">
                        <i class="fa fa-plus" style="margin-right:5px"></i>Crear Nuevo</a>
                    </div>                     
                    <div class="panel-body">
                        <div>
                            <div class="col-sm-12" ng-controller="couponController">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width: 40%;">Título</th>
                                            <th>Alianza</th>
                                            <th>Inicia</th>
                                            <th>Vence</th>
                                            <th>Estado</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         
                                        <tr class="" ng-repeat="coupon in coupons">
                                            <td>@{{coupon.title}}</td>
                                            <td>@{{coupon.company.name}}</td>
                                            <td>@{{coupon.start_date | limitTo : 10 }}</td>
                                            <td>@{{coupon.end_date | limitTo : 10 }}</td>
                                            <td>@{{coupon.status_label}}</td>
                                            <td>
                                            	<a  href="/admin/coupons/@{{coupon.id}}/edit.html"
                                            	ng-if="coupon.status == 'WAITING' || coupon.status == 'PENDING' || coupon.status == 'STOPPED'" 
                                            	class="accion"   data-toggle="tooltip" data-placement="top" title="editar"
                                            	style="text-decoration: none;cursor:pointer;">
                                             		<i class="fa fa-pencil"></i>
                                             	</a>
                                             	<a  ng-click="delete_coupon(coupon.id, $index)" 
                                            	ng-if="coupon.status == 'WAITING' || coupon.status == 'PENDING'"
                                            	class="accion"   data-toggle="tooltip" data-placement="top" title="eliminar"
                                            	style="text-decoration: none;cursor:pointer;">
                                             		<i class="fa fa-trash"></i>
                                             	</a>
                                             	<a  href="/admin/coupons/@{{coupon.id}}.html"                                            	
                                            	class="accion"   data-toggle="tooltip" data-placement="top" title="ver"
                                            	style="text-decoration: none;cursor:pointer;">
                                             		<i class="fa fa-eye"></i>
                                             	</a>   
                                             	<a  ng-click="activa_desactiva(coupon.id, 'STOPPED')"
                                            	ng-if="coupon.status == 'RUNNING'"
                                            	class="accion"   data-toggle="tooltip" data-placement="top" title="detener"
                                            	style="text-decoration: none;cursor:pointer;">
                                             		<i class="fa fa-pause"></i>
                                             	</a>
                                             	<a  ng-click="activa_desactiva(coupon.id, 'RUNNING')"
                                            	ng-if="coupon.status == 'STOPPED'"
                                            	class="accion" data-toggle="tooltip" data-placement="top" title="activar"
                                            	style="text-decoration: none;cursor:pointer;">
                                             		<i class="fa fa-play"></i>
                                             	</a>     
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <ul class="pagination pagination-sm">
                                    <li ng-class="{active:0}"><a href="#" ng-click="page_anterior()">Anterior</a>

                                    </li>
                                    <li> <a >@{{current_page}}</a>
                                       
                                    </li>
                                    <li><a href="#" ng-click="page_siguiente()">Siguiente</a>

                                    </li>
                                </ul>                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop