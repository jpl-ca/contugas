@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/coupons_controller.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload-shim.js"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload.js"></script>
@yield('js')
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
@section('content')
<div ng-app="app">
    <section class="content-header">
      <h1>
        Gestión de Cupones
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li><a href="/admin/coupons/index.html">Gestión de Cupones</a></li>
        <li class="active" ng-controller="addController"><a>Editar de Cupón @{{coupon.title}}</a></li>
      </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary" ng-controller="addController"  ng-init="coupon_id='{{ $item->id }}'">
                    <div class="box-header with-border"><h4>@{{coupon.title}}</h4>
                    </div>                     
                    <div class="panel-body"><br>
                        <div>
                            <div class="col-md-12">
								<strong>Titulo</strong>
								<p>@{{coupon.title}}</p>    
								<strong>Descripción</strong>
								<p>@{{coupon.description}}</p>    
								<strong>Fecha de Inicio</strong>
								<p>@{{coupon.start_date}}</p>
								<strong>Fecha de Caducidad</strong>
								<p>@{{coupon.end_date}}</p>
                                <strong>Alianza</strong>
                                <p>@{{coupon.company.name}}</p>                                       
                            </div>
                            <div class="col-md-12">
                            	<img ng-src="@{{coupon.img_url}}" alt="imagen" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
     </div>
@stop