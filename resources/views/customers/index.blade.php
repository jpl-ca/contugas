@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/customers_controller.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
@section('content')
    <section class="content-header">
      <h1>
        Clientes
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li class="active"><a>Clientes</a></li>
      </ol>
    </section>
    <section class="content" ng-app="app" ng-controller="CustomersController" ng-init='loadItems()'>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                    	<a ng-click='downloadXls();' title="Descargar archivo XLS" class='pull-right btn btn-primary'><i class='fa fa-download'></i> Exportar</a>
                    	<form class="form-inline">
						 	<div class="form-group">
						    	<input type="text" class="form-control" id="search-text" placeholder="Ingrese un texto a buscar" ng-model='searchText' size='30'/>
						  	</div>
						  	<div class="form-group">
						    	<select class='form-control' name="" id="" ng-options='item as item.name for item in filterItems' ng-model='selectedFilter'></select>
						  	</div>
						  	<button type="submit" class="btn btn-default" ng-click='loadItems()'>Buscar</button>
						</form>
                    </div>                     
                    <div class="panel-body">
                        <div>
                            <div class="col-sm-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        	<th>Número de cliente</th>
                                            <th>Nombre</th>
                                            <th>E-mail</th>
                                            <th>Provincia</th>
                                            <th>Última conexión</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         
                                        <tr class="" ng-repeat="item in items">
                                            <td>@{{item.client_number}}</td>    
                                            <td>@{{item.name}}</td>
                                            <td>@{{item.email}}</td>
                                            <td>@{{item.province_text}}</td>
                                            <td>@{{item.last_login | limitTo:10}} </td>
                                            <td>
                                                <a ng-click="destroyItem(item.id)" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="eliminar">
                                                    <span class="fa fa-trash" aria-hidden="true"></span>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <nav>
                                    <ul class="pagination pagination-sm">
                                        <li ng-class="{active:0}">
                                            <a href="#" ng-click="loadPreviousItems();">Anterior</a>
                                        </li>
                                        <li> <a >@{{currentPage}}</a>
                                           
                                        </li>
                                        <li>
                                            <a href="#" ng-click="loadNextItems();">Siguiente</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop