@extends('layouts.login')

@section('content')
@section('js')
    <!-- for checkbox -->
    <link href="{{ asset('app/css/green.css')}}" rel="stylesheet">
        <!-- checkbox -->
    <script src="{{ asset('app/js/icheck.min.js')}}"></script>
    <script type="text/javascript">
            /* for checkbox*/
$(document).ready(function(){
  $('input').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
  });
});
    </script>
@stop
 <style>
    body{
        background: url('{{ asset('app/img/fondo.jpg')}}') no-repeat fixed;
        background-size: cover;
    }
     input, button {
    outline: none;
    border: none;
}

*, *:before, *:after {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
}
    .login-panel {
    padding: 25px;
    margin-top: 50%;
    background: rgba(255, 255, 255, 0.8);
    border-radius: 0;
    border: 0;
}

.newinput{
    display: inline-block;
    width: 22rem;
    height: 100%;
    padding-left: 1.5rem;
    font-size: 1.5rem;
    background: transparent;
    color: #929191;
    font-family: Arial;
        transition: 0.2s ease-out;
}

.newinput:focus {
    padding: 10px 5px 10px 10px;
    outline: 0;
    border-color: #FF7052;
}
.login__row {
    margin-bottom: 25px;
    height: 5rem;
    padding-top: 1rem;
    border-bottom: 1px solid rgba(86, 86, 86, 0.2)
}

.login__row i{
    color: #929191;
}
.btnlogin{
    background: #1ABC9C;
    border-color: #1ABC9C;
    margin-top: 15px;
    color: white;
}

.btnlogin:hover{
    background: #17A98C;
    border-color: #17A98C;
    margin-top: 15px;
    color: white;
}
    .imglogo{
        width: 450px;
        margin-top: 25%;
    }
@media (max-width: 991px){
    .imglogo{
        margin-top: 60%;
        width: 300px;
    }
}
@media (max-width: 767px){
    .imglogo{
         margin-top: 15%;
    }
    .login-panel {
        margin-top: 10%;
    }
}
@media (max-width: 600px){
    .imglogo{
         width: 250px;
    }
}
@media (max-width: 500px){
    .imglogo{
         margin-left: -20px;
    }
}
@media (max-width: 440px){
    .imglogo{
         margin-left: -30px;
    }
}
@media (max-width: 400px){
    .imglogo{
         margin-left: -45px;
    }
}
@media (max-width: 370px){
    .imglogo{
        width: 200px;
         margin-left: -35px;
    }
}
@media (max-width: 320px){
    .imglogo{
         margin-left: -45px;
    }
} 
    </style>
<div class="container">
    <div class="row">
        <div class="col-xs-3 visible-xs"></div>
        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-4">
                <img class="imglogo"
                src="{{ asset('app/img/logowhite.png')}}" 
                class="img img-responsive" alt="logo centrum">
        </div>
        <div class="col-xs-4 visible-xs"></div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="login-panel panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ action('Auth\AdminAuthController@postLogin') }}">
                        {!! csrf_field() !!}

                        <div class="login__row form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <i class="fa fa-user"></i>
                                    <input class="newinput" placeholder="E-mail" name="email" type="email" value="{{ old('email') }}" autofocus>
                                <!-- <input type="email" class="form-control" name="email" value="{{ old('email') }}"> -->

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="login__row form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <i class="fa fa-key"></i>
                                    <input class="newinput" placeholder="Contraseña" name="password" type="password" value="">
                                <!-- <input type="password" class="form-control" name="password"> -->

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group">
                            <div class="col-md-8" style="padding-left: 0;padding-right: 0;">
                                <div class="checkbox">
                                    <input tabindex="21" type="checkbox" id="polaris-checkbox-1" checked  name="remember">
                                    <label for="polaris-checkbox-1" style="padding-left: 10px;font-family: Arial;color: #929191;font-size: 1.5rem;">
                                    No cerrar sesión</label>
                                </div>
                                <!-- <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> No cerrar sesión
                                    </label>
                                </div> -->
                            </div>
                        </div>

                        <div class="form-group" style="margin-bottom: 0;">
                            <div class="col-md-12" style="padding-right: 0;">
                                <button type="submit" class="btn btnlogin pull-right">
                                    <i class="fa fa-btn fa-sign-in"></i>Iniciar sesion
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
