@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/dashboard_controller.js') }}"></script>
@yield('js')
@section('content')
    <style>
        .inner .titcard{
            font-size: 23px;
            width: 136px;
        }
        .alerta{
            z-index: 10;
            position: inherit;
            margin-top: -16px;
            font-size: 25px;
            margin-right: -12px;
        }
        .icon{
            margin-top: 15px;
        }        
        @media (max-width: 400px)
        {
        .inner .titcard {
            font-size: 21px;
            padding-bottom: 3px;
            width: 136px;
            margin-left: -10px;
        }   
    }
    </style>
    <section class="content-header">
      <h1>
        Panel de Control
      </h1>
      <ol class="breadcrumb">
        <li><a><i class="fa fa-dashboard"></i> Panel de Control</a></li>
      </ol>
    </section>
    <section class="content" ng-app="app">
      <div class="row" ng-controller="appController">
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6" style="cursor:pointer;" ng-click="update(item,'1')">
          <span class="label label-danger pull-right alerta" ng-if="item.new_quotation_requests > 0">
            @{{item.new_quotation_requests}}
          </span>
          <div class="small-box bg-aqua" style="height:128px;">

            <div class="inner">
              <p class="titcard">Solicitudes de Presupuesto</p>
            </div>
            <div class="icon">
              <i class="ion ion-cash"></i>
            </div>
            <a style="bottom: -8px;" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6" style="cursor:pointer;" ng-click="redirect('2')">
          <div class="small-box bg-green" style="height:128px;">          
            <div class="inner">
              <p class="titcard">Mensajes</p>
            </div>
            <div class="icon">
              <i class="ion ion-iphone"></i>
            </div>
            <a style="bottom: -40px;" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"  style="cursor:pointer;" ng-click="redirect('3')">
          <div class="small-box bg-yellow" style="height:128px;">
            <div class="inner">
              <p class="titcard">Gestión de Cupones</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a style="bottom: -8px;" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"  style="cursor:pointer;" ng-click="redirect('4')">
          <div class="small-box bg-red" style="height:128px;">
            <div class="inner">
              <p class="titcard">Gestión de Alianzas</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-home"></i>
            </div>
            <a style="bottom: -8px;" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"  style="cursor:pointer;" ng-click="redirect('5')">
          <div class="small-box bg-red" style="height:128px;">
            <div class="inner">
              <p class="titcard">Consejos</p>
            </div>
            <div class="icon">
              <i class="ion ion-information-circled"></i>
            </div>
            <a style="bottom: -40px;" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"  style="cursor:pointer;" ng-click="redirect('6')">
          <div class="small-box bg-yellow" style="height:128px;">
            <div class="inner">
              <p class="titcard">Eventos y Sorteos</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-bell"></i>
            </div>
            <a style="bottom: -8px;" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6" style="cursor:pointer;" ng-click="update(item,'7')" >
          <span class="label label-danger pull-right alerta"  ng-if="item.new_suggestions > 0">
            @{{item.new_suggestions}}
          </span>            
          <div class="small-box bg-green" style="height:128px;">
            <div class="inner">
              <p class="titcard">Sugerencias</p>
            </div>
            <div class="icon">
              <i class="ion ion-chatboxes"></i>
            </div>
            <a style="bottom: -40px;"class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"  style="cursor:pointer;" ng-click="redirect('8')">
          <div class="small-box bg-aqua" style="height:128px;">
            <div class="inner">
              <p class="titcard">Reportes</p>
            </div>
            <div class="icon">
              <i class="ion ion-connection-bars"></i>
            </div>
            <a style="bottom: -40px;"class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"  style="cursor:pointer;" ng-click="redirect('9')">
          <div class="small-box bg-aqua" style="height:128px;">
            <div class="inner">
              <p class="titcard">Ayuda</p>
            </div>
            <div class="icon">
              <i class="ion ion-help-circled"></i>
            </div>
            <a style="bottom: -40px;" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"  style="cursor:pointer;" ng-click="redirect('12')">
          <div class="small-box bg-green" style="height:128px;">
            <div class="inner">
              <p class="titcard">Ahorro en el consumo</p>
            </div>
            <div class="icon">
              <i class="ion ion-cash"></i>
            </div>
            <a style="bottom: -10px;" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"  style="cursor:pointer;" ng-click="redirect('13')">
          <div class="small-box bg-red" style="height:128px;">
            <div class="inner">
              <p class="titcard">Clientes</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-stalker"></i>
            </div>
            <a style="bottom: -40px;" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        @if($user_module_is_visible)
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"  style="cursor:pointer;" ng-click="redirect('10')">
          <div class="small-box bg-green" style="height:128px;">
            <div class="inner">
              <p class="titcard">Usuarios</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-stalker"></i>
            </div>
            <a style="bottom: -40px;" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"  style="cursor:pointer;" ng-click="redirect('11')">
          <div class="small-box bg-yellow" style="height:128px;">
            <div class="inner">
              <p class="titcard">Configuración</p>
            </div>
            <div class="icon">
              <i class="ion ion-gear-a"></i>
            </div>
            <a style="bottom: -40px;"  class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        @endif
      </div>
    </section>
@stop