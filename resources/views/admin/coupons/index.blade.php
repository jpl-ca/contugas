@extends('layouts.admin')

@section('content')
    <div class="container spark-screen">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Cupones</div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="{{ action('CouponController@getCreate') }}" class="btn btn-primary pull-right">Crear Nuevo</a>
                            </div>
                        </div>
                        <div>
                            <div class="col-sm-12">
                                <table class="table">
                                    <caption>Cupones disponibles.</caption>
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>título</th>
                                            <th>cant. cupones</th>
                                            <th>canjes por usuario</th>
                                            <th>desde</th>
                                            <th>hasta</th>
                                            <th>acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         @foreach($coupons as $coupon)
                                        <tr class="{{ $coupon->active == 1 ? 'info' : ''  }}">
                                            <th scope="row">
                                                {{ $coupon->id }}
                                                @if($coupon->active == 1)
                                                    <span class="label label-primary">activo</span>
                                                @endif
                                            </th>
                                            <td>{{ $coupon->title }}</td>
                                            <td>{{ $coupon->max_swaps }}</td>
                                            <td>{{ $coupon->user_swaps }}</td>
                                            <td>{{ $coupon->start_date }}</td>
                                            <td>{{ $coupon->end_date }}</td>
                                            <td>
                                                <a href="" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="ver">
                                                    <span class="fa fa-eye" aria-hidden="true"></span>
                                                </a>
                                                <a href="" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="editar">
                                                    <span class="fa fa-pencil" aria-hidden="true"></span>
                                                </a>
                                                <a href="" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="eliminar">
                                                    <span class="fa fa-trash" aria-hidden="true"></span>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop