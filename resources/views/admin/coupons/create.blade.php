@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Nuevo Cupón
                    </div>
                    <div class="panel-body">
                        {!! Form::fhOpen(['action' => 'CouponController@postStore', 'method' => 'post']) !!}

                            {!! Form::token() !!}

                            {!! Form::fhText('id', 'Codigo', $id, ['readonly']) !!}

                            {!! Form::fhText('title', 'Título') !!}

                            {!! Form::fhText('description', 'Descripción') !!}

                            {!! Form::fhText('img_url', 'Imagen') !!}

                            {!! Form::fhText('max_swaps', 'Cantidad de cupones') !!}

                            {!! Form::fhText('user_swaps', 'Canjes por usuario') !!}

                            {!! Form::fhDateTime('start_date', 'Fecha de inicio', 'start_date') !!}

                            {!! Form::fhDateTime('end_date', 'Fecha de fin', 'end_date') !!}

                            {!! Form::fhSelect('merchants[]', 'Comercios', $merchants, 1, ['multiple' => 'multiple']) !!}

                            {!! Form::fhSelect('active', 'Estado', ['0' => 'Inactivo', '1' => 'Activo'], 1) !!}

                            {!! Form::fhSubmit('Crear nuevo cupón', 'success', 'check')  !!}

                        {!! Form::fhClose() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        $(function () {
            $('#start_date').datetimepicker({
                locale: 'es',
                format: 'DD-MM-YYYY'
            });

            $('#end_date').datetimepicker({
                locale: 'es',
                format: 'DD-MM-YYYY',
                useCurrent: false //Important! See issue #1075
            });

            $("#start_date").on("dp.change", function (e) {
                $('#end_date').data("DateTimePicker").minDate(e.date.add(1, 'days'));
            });

            $("#end_date").on("dp.change", function (e) {
                $('#start_date').data("DateTimePicker").maxDate(e.date);
            });
        });
    </script>
@stop

