@extends('layouts.admin')
@section('content')
<section class="content-header">
      <h1>
        Configuración de sistema
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li class="active"><a>Configuración de sistema</a></li>
      </ol>
    </section>
<section class="content" ng-app="app">
	<div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                                         
                    <div class="box-body">
                         <form method='post' action=''>
                         	  <input type="hidden" name='_method' value='put'>
              						  <div class="form-group">
              						    <label for='main_mailbox'>Correo principal de emisión</label>
              						    <input type="email" name='main_mailbox' class="form-control" id="main_mailbox" placeholder="Email" value='{{$systemSetting->main_mailbox}}'>
              						  </div>

              						  <div class="form-group">
              						    <label for="recipient_mailbox_of_suggestions">Correo destinario del buzón de sugerencias</label>
              						    <input type="email" name='recipient_mailbox_of_suggestions' class="form-control" id="recipient_mailbox_of_suggestions" placeholder="Password" value='{{$systemSetting->recipient_mailbox_of_suggestions}}'>
              						  </div>

                            <div class="form-group">
                              <label for="recipient_mailbox_of_quotation_requests">Correo destinario del buzón de solicitudes de presupuesto</label>
                              <input type="email" name='recipient_mailbox_of_quotation_requests' class="form-control" id="recipient_mailbox_of_quotation_requests" placeholder="Password" value='{{$systemSetting->recipient_mailbox_of_quotation_requests}}'>
                            </div>
                    </div>
                    <div class="box-footer">
                      <button type="submit" class="btn btn-success pull-right">Guardar</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
</section>
@stop