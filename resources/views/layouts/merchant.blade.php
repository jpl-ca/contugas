<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Contugas</title>
    <link rel="shortcut icon" href="{{ asset('theme/dist/img/icon.png') }}">
    <!-- Fonts -->
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Ionicons -->
    <link href="{{ asset('theme/plugins/ionicons/2.0.1/css/ionicons.min.css') }}"  rel="stylesheet">
    <!-- Theme style -->
    <link href="{{ asset('theme/dist/css/AdminLTE.min.css') }}"  rel="stylesheet">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link href="{{ asset('theme/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="app-layout"  class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

  <header class="main-header">
    <a href="/alianzas" class="logo">
      <span class="logo-mini">
        <img src="{{ asset('theme/dist/img/logomin.png') }}" alt="Logo">
      </span>
      <span class="logo-lg">
        <img src="{{ asset('theme/dist/img/logo.png') }}" alt="Logo">
      </span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs">{{ Auth::guard('merchant')->user()->name }} 
               <i class="fa fa-angle-down"></i></span>
            </a>
            <ul class="dropdown-menu" style="background-color: transparent;padding-top: 0;border-right-width: 0px;">
                <div class="pull-right">
                  <a href="{{ $logoutUrl }}" class="btn btn-default btn-flat" 
                  style="background:#004C87;border: 1px solid #004C87;color:white;">
                  <i class="fa fa-sign-out fa-fw"></i>Cerrar Sesión</a>
                </div>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header">Menu de Navegación</li>
        <li class="treeview" id="acdash">
          <a href="/alianzas">
            <i class="fa fa-dashboard"></i> <span>Panel de Control</span>
          </a>
        </li>
        <li class="treeview" id="ac1">
          <a href="/alianzas/coupons/swap.html">
            <i class="fa fa-tag"></i> <span style="width: 200px;">Canje de cupones</span>
          </a>
        </li>
        <li class="treeview" id="ac2">
          <a href="/alianzas/coupons/history.html">
            <i class="fa fa-history"></i> <span>Historial de canjes</span>
          </a>
        </li>
      </ul>
    </section>
  </aside>

  <div class="content-wrapper">
 
    <section class="content">
      @yield('content')
    </section>

  </div>


  <!-- <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.3
    </div>
    <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com/">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer> -->

 
</div>

<!-- JavaScripts -->
{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
<script type="text/javascript"  src="{{ asset('jquery/2.1.4/jquery.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('moment/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('moment/locale/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('bootstrap/js/transition.js') }}"></script>
<script type="text/javascript" src="{{ asset('bootstrap/js/collapse.js') }}"></script>

<script type="text/javascript" src="{{ asset('bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script type="text/javascript" src="{{ asset('theme/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script type="text/javascript" src="{{ asset('theme/plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script type="text/javascript" src="{{ asset('theme/dist/js/app.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script type="text/javascript" src="{{ asset('theme/dist/js/demo.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/datetimepiker/datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/datetimepiker/datepicker.en.js') }}"></script>
@yield('js');
<script type="text/javascript">
       jQuery(document).ready(function() {
          $('.datex').datepicker({
            language: 'en'
          });
          $('#date').datepicker({
            language: 'en',
            minDate: new Date() 
          });

          /*tabs cupones*/
            $(".hidee").hide();
            $("#i1").show();    
            $(".contitem").click(function() {
                var lol  =  $(this).data("cont");
                $(".hidee").hide();     
                $("#"+lol).show();
                $(".contitem").css("background","transparent");  
                $(this).css("background","#f5f5f5");     
            });
            $(".btnpaso").click(function() {
                var lol  =  $(this).data("cont");
                $(".hidee").hide();     
                $("#"+lol).show();
                $(".contitem").css("background","transparent");  
                $("#a"+lol).css("background","#f5f5f5");     
            });

            var path = window.location.pathname;
            if (path == '/admin') {
                 $("#acdash").addClass("active");
            }
            if (/quotation-requests/.test(path)) {
                 $("#ac1").addClass("active");
            }
            if (/app-components/.test(path)) {
                 $("#ac2").addClass("active");
            }
            if (/admin\/coupons/.test(path)) {
                 $("#ac3").addClass("active");
            }
            if (/companies/.test(path)) {
                 $("#ac4").addClass("active");
            }
            if (/tips/.test(path)) {
                 $("#ac5").addClass("active");
            }
            if (/events/.test(path)) {
                 $("#ac6").addClass("active");
            }
            if (/suggestions/.test(path)) {
                 $("#ac7").addClass("active");
            }
            if (/reports/.test(path)) {
                 $("#ac8").addClass("active");
            }
            if (/help/.test(path)) {
                 $("#ac9").addClass("active");
            }
            if (/admin-users/.test(path)) {
                 $("#ac10").addClass("active");
            }
            if (/system-configuration/.test(path)) {
                 $("#ac11").addClass("active");
            }      

            if (/customers/.test(path)) {
                 $("#ac12").addClass("active");
            }
       });
</script>


</body>
</html>
