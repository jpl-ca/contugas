<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Contugas</title>

    <!-- Fonts -->
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Ionicons -->
    <link href="{{ asset('theme/plugins/ionicons/2.0.1/css/ionicons.min.css') }}"  rel="stylesheet">
    <!-- Theme style -->
    <link href="{{ asset('theme/dist/css/AdminLTE.min.css') }}"  rel="stylesheet">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link href="{{ asset('theme/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">


@yield('content')



<!-- JavaScripts -->
{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
<script type="text/javascript"  src="{{ asset('jquery/2.1.4/jquery.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('moment/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('moment/locale/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('bootstrap/js/transition.js') }}"></script>
<script type="text/javascript" src="{{ asset('bootstrap/js/collapse.js') }}"></script>

<script type="text/javascript" src="{{ asset('bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script type="text/javascript" src="{{ asset('theme/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script type="text/javascript" src="{{ asset('theme/plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script type="text/javascript" src="{{ asset('theme/dist/js/app.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script type="text/javascript" src="{{ asset('theme/dist/js/demo.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/datetimepiker/datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/datetimepiker/datepicker.en.js') }}"></script>
<script type="text/javascript">
       jQuery(document).ready(function() {
          $('.datex').datepicker({
            language: 'en'
          });
          $('#date').datepicker({
            language: 'en',
            minDate: new Date() 
          });

          /*tabs cupones*/
            $(".hidee").hide();
            $("#i1").show();    
            $(".contitem").click(function() {
                var lol  =  $(this).data("cont");
                $(".hidee").hide();     
                $("#"+lol).show();
                $(".contitem").css("background","transparent");  
                $(this).css("background","#f5f5f5");     
            });
            $(".btnpaso").click(function() {
                var lol  =  $(this).data("cont");
                $(".hidee").hide();     
                $("#"+lol).show();
                $(".contitem").css("background","transparent");  
                $("#a"+lol).css("background","#f5f5f5");     
            });

       });
</script>
@yield('js')

</body>
</html>
