@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/companies_controller.js') }}"></script>
<script src="https://maps.google.com/maps/api/js?libraries=placeses,visualization,drawing,geometry,places"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/ngmap/build/scripts/ng-map.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
@yield('js')
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
@section('content')
<div ng-app="app">
    <section class="content-header">
      <h1>
        Gestión de Alianzas
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li><a href="/admin/companies/index.html">Gestión de Alianzas</a></li>
        <li class="active"  ng-controller="showCtrl"><a>Alianza: @{{detcompanie.name}}</a></li>
      </ol>
    </section>
    <section class="content">
        <div class="row" ng-controller="showCtrl">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border"><h4>Alianza: @{{detcompanie.name}}</h4>
                    </div>                     
                    <div class="panel-body">
                        <div>
                            <div class="col-md-6">
								<strong>Nombre</strong>
								<p>@{{detcompanie.name}}</p> 
								<strong>Dirección</strong>
								<p>@{{detcompanie.address}}</p>
								<strong>Descripción</strong>
								<p>@{{detcompanie.description}}</p>                                          
                            </div>
                            <div class="col-md-6">
                                <ng-map zoom="12" 
                                    center="[@{{detcompanie.company_places[0].lat}}, @{{detcompanie.company_places[0].lng}}]" style="height:300px"
                                    on-click="add_place_to_company(event)">
                                    <marker
                                    ng-click="click(event)"
                                    ng-repeat="place in detcompanie.company_places"
                                    position="[@{{place.lat}}, @{{place.lng}}]"
                                    >
                                    </marker> 
                                </ng-map>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
  </div>
@stop