@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/companies_controller.js') }}"></script>
<script src="https://maps.google.com/maps/api/js?libraries=placeses,visualization,drawing,geometry,places"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/ngmap/build/scripts/ng-map.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
@yield('js')
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
@section('content')
	<div ng-app="app">
	<section class="content-header">
      <h1>
        Gestión de Alianzas
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li><a href="/admin/companies/index.html">Gestión de Alianzas</a></li>
        <li class="active"  ng-controller="showCtrl"><a>Editar de Alianza @{{detcompanie.name}}</a></li>
      </ol>
    </section>
    <section class="content">
        <div class="row"  ng-controller="showCtrl">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border"><h4>Editar Alianza: @{{detcompanie.name}}</h4>
                    </div>                     
                    <div class="box-body">
                        <div>
                            <div class="col-sm-12">
								<form role="form">
									<div class="form-group">
									    <label class="control-label" for="exampleInputEmail1">Nombre de la Alianza</label>
									    <input type="text" class="form-control" ng-model="detcompanie.name">
									</div>
									<div class="form-group">
									    <label class="control-label" for="exampleInputEmail1">Dirección</label>
									    <input type="text" class="form-control" ng-model="detcompanie.address">
									</div>
									<div class="row">
										<div class="col-md-6">
											<label class="control-label" for="exampleInputEmail1">Ubicaciones</label>
											<div>
											    <ng-map zoom="12" center="Lima" style="height:300px"
											      on-click="add_place_to_company(event)">
											      <marker
											      	ng-click="click(event)"
									        		ng-repeat="place in detcompanie.company_places"
									        		position="[@{{place.lat}}, @{{place.lng}}]"
											  		>
											      </marker>	
											    </ng-map>
											</div> 
											<p class="help-block">Clik para agregar marcador</p>

										</div>
										<div class="col-md-6">
											<div class="form-group">
											    <label class="control-label" for="exampleInputEmail1">Otros Detalles</label>
											    <textarea  class="form-control" rows="14" style="resize:none" ng-model="detcompanie.description">
											    </textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class='col-md-12'>
											<label class="control-label" for="exampleInputEmail1">Datos de Autenticación</label>
										</div>
										<div class="col-md-6">										
											<input type="email" class="form-control"  placeholder="email" ng-model="detcompanie.email">
										</div>
										<div class='col-md-6'>
											<input type="password" class="form-control"  placeholder="password" ng-model="detcompanie.password">
										</div>
									</div>	
									                                                   
                            </div>                            
                        </div>                        
                    </div>
                    <div class="box-footer">
						<a href="/admin/companies/index.html" 
						class="btn btn-danger pull-left">Cancelar</a>
						<a class="btn btn-success pull-right"  ng-click="editCompanie()">Actualizar</a>
					</div>								
					</form> 
                </div>
            </div>
        </div>
    </section>
    </div>
@stop