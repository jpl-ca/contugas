@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/companies_controller.js') }}"></script>
<script src="https://maps.google.com/maps/api/js?libraries=placeses,visualization,drawing,geometry,places"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/ngmap/build/scripts/ng-map.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
@yield('js')
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
@section('content')
    <section class="content-header">
      <h1>
        Gestión de Alianzas
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li class="active"><a>Gestión de Alianzas</a></li>
      </ol>
    </section>
    <section class="content" ng-app="app">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <a href="/admin/companies/create.html" 
                        class="btn btn-success pull-right">
                        <i class="fa fa-plus" style="margin-right:5px"></i>Crear Nuevo</a>
                    </div>                     
                    <div class="panel-body">
                        <div>
                            <div class="col-sm-12" ng-controller="comapniesCtrl">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width: 89%;">Nombre</th>
                                            <!-- <th>Dirección</th>
                                            <th>Descripción</th> -->
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         
                                        <tr class="" ng-repeat="conpanie in conpanies">
                                            <td>@{{conpanie.name}}</td>
                                            <!-- <td>@{{conpanie.address}}</td>
                                            <td>@{{conpanie.description}}</td> -->
                                            <td>
                                                <a href="/admin/companies/@{{conpanie.id}}.html" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="ver">
                                                    <span class="fa fa-eye" aria-hidden="true"></span>
                                                </a>
                                                <a href="/admin/companies/@{{conpanie.id}}/edit.html" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="editar">
                                                    <span class="fa fa-pencil" aria-hidden="true"></span>
                                                </a>
                                                <a ng-click="delete_companie(conpanie.id, $index)" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="eliminar">
                                                    <span class="fa fa-trash" aria-hidden="true"></span>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <ul class="pagination pagination-sm">
                                    <li ng-class="{active:0}"><a href="#" ng-click="page_anterior()">Anterior</a>

                                    </li>
                                    <li> <a >@{{current_page}}</a>
                                       
                                    </li>
                                    <li><a href="#" ng-click="page_siguiente()">Siguiente</a>

                                    </li>
                                </ul>                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop