<ul class="nav navbar-nav">
    @if (Auth::guard($guard)->check())

    @endif
</ul>

<!-- Right Side Of Navbar -->
<ul class="nav navbar-nav navbar-right">
    <!-- Authentication Links -->
    @if (Auth::guard($guard)->guest())
        <li><a href="{{ $loginUrl }}">Iniciar sesión</a></li>
        {{--
        <li><a href="{{ action('Auth\AdminAuthController@getRegister') }}">Register</a></li>
        --}}
    @else
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::guard($guard)->user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li><a href="#"><i class="fa fa-btn fa-user"></i>Mi cuenta</a></li>
                <li><a href="{{ $logoutUrl }}"><i class="fa fa-btn fa-sign-out"></i>Cerrar sesión</a></li>
            </ul>
        </li>
    @endif
</ul>