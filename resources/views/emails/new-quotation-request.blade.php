<h1>Solicitud de presupuesto</h1>

<div>
	Solicitante: {{ $item->last_name }}, {{ $item->name }}
</div>

<div>
	Dirección: {{ $item->address }}, {{$item->district->name}}, {{$item->province->name}} 
</div>

<div>
	Telefono: {{ $item->phone }}
</div>

<div>
	E-mail: {{ $item->email }}
</div>

@if( $item->lat )
<div>
	Ubicación :<a href="{{ 'https://www.google.com.pe/maps/@' . $item->lat}},{{$item->lng}},16z">Ver mapa</a>
</div>
@endif

<div>Contenido</div>
<p>
	{{ $item->comment }}
</p>
