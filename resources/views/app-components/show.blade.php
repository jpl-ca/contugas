@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/message_controller.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload-shim.js"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload.js"></script>
@yield('js')
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
@section('content')
    <div  ng-app="app">
    <section class="content-header">
      <h1>
        Mensajes en Aplicación Móvil
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li><a href="/admin/app-components/index.html">Mensajes en Aplicación Móvil</a></li>
        <li class="active"  ng-controller="addController">Mensaje #@{{detmessage.id}}</li>
      </ol>
    </section>
    <section class="content">
        <div class="row" ng-controller="addController">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="panel-heading"><h4>Mensaje #@{{detmessage.id}}</h4>
                    </div>                     
                    <div class="panel-body">
                        <div>
                            <div class="col-md-12">
								<strong>Sección</strong>
								<p>@{{detmessage.section}}</p>    
								<strong>Descripción</strong>
								<p>@{{detmessage.description}}</p>    
								<strong>Fecha de Inicio</strong>
								<p>@{{detmessage.starts}}</p>
								<strong>Fecha de Caducidad</strong>
								<p>@{{detmessage.ends}}</p>                                       
                            </div>
                            <div class="col-md-12">
                            	<img ng-src="@{{detmessage.image_url}}" alt="imagen" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop