@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/message_controller.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload-shim.js"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload.js"></script>
@yield('js')
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/datetimepiker/datepicker.min.css') }}">
<style>
	form .progress {
    line-height: 15px;
}
}

.progress {
    display: inline-block;
    width: 100px;
    border: 3px groove #CCC;
}

.progress div {
    font-size: smaller;
    background: orange;
    width: 0;
}
</style>
@section('content')
	<section class="content-header">
      <h1>
        Mensajes en Aplicación Móvil
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li><a href="/admin/app-components/index.html">Mensajes en Aplicación Móvil</a></li>
        <li class="active">Crear Mensaje</li>
      </ol>
    </section>
    <section class="content" ng-app="app">
        <div class="row" ng-controller="addController">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border"><h4>Crear Mensaje</h4>
                    </div>                     
                    <div class="panel-body">
                        <div>
                            <div class="col-sm-12">
								<form role="form" name="myForm" autocomplete="off">
									<div class="form-group">
										<label class="control-label">Sección</label>
										<select class="form-control"
					                      ng-options="section.label for section in sections" 
					                      ng-model="select_section">
					                      </select>
									</div>
									<div class="form-group">
										<label class="control-label">Provincia</label>
										<select class="form-control"
					                      ng-options="province.name for province in provinces" 
					                      ng-model="select_prov">
					                      </select>
									</div><br>
									<div class="form-inline">										
										<div class="form-group" style="margin-right:50px;">
											<label class="control-label" 
											for="exampleInputEmail1">Fecha de Inicio</label>
											<input type="text" class="form-control datex" 
								                required data-date-format="yyyy-mm-dd"
								                id="starts">
										</div>
										<div class="form-group">
											<label class="control-label" 
											for="exampleInputEmail1">Fecha de Fin</label>
											<input type="text" class="form-control datex" 
								                required data-date-format="yyyy-mm-dd"
								                id="ends">
										</div>
									</div><br>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
											    <label class="control-label" for="exampleInputEmail1">Descripción</label>
											    <textarea  class="form-control" rows="5" style="resize:none" ng-model="message.description"></textarea><br>
											</div>
										</div>
										<div class="col-md-12 ">
											<div class="form-group">
											 	<label class="control-label">Imagen</label>
										      	<input type="file" ngf-select ng-model="picFile" name="file"    
										             accept="image/*" ngf-max-size="2MB" required
										             ngf-model-invalid="errorFile">
										             <p class="help-block">La imagen debe tener una de las siguientes dimensiones:
										             	<ul>
										             		<li>Principal 640 x 1024 píxeles</li>
										             		<li>Consumo 950 x 150 píxeles</li>
										             		<li>Historial 950 x 150 píxeles</li>
										             		<li>Lista de pagos 950 x 150 píxeles</li>
										             		<li>Financiamiento 950 x 150 píxeles</li>
										             	</ul>
										             </p>
										      	<i ng-show="myForm.file.$error.required" style="color:red">* Requerido</i><br>
										      	<i ng-show="myForm.file.$error.maxSize"  style="color:red">Archivo demasiado grande 
										          (@{{errorFile.size / 1000000|number:1}}MB): máximo  2MB</i>
										      	<div class="row">    
						                            <div class="col-sm-10">
						                            	<img ng-show="myForm.file.$valid" ngf-thumbnail="picFile" 
											      			class="img-responsive">
											      	</div>
						                            <div class="col-sm-2">
						                            	<button ng-click="picFile = null" ng-show="picFile" 
													      class="btn btn-danger"><i class="fa fa-remove"></i> Quitar</button>
													      </div>
						                            </div>	
					                         </div>					       
										      
										    <div class="form-group">
										      	  <span class="progress" ng-show="picFile.progress >= 0">
										            <div style="width:@{{picFile.progress}}%" 
										            ng-bind="picFile.progress + '%'"></div>
											      </span>
											      <span ng-show="picFile.result">	</span>
											      <span class="err" ng-show="errorMsg">@{{errorMsg}}</span>
										    </div>
										</div>										
									</div>											                                                  
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                    	<a href="/admin/app-components/index.html" 
					    class="btn btn-danger pull-left">Cancelar</a>
					    <button class="btn btn-success pull-right" ng-click="addMessage(picFile)" ng-disabled="!myForm.$valid" >Crear ahora</button>
                    </div>
                    </form> 
                </div>
            </div>
        </div>
    </section>
@stop