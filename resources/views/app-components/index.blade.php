@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/message_controller.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload-shim.js"></script>
<script type="text/javascript" src="https://angular-file-upload.appspot.com/js/ng-file-upload.js"></script>
@yield('js')
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
@section('content')
    <section class="content-header">
      <h1>
        Mensajes en Aplicación Móvil
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li class="active"><a>Mensajes en Aplicación Móvil</a></li>
      </ol>
    </section>
    <section class="content" ng-app="app">
        <div class="row"  ng-controller="messageController">
            <div class="col-md-12">
                <div  class="box box-primary">
                    <div class="box-header with-border">
                        <a href="/admin/app-components/create.html" 
                        class="btn btn-success pull-right">
                        <i class="fa fa-plus" style="margin-right:5px"></i>Crear Nuevo</a>

                        <a href="{{url('/admin/app-components/index.xls')}}" title="Descargar archivo XLS"
                            style='margin-right:7px;' class='pull-right btn btn-primary'>
                            <i class='fa fa-download'></i>Exportar
                        </a>
                    </div>                     
                    <div class="box-header with-border">
                        <div>
                            <div class="col-sm-12">
                                <table class="table">
                                <br>
                                    <thead>
                                        <tr>
                                            <th style="width: 55%;">Sección</th>
                                            <th>Provincia</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         
                                        <tr class="" ng-repeat="message in messages">
                                            <td>@{{message.label}}</td>    
                                            <td>@{{message.ubigeo.name}}</td>
                                            <td>
                                                <a href="/admin/app-components/@{{message.id}}.html" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="ver">
                                                    <span class="fa fa-eye" aria-hidden="true"></span>
                                                </a>
                                                <a href="/admin/app-components/@{{message.id}}/edit.html" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="editar">
                                                    <span class="fa fa-pencil" aria-hidden="true"></span>
                                                </a>
                                                <a ng-click="delete_message(message.id, $index)" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="eliminar">
                                                    <span class="fa fa-trash" aria-hidden="true"></span>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <nav>
                                    <ul class="pagination pagination-sm">
                                        <li ng-class="{active:0}"><a href="#" ng-click="page_anterior()">Anterior</a>

                                        </li>
                                        <li> <a >@{{current_page}}</a>
                                           
                                        </li>
                                        <li><a href="#" ng-click="page_siguiente()">Siguiente</a>

                                        </li>
                                    </ul>                                    
                                </nav>                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop