@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/message_controller.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/admin-users.js') }}"></script>
@yield('js')
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
@section('content')
    <section class="content-header">
      <h1>
        Usuarios de administración
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li class="active"><a>Usuarios de administración</a></li>
      </ol>
    </section>
    <section class="content" ng-app="app">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <a href="/admin/admin-users/create.html"
                        class="btn btn-success pull-right">
                        <i class="fa fa-plus" style="margin-right:5px"></i>Crear Nuevo</a>
                    </div>                     
                    <div class="panel-body">
                        <div>
                            <div class="col-sm-12" ng-controller="adminUserController" ng-init='load_items()'>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>E-mail</th>
                                            <th>Creación</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         
                                        <tr class="" ng-repeat="item in items">
                                            <td>@{{item.name}}</td>    
                                            <td>@{{item.email}}</td>
                                            <td>@{{item.created_at}}</td>
                                            <td>
                                                <a href="/admin/admin-users/@{{item.id}}/edit.html" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="editar">
                                                    <span class="fa fa-pencil" aria-hidden="true"></span>
                                                </a>
                                                <a ng-click="destroy_item(item.id)" class="btn btn-xs" data-toggle="tooltip" data-placement="top" title="eliminar">
                                                    <span class="fa fa-trash" aria-hidden="true"></span>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <nav>
                                    <ul class="pagination pagination-sm">
                                        <li ng-class="{active:0}">
                                            <a href="#" ng-click="load_previous_items();">Anterior</a>
                                        </li>
                                        <li> <a >@{{current_page}}</a>
                                           
                                        </li>
                                        <li>
                                            <a href="#" ng-click="load_next_items();">Siguiente</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop