@extends('layouts.admin')
@yield('content')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/scripts/controllers/admin-users.js') }}"></script>
@yield('js')
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app/datetimepiker/datepicker.min.css') }}">
@section('content')
    <section class="content-header">
      <h1>
        Usuarios de administración
      </h1>
       <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li><a href="/admin/admin-users/index.html">Usuarios de administración</a></li>
        <li class="active"><a>Crear Usuarios</a></li>
      </ol>
    </section>
    <section class="content" ng-app="app">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary"  ng-controller="adminUserController">
                    <div class="box-header with-border"><h4>Crear Usuarios</h4>
                    </div>                     
                    <div class="box-body">
                        <div>
                            <div class="col-sm-12">
								<form role="form" id="item-form" autocomplete="off">
									<div class="form-group">
									    <label for="name">Nombre</label>
									    <input type="text" class="form-control" id="name" placeholder="Nombre" ng-model='item.name'>
									</div>
									<div class="form-group">
									    <label for="email">E-mail</label>
									    <input type="email" class="form-control" id="email" placeholder="E-mail" ng-model='item.email'>
									</div>
                                    <div class="form-group">
                                        <label for="name">Password</label>
                                        <input type="password" class="form-control" id="password" placeholder="Password" ng-model='item.password'>
                                    </div>									                                             
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type='submit' class='btn btn-success pull-right' ng-click='save();'>Guardar</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@stop