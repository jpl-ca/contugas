@extends('layouts.merchant')
@section('js')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
<script type="text/javascript" src="{{ asset('/app/scripts/controllers/merchant_dashboard_controller.js') }}"></script>
@stop
@section('content')
    <style>
        .inner .titcard{
            font-size: 23px;
            width: 136px;
        }
        .alerta{
            z-index: 10;
            position: inherit;
            margin-top: -16px;
            font-size: 25px;
            margin-right: -12px;
        }
        .icon{
            margin-top: 15px;
        }        
        @media (max-width: 400px)
        {
        .inner .titcard {
            font-size: 21px;
            padding-bottom: 3px;
            width: 136px;
            margin-left: -10px;
        }   
    }
    </style>
    <section class="content-header">
      <h1>
        Panel de Control
      </h1>
      <ol class="breadcrumb">
        <li><a><i class="fa fa-dashboard"></i> Panel de Control</a></li>
      </ol>
    </section>
    <section class="content" ng-app="app">
      <div class="row" ng-controller="MerchantDashboardController">
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6" style="cursor:pointer;" ng-click="redirect('/merchant/coupons/swap.html')">
          <div class="small-box bg-green" style="height:128px;">          
            <div class="inner">
              <p class="titcard">Canjear cupón</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-pricetags"></i>
            </div>
            <a style="bottom: -8px;" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6"  style="cursor:pointer;" ng-click="redirect('/merchant/coupons/history.html')">
          <div class="small-box bg-yellow" style="height:128px;">
            <div class="inner">
              <p class="titcard">Historial de canjes</p>
            </div>
            <div class="icon">
              <i class="ion ion-clipboard"></i>
            </div>
            <a style="bottom: -8px;" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
    </section>
@stop