@extends('layouts.merchant')
@section('js')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
<script type="text/javascript" src="{{ asset('/app/scripts/controllers/merchant_dashboard_controller.js') }}"></script>
@stop
@section('content')
    <section class="content-header">
      <h1>
        Historial de canjes
      </h1>
       <ol class="breadcrumb">
        <li><a href="/merchant"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li class="active"><a>Historial de canjes</a></li>
      </ol>
    </section>
    <section class="content" ng-app="app" ng-controller='MerchantHistoryController' ng-init='loadItems();'>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        
                    </div>                     
                    <div class="panel-body">
                        <div>
                            <div class="col-sm-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th width="10">Cupón</th>
                                            <th width="70%">Cliente</th>
                                            <th width='20%'>Fecha de canje</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         
                                        <tr class="" ng-repeat="item in items">
                                            <td>@{{item.coupon_id}}-@{{item.user.id}}</td>
                                            <td>@{{item.user.name}}</td>
                                            <td>@{{item.created_at}}</td> 
                                        </tr>
                                    </tbody>
                                </table>
                                <nav>
                                    <ul class="pagination pagination-sm">
                                        <li>
                                            <a href="#" ng-click="loadPreviousPage()">Anterior</a>
                                        </li>
                                        <li>
                                            <a >@{{currentPage}}</a>
                                        </li>
                                        <li>
                                            <a href="#" ng-click='loadNextPage()'>Siguiente</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop