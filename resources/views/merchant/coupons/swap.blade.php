@extends('layouts.merchant')
@section('js')
<script src="{{ asset('app/bower_components/angular/angular.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/bower_components/angular-sweetalert/dist/ngSweetAlert.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('app/bower_components/sweetalert/dist/sweetalert.css') }}">
<script type="text/javascript" src="{{ asset('/app/scripts/controllers/merchant_dashboard_controller.js') }}"></script>

@stop
@section('content')
    <section class="content-header">
      <h1>
        Canje de cupón
      </h1>
       <ol class="breadcrumb">
        <li><a href="/merchant"><i class="fa fa-dashboard"></i> Panel de Control</a></li>
        <li class="active"><a>Canje de cupón</a></li>
      </ol>
    </section>
    <section class="content" ng-app="app" ng-controller='MerchantSwapController'>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        
                    </div>                     
                    <div class="panel-body">
                        <div>
                            <div class="col-sm-12">
                                <div class='col-md-6 col-md-offset-3'>
                                
                                <form class="form-inline" class='couponForm'>
                                  <div class="form-group">
                                    <label for="exampleInputName2" style="font-weight:normal;font-size:1.85em;">CUPÓN</label>
                                    <input type="text" size='11' class="form-control input-lg" id="exampleInputName2" placeholder="#######-#" ng-model='userCoupon'>
                                  </div>
                                  <button type="submit" class="btn btn-default btn-lg" ng-click='findCoupon()'>Buscar</button>
                                </form>
                                </div>
                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="couponModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel"><b>@{{coupon.title}}</b></h4>
                              </div>
                              <div class="modal-body">
                                <div class='row'>
                                  <div class='col-md-12'>
                                    <div class='col-md-7 col-lg-7'>
                                      <img alt="" ng-src='@{{coupon.img_url}}' class='img-responsive'>
                                      <div style='background: #f4f4f4;text-align:center; padding:4px; border:1px solid #ddd;'>
                                        <span class='text-center'>CUPÓN: @{{userCoupon}}</span>
                                      </div>
                                    </div>
                                    <div class='col-md-5 col-lg-5'>@{{coupon.description}}</div>
                                  </div>
                                </div>
                                <div class='text-center' style='margin-top:20px;'>
                                  <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Cancelar</button>
                                  <button type="button" class="btn btn-success btn-lg" ng-click='swapCoupon()'> <i class='fa fa-check'></i> Canjear ahora</button>
                                </div>
                                <div class='clearfix'></div>
                              </div>
                              </div>
                              <div class="modal-footer">
                                
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <style>
        .coupon
        {
            margin-top: 150px;
        }
    </style>
@stop